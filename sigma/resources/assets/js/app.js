
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('maquinaria-lista', require('./components/maquinaria/maquinaria-lista.vue'));
Vue.component('maquinaria-agregar', require('./components/maquinaria/maquinaria-agregar.vue'));
Vue.component('maquinaria-item', require('./components/maquinaria/maquinaria-item.vue'));
Vue.component('maquinaria-detail', require('./components/maquinaria/maquinaria-detail.vue'));

Vue.component('consumible-lista', require('./components/consumible/consumible-lista.vue'));
Vue.component('consumible-agregar', require('./components/consumible/consumible-agregar.vue'));
Vue.component('consumible-item', require('./components/consumible/consumible-item.vue'));

Vue.component('mpreventivo-lista', require('./components/mpreventivo/mpreventivo-lista.vue'));
Vue.component('mpreventivo-agregar', require('./components/mpreventivo/mpreventivo-agregar.vue'));
Vue.component('mpreventivo-item', require('./components/mpreventivo/mpreventivo-item.vue'));

Vue.component('mcorrectivo-lista', require('./components/mcorrectivo/mcorrectivo-lista.vue'));
Vue.component('mcorrectivo-agregar', require('./components/mcorrectivo/mcorrectivo-agregar.vue'));
Vue.component('mcorrectivo-item', require('./components/mcorrectivo/mcorrectivo-item.vue'));



const app = new Vue({
    el: '#app',
    data: {
        maquinarias: [],
        maquinaria: {},

        consumibles: [],
        consumible: {},
    }
    
});
