@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
           <div class="panel panel-default">
                <div class="panel-heading">Maquinarias</div>
                <div class="panel-body">
                	
		            

		            <maquinaria-lista :maquinarias="maquinarias" :maquinaria="maquinaria" ></maquinaria-lista>

				   
				   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection