@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Maquinarias</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success">O</button>
                            <button type="button" class="btn btn-warning">-</button>
                            <button type="button" class="btn btn-danger">I</button>
                        </div>
                    </div>
                    <hr>
                    <hr>
                    <div class="col-md-12">
                        <maquinaria-detail :maquinaria="maquinaria" ></maquinaria-detail>
                    </div>   
                    <hr>
                    <div class="col-md-12">
                        <button class="btn btn-primary">Programa de mantenimiento</button>
                    </div>   
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection