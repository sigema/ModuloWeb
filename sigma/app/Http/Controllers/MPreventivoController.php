<?php

namespace Sigma\Http\Controllers;

use Sigma\MPreventivo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MPreventivoController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data($id)
    {
        return MPreventivo::where('maquinaria_id', $id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $id)
    {
        $MPreventivo = MPreventivo::create(
            [
                'maquinaria_id' => $id,
                'fecha_ejecucion' => $request->get('fecha_ejecucion'),
                'fecha_reporte' => $request->get('fecha_reporte'),
                'hora' => $request->get('hora'),
                'mecanico' => $request->get('mecanico'),
                'obra_institucion' => $request->get('obra_institucion'),
                'usuario_id' =>  Auth::user()->id,
            ]);
        return $MPreventivo;
    }

    

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMaquinaria, $id)
    {
        MPreventivo::where('maquinaria_id', $idMaquinaria)
        ->where('mpreventivo_id', $id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idMaquinaria , $id)
    {
        $MPreventivo = MPreventivo::where('maquinaria_id', $idMaquinaria)
        ->where('mpreventivo_id', $id)->first();
        $MPreventivo->delete();
        return ["status" , "ok"];
    }
}
