<?php

namespace Sigma\Http\Controllers;

use Sigma\MCorrectivo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class MCorrectivoController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data($id)
    {
        return MCorrectivo::where('maquinaria_id', $id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $id)
    {
        $MCorrectivo = MCorrectivo::create(
            [
                'maquinaria_id' => $id,
                'reporte_falla' => $request->get('reporte_falla'),
                'nro_documento' => $request->get('nro_documento'),
                'hora' => $request->get('hora'),
                'mecanico' => $request->get('mecanico'),
                'obra_institucion' => $request->get('obra_institucion'),
                'usuario_id' =>  Auth::user()->id,
                'fecha' => $request->get('fecha'),
            ]);
        return $MCorrectivo;
    }

    

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMaquinaria, $id)
    {
        MCorrectivo::where('maquinaria_id', $idMaquinaria)
        ->where('mcorrectivo_id', $id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idMaquinaria , $id)
    {
        $MCorrectivo = MCorrectivo::where('maquinaria_id', $idMaquinaria)
        ->where('mcorrectivo_id', $id)->first();
        $MCorrectivo->delete();
        return ["status" , "ok"];
    }
}
