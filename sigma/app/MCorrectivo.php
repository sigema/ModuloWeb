<?php

namespace Sigma;

use Illuminate\Database\Eloquent\Model;

class MCorrectivo extends Model
{
    protected $primaryKey = 'mcorrectivo_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mcorrectivo_id',
        'obra_institucion',
        'reporte_falla',
        'nro_documento',
        'mecanico',
        'hora',
        'usuario_id',
        'maquinaria_id',
        'fecha',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
