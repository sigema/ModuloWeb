<?php

namespace Sigma;

use Illuminate\Database\Eloquent\Model;

class Maquinaria extends Model
{

    protected $primaryKey = 'maquinaria_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'marca',
        'modelo',
        'tipo',
        'nro_de_registro',
        'serie',
        'estado',
        'horometro',
        'doc_asignacion',
        'foto',
        'anio',
        'maquinaria_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
