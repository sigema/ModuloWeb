<?php

namespace Sigma;

use Illuminate\Database\Eloquent\Model;

class Consumible extends Model
{
    protected $primaryKey = 'consumible_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'consumible_id',
        'descripcion',
        'cantidad',
        'unidad',
        'precio_unitario',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
