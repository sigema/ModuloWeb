<?php

namespace Sigma;

use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    protected $primaryKey = 'provincia_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provincia_id',
        'ubicacion_id',
        'latitud',
        'longitud',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
