<?php

namespace Sigma;

use Illuminate\Database\Eloquent\Model;

class MPreventivo extends Model
{
    protected $primaryKey = 'mpreventivo_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mpreventivo_id',
        'obra_institucion',
        'fecha_reporte',
        'fecha_ejecucion',
        'mecanico',
        'hora',
        'usuario_id',
        'maquinaria_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
