<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function(Blueprint $table)
        {
            $table->increments('movimiento_id');
            $table->integer('maquinaria_id')->unsigned();
            $table->foreign('maquinaria_id')->references('maquinaria_id')->on('maquinarias');
            $table->date('fecha_entrada');
            $table->date('fecha_salida');
            $table->integer('ubicacion_id')->unsigned();
            $table->foreign('ubicacion_id')->references('ubicacion_id')->on('ubicaciones');
            $table->string('descripcion');
            $table->string('obra_institucion');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimientos');
    }
}
