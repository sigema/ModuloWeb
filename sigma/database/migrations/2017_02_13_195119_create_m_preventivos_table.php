<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPreventivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_preventivos', function(Blueprint $table)
        {
            $table->increments('mpreventivo_id');
            $table->integer('maquinaria_id')->unsigned();
            $table->foreign('maquinaria_id')->references('maquinaria_id')->on('maquinarias');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->integer('hora');
            $table->string('obra_institucion');
            $table->date('fecha_reporte');
            $table->date('fecha_ejecucion');
            $table->string('mecanico');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_preventivos');
    }
}
