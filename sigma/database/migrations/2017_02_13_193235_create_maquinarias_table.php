<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaquinariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maquinarias', function(Blueprint $table)
        {
            $table->increments('maquinaria_id');
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('tipo')->nullable();
            $table->string('nro_de_registro');
            $table->string('serie')->nullable();
            $table->string('estado')->nullable();
            $table->string('horometro')->nullable();
            $table->string('doc_asignacion')->nullable();
            $table->string('foto')->nullable();
            $table->string('anio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maquinarias');
    }
}
