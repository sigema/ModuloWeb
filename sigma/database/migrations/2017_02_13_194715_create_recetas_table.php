<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recetas', function(Blueprint $table)
        {
            $table->increments('receta_id');
            $table->integer('maquinaria_id')->unsigned();
            $table->foreign('maquinaria_id')->references('maquinaria_id')->on('maquinarias');
            $table->string('periodo');
            $table->string('horas_trabajo');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recetas');
    }
}
