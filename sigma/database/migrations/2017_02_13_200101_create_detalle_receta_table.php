<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleRecetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_receta', function(Blueprint $table)
        {
            $table->increments('detalle_receta_id');
            $table->integer('receta_id')->unsigned();
            $table->foreign('receta_id')->references('receta_id')->on('recetas');
            $table->integer('consumible_id')->unsigned();
            $table->foreign('consumible_id')->references('consumible_id')->on('consumibles');
            $table->dateTime('hora');
            $table->integer('cantidad');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_receta');
    }
}
