<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
//rutas maquinarias
Route::get('maquinarias/list' , 'MaquinariaController@data');

Route::get('maquinarias' , 'MaquinariaController@index')->name('maquinaria-list');

Route::post('maquinarias' , 'MaquinariaController@store');

Route::delete('maquinarias/{id}' , 'MaquinariaController@destroy');

Route::put('maquinarias/{id}' , 'MaquinariaController@update');

Route::get('maquinarias/{id}/show' , 'MaquinariaController@showData');
Route::get('maquinarias/{id}' , 'MaquinariaController@show');


//rutas consumibles
Route::get('consumibles/list' , 'ConsumibleController@data');

Route::get('consumibles' , 'ConsumibleController@index');

Route::post('consumibles' , 'ConsumibleController@store');

Route::delete('consumibles/{id}' , 'ConsumibleController@destroy');

Route::put('consumibles/{id}' , 'ConsumibleController@update');


//rutas mantenimientos preventivos

Route::get('maquinarias/{idMaquinaria}/mpreventivos/list' , 'MPreventivoController@data');

Route::put('maquinarias/{idMaquinaria}/mpreventivos/{id}' , 'MPreventivoController@update');

Route::delete('maquinarias/{idMaquinaria}/mpreventivos/{id}' , 'MPreventivoController@destroy');

Route::post('maquinarias/{idMaquinaria}/mpreventivos' , 'MPreventivoController@store');

Route::get('ubicacion', 'UbicacionController@index');

//rutas mantenimientos correctivos

Route::get('maquinarias/{idMaquinaria}/mcorrectivos/list' , 'MCorrectivoController@data');

Route::put('maquinarias/{idMaquinaria}/mcorrectivos/{id}' , 'MCorrectivoController@update');

Route::delete('maquinarias/{idMaquinaria}/mcorrectivos/{id}' , 'MCorrectivoController@destroy');

Route::post('maquinarias/{idMaquinaria}/mcorrectivos' , 'MCorrectivoController@store');

Route::get('ubicacion', 'UbicacionController@index');



Auth::routes();

Route::get('/home', 'HomeController@index');
