<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMCorrectivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_correctivos', function(Blueprint $table)
		{
			$table->integer('mcorrectivo_id', true);
			$table->integer('maquinaria_id')->nullable()->index('fk_MantenimientosCorrectivos_Maquinarias1_idx');
			$table->integer('usuario_id')->nullable()->index('fk_MantenimientosCorrectivos_Usuarios1_idx');
			$table->date('fecha')->nullable();
			$table->string('mecanico', 200)->nullable();
			$table->string('reporte_falla', 200)->nullable();
			$table->string('nro_documento', 200)->nullable();
			$table->string('obra_institucion')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_correctivos');
	}

}
