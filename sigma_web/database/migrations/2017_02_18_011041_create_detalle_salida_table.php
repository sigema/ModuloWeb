<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleSalidaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_salida', function(Blueprint $table)
		{
			$table->integer('detalle_salida_id', true);
			$table->integer('cantidad')->nullable();
			$table->integer('salida_id')->nullable()->index('fk_detalle_salida_salida1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_salida');
	}

}
