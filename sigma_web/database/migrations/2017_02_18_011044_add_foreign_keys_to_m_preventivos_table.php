<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMPreventivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_preventivos', function(Blueprint $table)
		{
			$table->foreign('maquinaria_id', 'fk_MantenimientosPreventivos_Maquinarias1')->references('maquinaria_id')->on('maquinarias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('usuario_id', 'fk_MantenimientosPreventivos_Usuarios1')->references('usuario_id')->on('usuarios')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_preventivos', function(Blueprint $table)
		{
			$table->dropForeign('fk_MantenimientosPreventivos_Maquinarias1');
			$table->dropForeign('fk_MantenimientosPreventivos_Usuarios1');
		});
	}

}
