<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetalleRecetaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detalle_receta', function(Blueprint $table)
		{
			$table->foreign('consumible_id', 'fk_detalle_receta_consumibles1')->references('consumible_id')->on('consumibles')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('hora_id', 'fk_detalle_receta_horas1')->references('hora_id')->on('horas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detalle_receta', function(Blueprint $table)
		{
			$table->dropForeign('fk_detalle_receta_consumibles1');
			$table->dropForeign('fk_detalle_receta_horas1');
		});
	}

}
