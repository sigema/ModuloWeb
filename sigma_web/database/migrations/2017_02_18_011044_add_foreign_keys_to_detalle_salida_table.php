<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetalleSalidaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detalle_salida', function(Blueprint $table)
		{
			$table->foreign('salida_id', 'fk_detalle_salida_salida1')->references('salida_id')->on('salida')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detalle_salida', function(Blueprint $table)
		{
			$table->dropForeign('fk_detalle_salida_salida1');
		});
	}

}
