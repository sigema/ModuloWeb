<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovimientosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movimientos', function(Blueprint $table)
		{
			$table->integer('movimiento_id', true);
			$table->integer('maquinaria_id')->nullable()->index('fk_movimientos_Maquinarias1_idx');
			$table->date('fecha_entrada')->nullable();
			$table->date('fecha_salida')->nullable();
			$table->text('descripcion', 65535)->nullable();
			$table->string('obra_institucion')->nullable();
			$table->integer('provincia_id')->nullable()->index('fk_movimientos_Provincias1_idx');
			$table->text('doc_asignacion', 65535)->nullable();
			$table->enum('turno', array('1','2'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movimientos');
	}

}
