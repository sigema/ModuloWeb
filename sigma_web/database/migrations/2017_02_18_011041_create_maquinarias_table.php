<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaquinariasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('maquinarias', function(Blueprint $table)
		{
			$table->integer('maquinaria_id', true);
			$table->string('marca', 45)->nullable();
			$table->string('modelo', 45)->nullable();
			$table->string('tipo', 45)->nullable();
			$table->string('nro_de_registro', 45)->nullable();
			$table->string('serie', 45)->nullable();
			$table->enum('estado', array('1','2','3'))->nullable()->default('2');
			$table->integer('horometro')->nullable();
			$table->text('foto', 65535)->nullable();
			$table->integer('anio')->nullable();
			$table->enum('condicion', array('1','2'))->nullable()->default('1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maquinarias');
	}

}
