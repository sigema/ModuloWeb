<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMovimientosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('movimientos', function(Blueprint $table)
		{
			$table->foreign('maquinaria_id', 'fk_movimientos_Maquinarias1')->references('maquinaria_id')->on('maquinarias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('provincia_id', 'fk_movimientos_Provincias1')->references('provincia_id')->on('provincias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('movimientos', function(Blueprint $table)
		{
			$table->dropForeign('fk_movimientos_Maquinarias1');
			$table->dropForeign('fk_movimientos_Provincias1');
		});
	}

}
