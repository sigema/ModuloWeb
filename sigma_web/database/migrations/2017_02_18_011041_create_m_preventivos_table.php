<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMPreventivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_preventivos', function(Blueprint $table)
		{
			$table->integer('mpreventivo_id', true);
			$table->string('mp', 45)->nullable();
			$table->string('obra_institucion')->nullable();
			$table->string('nro_reporte', 200)->nullable();
			$table->string('mecanico', 200)->nullable();
			$table->date('fecha_ejecución')->nullable();
			$table->integer('horometro_aprox')->nullable();
			$table->date('fecha_aprox')->nullable();
			$table->integer('usuario_id')->nullable()->index('fk_MantenimientosPreventivos_Usuarios1_idx');
			$table->integer('maquinaria_id')->nullable()->index('fk_MantenimientosPreventivos_Maquinarias1_idx');
			$table->boolean('realizado')->nullable();
			$table->boolean('pase')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_preventivos');
	}

}
