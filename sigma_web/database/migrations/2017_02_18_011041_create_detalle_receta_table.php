<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleRecetaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_receta', function(Blueprint $table)
		{
			$table->integer('detalle_receta_id', true);
			$table->integer('cantidad')->nullable();
			$table->integer('hora_id')->nullable()->index('fk_detalle_receta_horas1_idx');
			$table->integer('consumible_id')->nullable()->index('fk_detalle_receta_consumibles1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_receta');
	}

}
