<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetalleEntradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detalle_entrada', function(Blueprint $table)
		{
			$table->foreign('entrada_id', 'fk_detalle_entrada_entrada1')->references('entrada_id')->on('entrada')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detalle_entrada', function(Blueprint $table)
		{
			$table->dropForeign('fk_detalle_entrada_entrada1');
		});
	}

}
