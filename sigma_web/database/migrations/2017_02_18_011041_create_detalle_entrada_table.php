<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleEntradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_entrada', function(Blueprint $table)
		{
			$table->integer('detalle_entrada_id', true);
			$table->decimal('precio', 10, 0)->nullable();
			$table->integer('cantidad')->nullable();
			$table->integer('entrada_id')->nullable()->index('fk_detalle_entrada_entrada1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_entrada');
	}

}
