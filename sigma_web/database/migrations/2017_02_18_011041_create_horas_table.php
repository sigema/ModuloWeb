<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('horas', function(Blueprint $table)
		{
			$table->integer('hora_id')->primary();
			$table->string('cantidad', 45)->nullable();
			$table->integer('receta_id')->nullable()->index('fk_horas_Recetas1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('horas');
	}

}
