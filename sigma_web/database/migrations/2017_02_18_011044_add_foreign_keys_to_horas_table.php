<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHorasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('horas', function(Blueprint $table)
		{
			$table->foreign('receta_id', 'fk_horas_Recetas1')->references('receta_id')->on('recetas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('horas', function(Blueprint $table)
		{
			$table->dropForeign('fk_horas_Recetas1');
		});
	}

}
