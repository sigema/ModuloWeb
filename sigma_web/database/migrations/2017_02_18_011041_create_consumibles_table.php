<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConsumiblesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consumibles', function(Blueprint $table)
		{
			$table->integer('consumible_id', true);
			$table->string('descripcion', 200)->nullable();
			$table->string('cantidad', 45)->nullable();
			$table->string('unidad', 45)->nullable();
			$table->string('precio_unitario', 45)->nullable();
			$table->string('tipo', 45)->nullable();
			$table->enum('consumiblescol', array('filtro','lubricante','elemento_desgaste'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consumibles');
	}

}
