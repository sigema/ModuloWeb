<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMPreventivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_preventivos', function(Blueprint $table)
		{
			$table->increments('mpreventivo_id');
			$table->string('mp');
			$table->string('horometro');
			$table->string('obra_institucion');
			$table->string('nro_reporte');
			$table->string('mecanico');
			$table->date('fecha_ejecución');
			$table->integer('horometro_aprox');
			$table->date('fecha_aprox');
			$table->integer('movimiento_id')->unsigned()->nullable();
            $table->foreign('movimiento_id')->references('movimiento_id')->on('movimientos');
			$table->integer('maquinaria_id')->unsigned();
			$table->foreign('maquinaria_id')->references('maquinaria_id')->on('maquinarias');
			$table->integer('usuario_id')->unsigned();
			$table->foreign('usuario_id')->references('id')->on('users');
			$table->boolean('realizado')->nullable();
			$table->boolean('pase')->nullable();
			$table->boolean('status')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_preventivos');
	}

}
