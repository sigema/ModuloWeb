<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovimientosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movimientos', function(Blueprint $table)
		{
			$table->increments('movimiento_id');
			$table->integer('maquinaria_id')->unsigned();
			$table->foreign('maquinaria_id')->references('maquinaria_id')->on('maquinarias');
			$table->date('fecha_entrada');
			$table->date('fecha_salida');
			$table->text('descripcion');
			$table->string('obra_institucion');
			$table->integer('provincia_id')->unsigned();
			$table->foreign('provincia_id')->references('provincia_id')->on('provincias');
			$table->text('doc_asignacion');
			$table->enum('turno', array('1','2'));
			 $table->timestamps(); 
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movimientos');
	}

}
