<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleSalidaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_salida', function(Blueprint $table)
		{
			$table->increments('detalle_salida_id');
			$table->integer('cantidad');
			$table->integer('salida_id')->unsigned();
			$table->foreign('salida_id')->references('salida_id')->on('salidas');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_salida');
	}

}
