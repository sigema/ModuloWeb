<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaquinariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('maquinarias', function (Blueprint $table) {
            $table->increments('maquinaria_id');
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('tipo')->nullable();
            $table->string('nro_registro')->nullable();
            $table->string('serie')->nullable();
            $table->integer('horometro');
            $table->string('placa')->nullable();
            $table->text('foto')->nullable();
            $table->enum('estado' , ['1','2','3']);
            $table->integer('anio');
            $table->enum('condicion', ['1' , '2']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maquinarias');
    }
}
