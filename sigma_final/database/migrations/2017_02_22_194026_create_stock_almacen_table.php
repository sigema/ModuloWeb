<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockAlmacenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_almacen', function (Blueprint $table) {
            $table->increments('stock_almac_id');
            $table->integer('consumible_almac_id')->unsigned();
            $table->foreign('consumible_almac_id')->references('consumible_almac_id')->on('consumibles_almacen');
            $table->integer('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_almacen');
    }
}
