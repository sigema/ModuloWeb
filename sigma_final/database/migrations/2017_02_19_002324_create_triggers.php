<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER mCorrectivo_Insert AFTER
                        INSERT ON m_correctivos
                        FOR EACH ROW 
                        BEGIN
                        INSERT INTO bitacoras SET accion = "Se inserto un registro", usuario_id = NEW.usuario_id, tabla = "mantenimiento correctivo", maquinaria_id = NEW.maquinaria_id ,created_at = CURRENT_TIMESTAMP ; 
                        END');

        DB::unprepared('CREATE TRIGGER mCorrectivo_Update AFTER 
                        UPDATE ON m_correctivos
                        FOR EACH ROW 
                        BEGIN
                        INSERT INTO bitacoras SET accion = "Se edito un registro", usuario_id = NEW.usuario_id, tabla = "mantenimiento correctivo", maquinaria_id = NEW.maquinaria_id,created_at = CURRENT_TIMESTAMP ; 
                        END');
        DB::unprepared('CREATE TRIGGER mCorrectivo_Delete AFTER 
                        DELETE ON m_correctivos
                        FOR EACH ROW 
                        BEGIN
                        INSERT INTO bitacoras SET accion = "Se elimino un registro", usuario_id = OLD.usuario_id, tabla = "mantenimiento correctivo", maquinaria_id = OLD.maquinaria_id,created_at = CURRENT_TIMESTAMP ; 
                        END');

        DB::unprepared('CREATE TRIGGER mPreventivo_Insert AFTER 
                        INSERT ON m_preventivos
                        FOR EACH ROW 
                        BEGIN
                        INSERT INTO bitacoras SET accion = "Se inserto un registro", usuario_id = NEW.usuario_id, tabla = "mantenimiento preventivo", maquinaria_id = NEW.maquinaria_id,created_at = CURRENT_TIMESTAMP ; 
                        END');

        DB::unprepared('CREATE TRIGGER mPreventivo_Update AFTER 
                        UPDATE ON m_preventivos
                        FOR EACH ROW 
                        BEGIN
                        INSERT INTO bitacoras SET accion = "Se edito un registro", usuario_id = NEW.usuario_id, tabla = "mantenimiento preventivo", maquinaria_id = NEW.maquinaria_id ,created_at = CURRENT_TIMESTAMP ; 
                        END');
        DB::unprepared('CREATE TRIGGER mPreventivo_Delete AFTER 
                        DELETE ON m_preventivos
                        FOR EACH ROW 
                        BEGIN
                        INSERT INTO bitacoras SET accion = "Se elimino un registro", usuario_id = OLD.usuario_id, tabla = "mantenimiento preventivo", maquinaria_id = OLD.maquinaria_id ,created_at = CURRENT_TIMESTAMP ; 
                        END');
    }


    public function down()
    {
        DB::unprepared('DROP TRIGGER `mCorrectivo_Insert`');
        DB::unprepared('DROP TRIGGER `mCorrectivo_Update`');
        DB::unprepared('DROP TRIGGER `mCorrectivo_Delete`');
        DB::unprepared('DROP TRIGGER `mPreventivo_Delete`');
        DB::unprepared('DROP TRIGGER `mPreventivo_Insert`');
        DB::unprepared('DROP TRIGGER `mPreventivo_Update`');
    }
}
