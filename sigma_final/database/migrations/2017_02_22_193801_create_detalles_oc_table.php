<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesOcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_oc', function (Blueprint $table) {
            $table->increments('detalle_oc_id');
            $table->integer('oc_id')->unsigned();
            $table->foreign('oc_id')->references('oc_id')->on('orden_compras');
            $table->integer('consumible_almac_id')->unsigned();
            $table->foreign('consumible_almac_id')->references('consumible_almac_id')->on('consumibles_almacen');
            $table->integer('cantidad');
            $table->decimal('precio_unitario', 10 , 2);
            $table->decimal('importe', 10 , 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_oc');
    }
}
