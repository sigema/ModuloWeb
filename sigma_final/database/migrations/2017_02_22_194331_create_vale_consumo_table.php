<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValeConsumoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vale_consumo', function (Blueprint $table) {
            $table->increments('vale_consumo_id');
            $table->integer('obra_id')->unsigned();
            $table->foreign('obra_id')->references('obra_id')->on('obras');
            $table->date('fecha_salida');
            $table->string('firma');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vale_consumo');
    }
}
