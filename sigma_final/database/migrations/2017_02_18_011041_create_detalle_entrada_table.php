<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleEntradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_entrada', function(Blueprint $table)
		{
			$table->increments('detalle_entrada_id');
			$table->decimal('precio', 10 , 2);
			$table->integer('cantidad');
			$table->integer('entrada_id')->unsigned();
			$table->foreign('entrada_id')->references('entrada_id')->on('entradas');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_entrada');
	}

}
