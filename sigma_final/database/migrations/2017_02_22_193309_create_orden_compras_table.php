<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_compras', function (Blueprint $table) {
            $table->increments('oc_id');
            $table->string('codigo');            
            $table->string('codigo_solicitud');
            $table->date('fecha_ingreso');
            $table->decimal('total',10,2);
            $table->integer('obra_id')->nullable()->unsigned();
            $table->foreign('obra_id')->references('obra_id')->on('obras');             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_compras');
    }
}
