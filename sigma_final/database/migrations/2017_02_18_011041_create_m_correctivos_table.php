<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMCorrectivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_correctivos', function(Blueprint $table)
		{
			$table->increments('mcorrectivo_id');
			$table->integer('maquinaria_id')->unsigned();
			$table->foreign('maquinaria_id')->references('maquinaria_id')->on('maquinarias');
			$table->integer('usuario_id')->unsigned();
			$table->foreign('usuario_id')->references('id')->on('users');
            $table->integer('movimiento_id')->unsigned()->nullable();
            $table->foreign('movimiento_id')->references('movimiento_id')->on('movimientos');
			$table->date('fecha');
			$table->string('mecanico');
			$table->string('reporte_falla');
			$table->string('nro_documento');
			$table->string('obra_institucion');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_correctivos');
	}

}
