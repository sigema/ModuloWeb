<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePecosasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pecosas', function (Blueprint $table) {
            $table->increments('pecosa_id');
            $table->integer('obra_id')->unsigned();
            $table->foreign('obra_id')->references('obra_id')->on('obras');
            $table->string('codigo');
            $table->date('fecha_salida');
            $table->decimal('total',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pecosa');
    }
}
