<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleVcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_vc', function (Blueprint $table) {
            $table->increments('detalle_vc_id');
            $table->integer('consumible_almac_id')->unsigned();
            $table->foreign('consumible_almac_id')->references('consumible_almac_id')->on('consumibles_almacen');
            $table->integer('vale_consumo_id')->unsigned();
            $table->foreign('vale_consumo_id')->references('vale_consumo_id')->on('vale_consumo');
            $table->integer('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_vc');
    }
}
