<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleRecetaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detalle_receta', function(Blueprint $table)
		{
			$table->increments('detalle_receta_id');
			$table->integer('cantidad')->nullable();
			$table->integer('consumible_id')->unsigned();
			$table->foreign('consumible_id')->references('consumible_almac_id')->on('consumibles_almacen');
			$table->integer('receta_id')->unsigned();
			$table->foreign('receta_id')->references('receta_id')->on('recetas');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detalle_receta');
	}

}
