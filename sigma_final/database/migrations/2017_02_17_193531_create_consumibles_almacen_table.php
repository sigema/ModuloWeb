<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumiblesAlmacenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumibles_almacen', function (Blueprint $table) {
            $table->increments('consumible_almac_id');
            $table->string('codigo');
            $table->text('descripcion');
            $table->string('unidad');
            $table->string('marca')->nullable();           
            $table->decimal('precio_ref', 10 , 2);
            $table->enum('tipo', ['1' , '2' , '3']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumible_almacen');
    }
}
