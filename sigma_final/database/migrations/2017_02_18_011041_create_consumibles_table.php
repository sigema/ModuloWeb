<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConsumiblesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consumibles', function(Blueprint $table)
		{
			$table->increments('consumible_id');
			$table->string('descripcion');
			$table->integer('cantidad');
			$table->string('unidad');
			$table->string('precio_unitario')->nullable();
			$table->enum('tipo', ['filtro','lubricante','elemento_desgaste']);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consumibles');
	}

}
