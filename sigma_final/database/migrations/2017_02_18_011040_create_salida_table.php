<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalidaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('salidas', function(Blueprint $table)
		{
			$table->increments('salida_id');
			$table->date('fecha');
		});
	}


	/**
	 * Reverse the migratioss.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('salidas');
	}

}
