<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes', function (Blueprint $table) {
            $table->increments('comprobante_id');
            $table->string('num_comprobante');
            $table->enum('tipo', array('1','2'));
            $table->integer('oc_id')->unsigned();
            $table->foreign('oc_id')->references('oc_id')->on('orden_compras');
            $table->integer('proveedor_id')->unsigned();
            $table->foreign('proveedor_id')->references('proveedor_id')->on('proveedores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes');
    }
}
