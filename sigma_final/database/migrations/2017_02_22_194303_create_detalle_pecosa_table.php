<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallePecosaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_pecosa', function (Blueprint $table) {
            $table->increments('detalle_pecosa_id');
            $table->integer('pecosa_id')->unsigned();
            $table->foreign('pecosa_id')->references('pecosa_id')->on('pecosas');
            $table->integer('detalle_oc_id')->unsigned();
            $table->foreign('detalle_oc_id')->references('detalle_oc_id')->on('detalle_oc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_pecosa');
    }
}
