const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js([
	'resources/assets/js/app.js',
	'node_modules/datatables.net/js/jquery.dataTables.js',
	'node_modules/datatables.net-buttons/js/dataTables.buttons.js',
	'node_modules/datatables.net-buttons/js/buttons.flash.js',
	'node_modules/jszip/dist/jszip.js',
 	'node_modules/pdfmake/build/pdfmake.min.js',
 	'node_modules/pdfmake/build/vfs_fonts.js',
 	'node_modules/datatables.net-buttons/js/buttons.html5.js',
 	'node_modules/datatables.net-buttons/js/buttons.print.js'


	] , 'public/js/app.js')
	.sass('resources/assets/sass/app.scss', 'public/css');
//mix.js(['node_modules/datatables.net/js/jquery.dataTables.js'] , 'public/app.js')
//mix.copy('node_modules/pdfmake/build/pdfmake.js' , 'public/js/pdfmake.js');
//mix.copy('node_modules/pdfmake/build/vfs-fonts.js' , 'public/js/vfs-fonts.js');

mix.copy('node_modules/components-font-awesome/fonts/*' , 'public/fonts');

