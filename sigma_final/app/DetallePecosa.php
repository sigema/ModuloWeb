<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePecosa extends Model
{
    protected $table = 'detalle_pecosa';
    protected $primaryKey = 'detalle_pecosa_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'detalle_pecosa_id',
        'pecosa_id',
        'detalle_oc_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
