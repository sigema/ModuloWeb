<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleValeConsumo extends Model
{
    protected $table = 'detalle_vc';
    protected $primaryKey = 'detalle_vc_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'detalle_vc_id',
        'consumible_almac_id',
        'vale_consumo_id',
        'cantidad'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
