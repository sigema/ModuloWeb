<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pecosa extends Model
{
    protected $table = 'pecosas';
    protected $primaryKey = 'pecosa_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pecosa_id',
        'obra_id',
        'codigo',
        'fecha_salida',
        'total'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function detalles_pecosa(){
        return $this->hasMany('App\DetallePecosa','pecosa_id');
    }
}
