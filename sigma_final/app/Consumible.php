<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumible extends Model
{
    
	protected $primaryKey = 'consumible_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'decripcion',
        'cantidad ',
        'unidad',
        'precio_unitario',
        'tipo',
        'consumible_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function detalleReceta(){
        return $this->hasOne('App\DetalleReceta' , 'consumible_id');
    }
}
