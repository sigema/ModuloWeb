<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Provincia extends Model
{
    protected $primaryKey = 'provincia_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provincia_id',
        'nombre',
        'latitud',
        'longitud'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function movimientos(){
        return $this->hasMany('App\Movimiento' , 'provincia_id');
    }

    public static function maquinarias_agrupadas_sin_movimientos() {
      return DB::select( DB::raw("select count(*), m.tipo FROM maquinarias m WHERE m.maquinaria_id NOT IN (select mo.maquinaria_id FROM movimientos mo) GROUP BY m.tipo"), array() );
    }

    public static function maquinarias_sin_movimientos() {
      return DB::select( DB::raw("select m.maquinaria_id, m.marca, m.modelo, m.tipo, m.nro_registro, m.serie, m.horometro, m.foto, m.estado, m.anio, m.condicion, '1' as provincia_id, 'Arequipa' as nombre, '-16.40849600' as latitud, '-71.53815400' as longitud FROM maquinarias m WHERE m.maquinaria_id NOT IN (select mo.maquinaria_id FROM movimientos mo)"), array() );
    }

    public static function maquinarias_agrupadas_despues_salida($provincia_id) {
      return DB::select( DB::raw("select count(*), m.tipo FROM maquinarias m, movimientos mo, provincias p WHERE p.provincia_id=mo.provincia_id AND m.maquinaria_id=mo.maquinaria_id AND DATE(now()) > DATE(mo.fecha_salida) AND p.provincia_id = :provincia_id GROUP BY m.tipo"), array('provincia_id'=>$provincia_id) );
    }

    public static function maquinarias_despues_salida($provincia_id) {
      return DB::select( DB::raw("select * FROM maquinarias m, movimientos mo, provincias p WHERE p.provincia_id=mo.provincia_id AND m.maquinaria_id=mo.maquinaria_id AND DATE(now()) > DATE(mo.fecha_salida) AND p.provincia_id = :provincia_id"), array('provincia_id'=>$provincia_id) );
    }

    public static function maquinarias_agrupadas($provincia_id) {
      return DB::select( DB::raw("select count(*), m.tipo FROM maquinarias m, movimientos mo, provincias p WHERE p.provincia_id=mo.provincia_id AND m.maquinaria_id=mo.maquinaria_id AND DATE(now())>=DATE(mo.fecha_entrada) AND DATE(now()) < DATE(mo.fecha_salida) AND p.provincia_id = :provincia_id GROUP BY m.tipo"), array('provincia_id'=>$provincia_id) );
    }
    public static function maquinarias($provincia_id) {
//      if ($provincia_id != 1)
        return DB::select( DB::raw("select * FROM maquinarias m, movimientos mo, provincias p WHERE p.provincia_id=mo.provincia_id AND m.maquinaria_id=mo.maquinaria_id AND DATE(now()) >= DATE(mo.fecha_entrada) AND DATE(now()) < DATE(mo.fecha_salida) AND p.provincia_id = :provincia_id"), array('provincia_id'=>$provincia_id) );
/*      else {
        $query = "";
        $query.= "(select m.maquinaria_id,m.marca,m.modelo,m.tipo,m.nro_registro,m.serie,m.horometro,m.foto,m.estado,m.anio,m.condicion, p.provincia_id,p.nombre,p.latitud,p.longitud ";
        $query.= "FROM maquinarias m, movimientos mo, provincias p ";
        $query.= "WHERE p.provincia_id=mo.provincia_id AND m.maquinaria_id=mo.maquinaria_id ";
        $query.= "AND DATE(now())>DATE(mo.fecha_entrada) AND DATE(now()) < DATE(mo.fecha_salida) ";
        $query.= "AND p.provincia_id = :provincia_id) ";
        $query.= "UNION ";
        $query.= "(select m.maquinaria_id,m.marca,m.modelo,m.tipo,m.nro_registro,m.serie,m.horometro,m.foto,m.estado,m.anio,m.condicion, p.provincia_id,p.nombre,p.latitud,p.longitud ";
        $query.= "FROM (maquinarias m LEFT JOIN movimientos mo ON m.maquinaria_id=mo.maquinaria_id) LEFT JOIN provincias p ON mo.provincia_id=p.provincia_id ";
        $query.= "WHERE mo.movimiento_id IS NULL)";
        $query.= "";
        $query.= "";
        $query.= "";
        return DB::select( DB::raw($query, array('provincia_id'=>$provincia_id)));
        }*/
    }
}
