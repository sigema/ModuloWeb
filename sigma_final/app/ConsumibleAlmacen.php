<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumibleAlmacen extends Model
{
    protected $table = 'consumibles_almacen';
	protected $primaryKey = 'consumible_almac_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'consumible_almac_id',
        'codigo',
        'descripcion',
        'unidad',
        'marca',
        'precio_ref',
        'tipo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function detalleReceta(){
        return $this->hasOne('App\DetalleReceta' , 'consumible_id');
    }

    public function detalles_oc(){
        return $this->hasMany('App\DetalleOrdenCompra','consumible_almac_id');
    }

    public function stock_almacen(){
        return $this->hasOne('App\StockAlmacen','consumible_almac_id');
    }

    public function detalles_vale(){
        return $this->hasMany('App\DetalleVale','consumible_almac_id');
    }
}
