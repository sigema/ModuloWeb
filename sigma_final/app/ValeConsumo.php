<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValeConsumo extends Model
{
    protected $table = 'vale_consumo';
    protected $primaryKey = 'vale_consumo_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vale_consumo_id',
        'obra_id',
        'fecha_salida',
        'firma'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function detalles_vale(){
        return $this->hasMany('App\DetalleVale','vale_consumo_id');
    }
}
