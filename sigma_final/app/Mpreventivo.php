<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mpreventivo extends Model
{
    protected $table = 'm_preventivos';

    protected $primaryKey = 'mpreventivo_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mp',
        'obra_institucion',
        'nro_reporte',
        'mecanico',
        'fecha_ejecución',
        'horometro_aprox',
        'fecha_aprox',
        'maquinaria_id',
        'usuario_id',
        'realizado',
        'pase',
        'mpreventivo_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function maquinaria(){
        return $this->belongsTo('App\Maquinaria' , 'maquinaria_id');
    }
}
