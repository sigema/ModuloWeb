<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenCompra extends Model
{
    protected $table = 'detalle_oc';
    protected $primaryKey = 'detalle_oc_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'detalle_oc_id',
        'oc_id',
        'consumible_almac_id',
        'cantidad',
        'precio_unitario',
        'importe'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function detalle_pecosa(){
        return $this->hasOne('App\DetallePecosa','detalle_oc_id');
    }
}
