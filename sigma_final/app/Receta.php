<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    

	protected $primaryKey = 'receta_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'periodo',
        'horas_trabajo',
        'receta_id',
        'maquinaria_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function maquinaria(){
        return $this->belongsTo('App\Maquinaria' , 'maquinaria_id');
    }

    public function detalleRecetas(){
        return $this->hasMany('App\DetalleReceta' , 'receta_id');
    }

}
