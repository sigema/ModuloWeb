<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    protected $table = 'movimientos';

    protected $primaryKey = 'movimiento_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'movimiento_id',
        'maquinaria_id',
        'fecha_entrada',
        'fecha_salida',
        'descripcion',
        'obra_institucion',
        'provincia_id',
        'doc_asignacion',
        'turno'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function maquinaria(){
        return $this->belongsTo('App\Maquinaria' , 'maquinaria_id');
    }


}
