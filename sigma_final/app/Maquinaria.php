<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Maquinaria extends Model
{
    protected $primaryKey = 'maquinaria_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'marca',
        'modelo',
        'tipo',
        'nro_registro',
        'serie',
        'estado',
        'horometro',
        'condicion',
        'foto',
        'anio',
        'placa',
        'maquinaria_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function mpreventivos(){
        return $this->hasMany('App\Mpreventivo' , 'maquinaria_id');
    }

    public function mcorrectivos(){
        return $this->hasMany('App\Mcorrectivo' , 'maquinaria_id');
    }

    public function receta(){
        return $this->hasOne('App\Receta' , 'maquinaria_id');
    }

    public function movimientos(){
        return $this->hasMany('App\Movimiento' , 'maquinaria_id');
    }

    public static function ubicacion($serie){
      return DB::select( DB::raw("select * FROM maquinarias m, movimientos mo, provincias p WHERE p.provincia_id=mo.provincia_id AND m.maquinaria_id=mo.maquinaria_id AND DATE(now())>DATE(mo.fecha_entrada) AND DATE(now()) < DATE(mo.fecha_salida) AND m.serie = :serie"), array('serie'=>$serie) );
    }
    public static function ubicaciones(){
        return DB::select("select * FROM maquinarias m, movimientos mo, provincias p WHERE p.provincia_id=mo.provincia_id AND m.maquinaria_id=mo.maquinaria_id AND DATE(now())>DATE(mo.fecha_entrada) AND DATE(now()) < DATE(mo.fecha_salida)");
    }
}
