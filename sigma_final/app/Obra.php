<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obra extends Model
{
    protected $table = 'obras';

    protected $primaryKey = 'obra_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'obra_id',
        'num_meta',
        'descripcion',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function orden_compra(){
        return $this->hasMany('App\OrdenCompra','obra_id');
    }

    public function pecosas(){
        return $this->hasMany('App\Pecosa','obra_id');
    }

    public function vales_consumo(){
        return $this->hasMany('App\ValeConsumo','obra_id');
    }
}