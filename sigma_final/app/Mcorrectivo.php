<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mcorrectivo extends Model
{
    protected $table = 'm_correctivos';

    protected $primaryKey = 'mcorrectivo_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'obra_institucion',
        'fecha',
        'mecanico',
        'reporte_falla',
        'nro_documento',
        'maquinaria_id',
        'usuario_id',
        'mcorrectivo_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function maquinaria(){
        return $this->belongsTo('App\Maquinaria' , 'maquinaria_id');
    }
}
