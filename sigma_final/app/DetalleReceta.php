<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleReceta extends Model
{

	protected $table = 'detalle_receta';
    protected $primaryKey = 'detalle_receta_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cantidad',
        'hora_id ',
        'consumible_id',
        'receta_id',
        'detalle_receta_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function consumible(){
        return $this->belongsTo('App\ConsumibleAlmacen' , 'consumible_id');
    }


    public function receta(){
        return $this->belongsTo('App\Receta' , 'receta_id');
    }
}
