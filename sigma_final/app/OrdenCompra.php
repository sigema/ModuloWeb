<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
    protected $table = 'orden_compras';
    protected $primaryKey = 'oc_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'oc_id',
        'codigo',
        'codigo_solicitud',
        'fecha_ingreso',
        'total',
        'obra_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    
    public function comprobantes(){
        return $this->hasMany('App\Comprobante','oc_id');
    }

    public function detalles_oc(){
        return $this->hasMany('App\DetalleOrdenCompra','oc_id');
    }
}
