<?php

namespace App\Http\Controllers;

use App\Pecosa;
use App\Obra;
use App\DetallePecosa;
use App\StockAlmacen;
use App\ConsumibleAlmacen;
use App\DetalleOrdenCompra;
use App\OrdenCompra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class PecosasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ocs = DB::table('obras')
                ->join('pecosas as p','p.obra_id','=','obras.obra_id')
                //->get()
                ->paginate(10);
        return view('pecosas.index', ['pecosas' => $ocs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pecosas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pecosa = new Pecosa();
        $pecosa->obra_id = $request->get('obra_id');
        $pecosa->codigo = $request->get('codigo');
        $pecosa->fecha_salida = $request->get('fecha');
        $pecosa->total = $request->get('total');
        $pecosa->save();

        $pecosa_id = $pecosa->pecosa_id;

        $detalles = $request->get('detalle_oc');
        foreach ($detalles as $value) {
            $detalle = new DetallePecosa();
            $detalle->pecosa_id = $pecosa_id;
            $detalle->detalle_oc_id = $value['detalle_oc_id'];
            $detalle->save();

            $stock = StockAlmacen::where('consumible_almac_id', '=', $value['consumible_almac_id'])->first();
            if (!empty($stock)) {
                $stock->cantidad = $stock->cantidad - $value['cantidad'];
                $stock->save();
            } else {
                $stock = new StockAlmacen();
                $stock->consumible_almac_id = $value['consumible_almac_id'];
                $stock->cantidad = 0;
                $stock->save();
            }
        }
        return Response::json(['data' => 'saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pecosa = Pecosa::findOrFail($id);
        $obra = Obra::where('obra_id',$pecosa->obra_id)->first();        
        $detalle_pecosa =DB::table('detalle_oc as d_oc')
                        ->join('detalle_pecosa as d_pecosa','d_pecosa.detalle_oc_id','=','d_oc.detalle_oc_id')
                        ->join('consumibles_almacen as ca','ca.consumible_almac_id','=','d_oc.consumible_almac_id')
                        ->where('d_pecosa.pecosa_id',$pecosa->pecosa_id)
                        ->get();
        
        //echo json_encode($detalle_pecosa);
        // $ocs = DB::table('obras')
        // echo json_encode($detalle_pecosa);
        //echo json_encode($detalle_pecosa->oc_id);
        

        // ->join('detalle_oc as d_oc','d_oc.consumible_almac_id','=','ca.consumible_almac_id')
        // ->where('d_oc.oc_id',$oc->oc_id)
        // ->get();
        //         ->join('pecosas as p','p.obra_id','=','obras.obra_id')
        //         ->get()
        // return view('pecosas.show' , ['pecosa' => $pecosa, 'obra'=> $obra]);
        return view('pecosas.show' , ['pecosa' => $pecosa, 'obra'=> $obra, 'd_pecosa'=>$detalle_pecosa]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
