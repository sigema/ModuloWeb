<?php

namespace App\Http\Controllers;

use App\Mpreventivo;
use App\Maquinaria;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MpreventivosController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkStatusMPreventivo')->only('edit' , 'update' , 'destroy');
   
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        
        //return $mpreventivo;
        return Mpreventivo::where('mpreventivo_id', $id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('mpreventivos.create' , [ 'maquinaria' => Maquinaria::findOrFail($id) ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $idMaquinaria)
    {

        $this->validate($request , [
            'mp' => 'required:numeric',
            'obra_institucion' => 'required',
            'nro_reporte'=> 'required',
            'mecanico' => 'required',
            'fecha_ejecución' => 'required|date',
                  
        ]);



        $maquinaria = Maquinaria::findOrFail($idMaquinaria);
        $mpreventivo = new Mpreventivo();

        if(!($request->get('mp') > $maquinaria->horometro)){
            return redirect()->back()->with('flash_message' , 'El mp debe ser mayor al horometro')->withInput();
        }

        $mpreventivo->mp                = $request->get('mp');
        $mpreventivo->obra_institucion  = $request->get('obra_institucion');
        $mpreventivo->horometro         = $mpreventivo->mp - $maquinaria->horometro;
        $mpreventivo->nro_reporte       = $request->get('nro_reporte');
        $mpreventivo->mecanico          = $request->get('mecanico');
        $mpreventivo->fecha_ejecución   = $request->get('fecha_ejecución');
        $mpreventivo->maquinaria_id     = $idMaquinaria;
        $mpreventivo->usuario_id        = Auth::user()->id;
        $mpreventivo->realizado         = $request->get('realizado');
        $mpreventivo->pase              = $request->get('pase');
        
        $mpreventivo->horometro_aprox   = $mpreventivo->horometro + $maquinaria->horometro;
        $date = Carbon::parse($mpreventivo->fecha_ejecución);
        $mpreventivo->fecha_aprox       = $date->addMonth(); 
        
        if($mpreventivo->realizado != null){
            $mpreventivo->status = true;
            $maquinaria->horometro = $mpreventivo->horometro + $maquinaria->horometro;
            $maquinaria->save();
        }
        $mpreventivo->save();
            
        return redirect()->route('maquinarias.show' , $idMaquinaria);
        
        

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idMaquinaria , $id)
    {
        //$this->middleware('checkStatusMPreventivo');
        $mpreventivo = Mpreventivo::where('maquinaria_id' , $idMaquinaria)->where('mpreventivo_id' , $id)->first();
        $maquinaria = $mpreventivo->maquinaria;
        return view('mpreventivos.edit' , ['mp' => $mpreventivo , 'maquinaria' => $maquinaria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMaquinaria, $id)
    {
        //$this->middleware('checkStatusMPreventivo');
         $this->validate($request , [
            'mp' => 'required:numeric',
            'obra_institucion' => 'required',
            'nro_reporte'=> 'required',
            'mecanico' => 'required',
            'fecha_ejecución' => 'required|date',
        ]);
        $maquinaria = Maquinaria::findOrFail($idMaquinaria);
        $mpreventivo = Mpreventivo::where('maquinaria_id' , $idMaquinaria)->where('mpreventivo_id' , $id)->first();

        if(!($request->get('mp') > $maquinaria->horometro)){
            return redirect()->back()->with('flash_message' , 'El mp debe ser mayor al horometro')->withInput();;
        }

        $oldRealizado = $mpreventivo->realizado;
        $mpreventivo->mp                = $request->get('mp');
        
        $mpreventivo->obra_institucion  = $request->get('obra_institucion');
        $mpreventivo->nro_reporte       = $request->get('nro_reporte');
        $mpreventivo->mecanico          = $request->get('mecanico');
        $mpreventivo->fecha_ejecución   = $request->get('fecha_ejecución');
        $mpreventivo->maquinaria_id     = $idMaquinaria;
        $mpreventivo->usuario_id        = Auth::user()->id;
        $mpreventivo->realizado         = $request->get('realizado');
        $mpreventivo->pase              = $request->get('pase');
        $date = Carbon::parse($mpreventivo->fecha_ejecución);
        $mpreventivo->fecha_aprox       = $date->addMonth(); 
        
        
        if($oldRealizado != null){
            $maquinaria->horometro = $maquinaria->horometro - $mpreventivo->horometro;
        }
        $mpreventivo->horometro         = $mpreventivo->mp - $maquinaria->horometro;
        if($mpreventivo->realizado != null){
            $mpreventivo->status = true;
            $maquinaria->horometro = $maquinaria->horometro + $mpreventivo->horometro;    
             
        }
        if($mpreventivo->mp == $maquinaria->horometro){
            $mpreventivo->horometro_aprox   = $mpreventivo->mp;
        }else{
            $mpreventivo->horometro_aprox   = $mpreventivo->horometro + $maquinaria->horometro;
        }
        $maquinaria->save();
        $mpreventivo->save();  
        
        //$mpreventivo->horometro         = $mpreventivo->mp - $maquinaria->horometro;
        //
        
        
        return redirect()->route('maquinarias.show' , $idMaquinaria);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idMaquinaria , $id)
    {
        $maquinaria = Maquinaria::findOrFail($idMaquinaria);
        $mpreventivo = Mpreventivo::where('maquinaria_id' , $idMaquinaria)->where('mpreventivo_id' , $id)->first();
        if($mpreventivo->realizado != null){
            $maquinaria->horometro = $maquinaria->horometro - $mpreventivo->horometro;
            $maquinaria->save();
        }
        
        $mpreventivo->delete();
        

        

        return redirect()->route('maquinarias.show' , $idMaquinaria);
    }
}
