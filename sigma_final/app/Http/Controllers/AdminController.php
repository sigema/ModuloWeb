<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Bitacora;
use Illuminate\Support\Facades\DB;
use PDF;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $bitacoras = DB::table('bitacoras')
            ->join('users as u','u.id','=','Bitacoras.usuario_id')
            ->join('maquinarias as m','m.maquinaria_id','=','Bitacoras.maquinaria_id')
            ->select('bitacoras.*','u.name','m.nro_registro')
            ->get();



        return view('perfil.index', ['usuarios' => User::all(),'bitacoras' => $bitacoras]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perfil.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $usuario = new User();
        $usuario->name              = $request->get('usuario_name');
        $usuario->email              = $request->get('usuario_email');
        $usuario->password              = bcrypt($request->get('usuario_password'));
        $usuario->tipo              = $request->get('usuario_tipo');
        $usuario->save();
        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('perfil.edit',['usuario' => User::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMaquinaria)
    {
        $usuario = User::findorFail($idMaquinaria);
        $usuario->name              = $request->get('usuario_name');
        $usuario->email             = $request->get('usuario_email');
        $usuario->password          = bcrypt($request->get('usuario_password'));
        $usuario->tipo              = $request->get('usuario_tipo');
        $usuario->save();
        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $usuario = User::findorFail($id);
        $usuario->delete();
        return redirect()->route('admin.index');
    }

}
