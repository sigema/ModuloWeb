<?php

namespace App\Http\Controllers;

use App\Obra;
use Illuminate\Http\Request;
use Response;

class ObrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('obras.index', ['obras'=>Obra::all()]);
    }

    public function search($term)
    {
      $obras = Obra::where('num_meta', 'like', '%'.$term.'%')
        ->orWhere('descripcion', 'like', '%'.$term.'%')
        ->get();
      return Response::json(['items' => $obras]);
    }

    public function save(Request $request)
    {
        $obra = new Obra();
        $obra->num_meta = $request->input('num_meta');
        $obra->descripcion = $request->input('descripcion');
        $obra->save();
        return Response::json(['result'=>$obra], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('obras.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obra = new Obra();
        $obra->num_meta = $request->get('num_meta');
        $obra->descripcion = $request->get('descripcion');
        $obra->save();
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obra = Obra::where('obra_id',$id)->first();
        return view('obras.edit', ['obra'=> $obra]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $obra = Obra::findOrFail($id);
        $obra->num_meta = $request->get('num_meta');
        $obra->descripcion = $request->get('descripcion');
        $obra->save();
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obra = Obra::findOrFail($id);
        $obra->delete();
        return $this->index();

    }
}
