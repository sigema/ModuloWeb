<?php

namespace App\Http\Controllers;

use App\Maquinaria;
use App\Movimiento;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mpreventivo;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class MaquinariasController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function data()
    {
       return [ 'data' => Maquinaria::all()];
        //return Maquinaria::all();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     // $maquinarias = Maquinaria::ubicaciones();

        return view('maquinaria.index' , ['maquinarias' => Maquinaria::all()]);
       //return Maquinaria::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('maquinaria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request , [
            'marca'         => 'required',
            'modelo'        => 'required',
            'tipo'          => 'required',
            'nro_registro'  => 'required|unique:maquinarias',
            'serie'         => 'required',
            'estado'        => 'required',
            'horometro'     => 'required|numeric',
            'condicion'     => 'required',
            'foto'          => 'mimes:jpeg,png',
            'anio'          => 'required|numeric',
            'placa'         => 'required'
        ]);

        
        if($request->hasFile('foto')){


            $image = $request->foto;
            $extension = $request->foto->getClientOriginalExtension();
            
            $filename  = '/images/' . sha1(Carbon::now()) . '.' . $extension;

            $path = public_path($filename);
             
            Image::make($image->getRealPath())->resize(300, 200)->save($path);
        }else{
            $filename = '/images/default-image.jpg';
        }


        $maquinaria = new Maquinaria();
        $maquinaria->marca          = $request->get('marca');
        $maquinaria->modelo         = $request->get('modelo');
        $maquinaria->tipo           = $request->get('tipo');
        $maquinaria->nro_registro   = $request->get('nro_registro');
        $maquinaria->serie          = $request->get('serie');
        $maquinaria->estado         = $request->get('estado');    
        $maquinaria->horometro      = $request->get('horometro');
        $maquinaria->condicion      = $request->get('condicion');
        $maquinaria->anio           = $request->get('anio');
        $maquinaria->placa          = $request->get('placa');
        $maquinaria->foto           = ($filename != null) ? $filename : '' ;
        $maquinaria->save();
        //Session::flash('flash_message', 'Task successfully deleted!');
        return redirect()->route('maquinarias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $maquinaria = Maquinaria::findOrFail($id);
        $mp = $maquinaria->mpreventivos;
        $mc = $maquinaria->mcorrectivos;
        $receta = $maquinaria->receta;
        $mov = $maquinaria->movimientos;
        $mov = Movimiento::join('provincias as p','p.provincia_id','=','movimientos.provincia_id')
            ->select('movimientos.*','p.nombre')
            ->where('movimientos.maquinaria_id','=',$id)
            ->orderBy('fecha_entrada')
            ->get();
        $lastmpreventivo = Mpreventivo::where('maquinaria_id' , $id)->where('realizado' , true)->get()->last();
        $colorEstate = '';
        if($lastmpreventivo != null){
            $fecha_aprox = Carbon::parse($lastmpreventivo->fecha_aprox);
            if( $fecha_aprox->lt(Carbon::now())){
                $colorEstate = 'danger';
            }else if($fecha_aprox->subDays(5)->lte(Carbon::now())){
                $colorEstate = 'warning';
            }else if ($fecha_aprox->gt(Carbon::now())){
                $colorEstate = 'success';
            }
        }
        

        return view('maquinaria.show' , ['maquinaria' => $maquinaria , 'mps' => $mp , 'mcs' => $mc , 'receta' => $receta , 'mov' => $mov , 'state' => $colorEstate ]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('maquinaria.edit' , ['maquinaria' => Maquinaria::findOrFail($id)]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request , [
            'marca'         => 'required',
            'modelo'        => 'required',
            'tipo'          => 'required',
            'nro_registro'  => 'required',
            'serie'         => 'required',
            'estado'        => 'required',
            'horometro'     => 'required|numeric',
            'condicion'     => 'required',
            'foto'          => 'mimes:jpeg,png',
            'anio'          => 'required|numeric',
            'placa'         => 'required'
        ]);


        

        $maquinaria = Maquinaria::findOrFail($id);
        $maquinaria->marca          = $request->get('marca');
        $maquinaria->modelo         = $request->get('modelo');
        $maquinaria->tipo           = $request->get('tipo');
        $maquinaria->nro_registro   = $request->get('nro_registro');
        $maquinaria->serie          = $request->get('serie');
        $maquinaria->estado         = $request->get('estado');    
        $maquinaria->horometro      = $request->get('horometro');
        $maquinaria->condicion      = $request->get('condicion');
        $maquinaria->anio           = $request->get('anio');
        $maquinaria->placa          = $request->get('placa');
        if($request->hasFile('foto')){
            $image = $request->foto;
            $extension = $request->foto->getClientOriginalExtension();
            $filename  = '/images/' . sha1(Carbon::now()) . '.' . $extension;
            $path = public_path($filename);
            $rutaAnterior = public_path($maquinaria->foto);
            if (file_exists($rutaAnterior))
            {
                unlink (realpath($rutaAnterior));

            }
            Image::make($image->getRealPath())->resize(300, 200)->save($path);
            $maquinaria->foto           = ($filename != null) ? $filename : '' ;
        }else{
            $maquinaria->foto = $maquinaria->foto;
        }
        
        $maquinaria->save();
        return redirect()->route('maquinarias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $maquinaria = Maquinaria::findOrFail($id);
        $ruta = public_path($maquinaria->foto);

        if (file_exists($ruta))
        {
            unlink (realpath($ruta));
        }
        $maquinaria->delete();
        return redirect()->route('maquinarias.index');
    }
}
