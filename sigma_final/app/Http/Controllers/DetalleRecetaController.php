<?php

namespace App\Http\Controllers;

use App\DetalleReceta;
//use App\Hora;
use App\Maquinaria;
use App\Receta;
use App\ConsumibleAlmacen;
use Illuminate\Http\Request;

class DetalleRecetaController extends Controller
{

	public function create($idMaquinaria , $idReceta)
    {
    	$maquinaria = Maquinaria::findOrFail($idMaquinaria);
    	$receta = Receta::findOrFail($idReceta);
    	//$horas = Hora::all();
    	$consumibles = ConsumibleAlmacen::all();
    	return view('detallereceta.create', [
    			'maquinaria' => $maquinaria,
    			'receta' => $receta,
    		
    			'consumibles' => $consumibles
    		]);
        
    }



    public function store(Request $request)
    {
    	
    	//return $request->get('hora250');
    	$maquinaria = $request->get('maquinaria');
    	//$horas = Hora::all();
    	$existConsumible = DetalleReceta::where('consumible_id',  $request->get('consumible'))->where('receta_id' , $request->get('receta'))->first();
    	if(!$existConsumible){
    		for($i = 250; $i <= 5000 ; $i+=250){
	    		$detalle = new DetalleReceta();
	    		$detalle->receta_id = $request->get('receta');
	    		$detalle->consumible_id = $request->get('consumible');
	    		$cantidad = (string) 'hora' . $i;
	    		$detalle->cantidad = ($request->get($cantidad) != null) ? $request->get($cantidad) : 0 ;
	    		$detalle->save();
	    	
    		}
    		return redirect()->route('maquinarias.receta.index' , ['maquinaria' => $maquinaria ]);
    	}else{

    		return redirect()->back()->with('flash_message','Este consumible  con sus horas respectivas ya se agrego');
    	}

    	
    	
    	
    	 
        
        
    }
    public function destroy(Request $request){
    	$maquinaria = $request->get('maquinaria');
    	$receta = $request->get('receta');
    	$consumible = $request->get('consumible');
    	$detalles = DetalleReceta::where('receta_id' , $receta)->where('consumible_id', $consumible)->get(); 
    	foreach($detalles as $detalle){
    		$detalle->delete();
    	}
    	return redirect()->route('maquinarias.receta.index' , ['maquinaria' => $maquinaria ]);

    }
}
