<?php 
 
namespace App\Http\Controllers; 
 
use App\Movimiento; 
use App\Maquinaria;
use App\Provincia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
 
class MovimientosController extends Controller 
{ 
    /** 
     * Display a listing of the resource. 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function index() 
    { 
 
    } 
 
    /** 
     * Show the form for creating a new resource. 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function create($idMaquinaria) 
    {

        $provincias= Provincia::select('nombre','provincia_id')->get();
        return view('movimientos.create', [ 'maquinaria' => Maquinaria::findOrFail($idMaquinaria),'provincias' =>$provincias]);
    } 
 
    /** 
     * Store a newly created resource in storage. 
     * 
     * @param  \Illuminate\Http\Request  $request 
     * @return \Illuminate\Http\Response 
     */ 
    public function store(Request $request, $idMaquinaria) 
    {

        $this->validate($request , [
            'fecha_entrada'     => 'required',
            'fecha_salida'      => 'required',
            'descripcion'       => 'required',
            'obra_institucion'  => 'required',
            'provincia_id'      => 'required',
            'doc_asignacion'    => 'required',
            'turno'             => 'required',

        ]);

        $maquinaria = Maquinaria::findOrFail($idMaquinaria); 
 
        $movimiento = new Movimiento(); 
        $movimiento->maquinaria_id = $idMaquinaria; 
        $movimiento->fecha_entrada = $request->get('fecha_entrada'); 
        $movimiento->fecha_salida = $request->get('fecha_salida'); 
        $movimiento->descripcion = $request->get('descripcion'); 
        $movimiento->obra_institucion  = $request->get('obra_institucion'); 
        $movimiento->provincia_id  = $request->get('provincia_id')+1;
        $movimiento->doc_asignacion  = $request->get('doc_asignacion'); 
        $movimiento->turno  = $request->get('turno'); 
 
        $movimiento->save(); 
        return redirect()->route('maquinarias.show' , $idMaquinaria); 
    } 
 
    /** 
     * Display the specified resource. 
     * 
     * @param  \App\Movimiento  $movimiento 
     * @return \Illuminate\Http\Response 
     */ 
    public function show(Movimiento $movimiento) 
    { 
        // 
    } 
 
    /** 
     * Show the form for editing the specified resource. 
     * 
     * @param  \App\Movimiento  $movimiento 
     * @return \Illuminate\Http\Response 
     */ 
    public function edit($idMaquinaria , $id) 
    {
        $provincias= Provincia::select('nombre','provincia_id')->get();
        $movimiento = Movimiento::where('maquinaria_id',$idMaquinaria)->where('movimiento_id',$id)->first(); 
        $maquinaria = $movimiento->maquinaria;
        return view('movimientos.edit', ['mov'=> $movimiento,'provincias' =>$provincias ,'maquinaria' => $maquinaria]);
    } 
 
    /** 
     * Update the specified resource in storage. 
     * 
     * @param  \Illuminate\Http\Request  $request 
     * @param  \App\Movimiento  $movimiento 
     * @return \Illuminate\Http\Response 
     */ 
    public function update(Request $request, $idMaquinaria, $id) 
    {

        $this->validate($request , [
            'fecha_entrada'     => 'required',
            'fecha_salida'      => 'required',
            'descripcion'       => 'required',
            'obra_institucion'  => 'required',
            'provincia_id'      => 'required',
            'doc_asignacion'    => 'required',
            'turno'             => 'required',

        ]);
 
        $movimiento = Movimiento::where('maquinaria_id',$idMaquinaria)->where('movimiento_id',$id)->first(); 
        $movimiento->maquinaria_id = $idMaquinaria; 
        $movimiento->fecha_entrada = $request->get('fecha_entrada'); 
        $movimiento->fecha_salida = $request->get('fecha_salida'); 
        $movimiento->descripcion = $request->get('descripcion'); 
        $movimiento->obra_institucion  = $request->get('obra_institucion'); 
        $movimiento->provincia_id  = $request->get('provincia_id')+1;
        $movimiento->doc_asignacion  = $request->get('doc_asignacion'); 
        $movimiento->turno  = $request->get('turno'); 
 
        $movimiento->save(); 
        return redirect()->route('maquinarias.show' , $idMaquinaria); 
    } 
 
    /** 
     * Remove the specified resource from storage. 
     * 
     * @param  \App\Movimiento  $movimiento 
     * @return \Illuminate\Http\Response 
     */ 
    public function destroy($idMaquinaria,$id ) 
    { 
        //$maquinaria = Maquinaria::findOrFail($idMaquinaria); 
        $movimiento = Movimiento::where('maquinaria_id',$idMaquinaria)->where('movimiento_id',$id)->first(); 
 
        $movimiento->delete(); 
        return redirect()->route('maquinarias.show' , $idMaquinaria); 
 
 
    } 
} 