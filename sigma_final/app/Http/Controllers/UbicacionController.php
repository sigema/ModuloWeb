<?php

namespace App\Http\Controllers;

use App\Provincia;
use App\Maquinaria;
use Illuminate\Http\Request;
use Response;

class UbicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('ubicacion.index' , ['provincias' => Provincia::all()]);
    }

    public function maquinarias($id) {
      $maquinarias = Provincia::maquinarias($id);
      if ($id == 1) {
        $maquinarias_sin_movimientos = Provincia::maquinarias_sin_movimientos();
        $maquinarias_despues_salida = Provincia::maquinarias_despues_salida($id);
        $maquinarias = array_merge(array_merge($maquinarias, $maquinarias_sin_movimientos), $maquinarias_despues_salida);
      }

      $maquinarias_agrupadas = Provincia::maquinarias_agrupadas($id);
      if ($id == 1) {
        $maquinarias_agrupadas_sin_movimientos = Provincia::maquinarias_agrupadas_sin_movimientos();
        $maquinarias_agrupadas_despues_salida = Provincia::maquinarias_agrupadas_despues_salida($id);
        $maquinarias_agrupadas = array_merge(
          array_merge($maquinarias_agrupadas, $maquinarias_agrupadas_sin_movimientos),
          $maquinarias_agrupadas_despues_salida
        );
      }
      $maquinarias_agrupadas = array_reduce($maquinarias_agrupadas, function ($a, $b) {
        $a = (array)$a; $b = (array)$b;
        isset($a[$b['tipo']]) ? $a[$b['tipo']]['count(*)'] += $b['count(*)'] : $a[$b['tipo']] = $b;
        return $a;
      });

      return Response::json(['data' => array(
        'maquinarias' => $maquinarias,
        'grupos_maquinarias' => $maquinarias_agrupadas
      )]);
    }

    public function buscarMaquinaria($q) {
      $maquinaria = Maquinaria::where('maquinaria_id' , $q)
        ->first();

      return ['data' => [
        'provincia' => Maquinaria::ubicacion($q),
        'maquinaria' => $maquinaria
      ]];
    }

    public function search($term)
    {
      $maquinarias = Maquinaria::where('marca', 'like', '%'.$term.'%')
        ->orWhere('modelo', 'like', '%'.$term.'%')
        ->orWhere('tipo', 'like', '%'.$term.'%')
        ->orWhere('nro_registro', 'like', '%'.$term.'%')
        ->orWhere('serie', 'like', '%'.$term.'%')
        ->orWhere('horometro', 'like', '%'.$term.'%')
        ->orWhere('placa', 'like', '%'.$term.'%')
        ->orWhere('modelo', 'like', '%'.$term.'%')
        ->orWhere('estado', 'like', '%'.$term.'%')
        ->orWhere('anio', 'like', '%'.$term.'%')
        ->get();
      return Response::json(['items' => $maquinarias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('ubicacion.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
