<?php

namespace App\Http\Controllers;

use App\OrdenCompra;
use App\ValeConsumo;
use App\DetalleValeConsumo;
use App\Obra;
use App\Comprobante;
use App\DetalleOrdenCompra;
use App\StockAlmacen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class ValeConsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vcs = DB::table('obras')
                ->join('vale_consumo as vc','vc.obra_id','=','obras.obra_id')
                ->get();
//                ->paginate(10);
        return view('valeConsumo.index', ['items' => $vcs]);
    }

    public function search($term)
    {
      $orden_compra = DB::table('orden_compras')
                    ->join('obras as ob','ob.obra_id','=','orden_compras.obra_id')
                    ->select('*')
                    ->where('codigo', 'like', '%'.$term.'%')
                    ->orWhere('ob.obra_id', 'like', '%'.$term.'%')
                    ->get();

      return Response::json(['items' => $orden_compra]);
    }

    public function search_detalle_oc($term){
      $detalle_oc = DB::table('detalle_oc')
                    ->join('consumibles_almacen as con_almac','con_almac.consumible_almac_id','=','detalle_oc.consumible_almac_id')
                    ->select('*')
                    ->where('detalle_oc.oc_id', '=', $term)
                    ->get();

      return Response::json(['items' => $detalle_oc]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('valeConsumo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valeConsumo = new ValeConsumo();
        $valeConsumo->firma = $request->get('firma');
        $valeConsumo->fecha_salida = $request->get('fecha');
        $valeConsumo->obra_id = $request->get('obra');
        $valeConsumo->save();

        $vc_id = $valeConsumo->vale_consumo_id;

        $detalles = $request->get('detalles');
        foreach ($detalles as $value) {
            $detalle = new DetalleValeConsumo();
            $detalle->consumible_almac_id = $value['consumible_almac_id'];
            $detalle->cantidad = $value['cantidad'];
            $detalle->vale_consumo_id = $vc_id;
            $detalle->save();

            $stock = StockAlmacen::where('consumible_almac_id', '=', $value['consumible_almac_id'])->first();
            if (!empty($stock)) {
                $stock->cantidad = $stock->cantidad - $value['cantidad'];
                $stock->save();
            } else {
                $stock = new StockAlmacen();
                $stock->consumible_almac_id = $value['consumible_almac_id'];
                $stock->cantidad = '0';
                $stock->save();
            }

        }
        return Response::json(['data' => 'saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vc = ValeConsumo::findOrFail($id);
        $obra = Obra::where('obra_id',$vc->obra_id)->first();
        $detalle_vc =DB::table('consumibles_almacen as ca')
                        ->join('detalle_vc as d_vc','d_vc.consumible_almac_id','=','ca.consumible_almac_id')
                        ->where('d_vc.vale_consumo_id',$vc->vale_consumo_id)
                        ->get();
        // echo json_encode($detalle_oc);
        return view('valeConsumo.show' , ['valeConsumo' => $vc, 'obra'=> $obra, 'd_vc'=>$detalle_vc]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vc = ValeConsumo::findOrFail($id);
        $obra = Obra::where('obra_id',$vc->obra_id)->first();
        $detalle_vc =DB::table('consumibles_almacen as ca')
                        ->join('detalle_vc as d_vc','d_vc.consumible_almac_id','=','ca.consumible_almac_id')
                        ->where('d_vc.vale_consumo_id',$vc->vale_consumo_id)
                        ->get();
        // echo json_encode($detalle_oc);
        return view('valeConsumo.edit' , ['valeConsumo' => $vc, 'obra'=> $obra, 'd_vc'=>$detalle_vc]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $valeConsumo = ValeConsumo::findOrFail($id);
        $valeConsumo->firma = $request->get('firma');
        $valeConsumo->fecha_salida = $request->get('fecha');
        $valeConsumo->obra_id = $request->get('obra');
        $valeConsumo->save();

        $vc_id = $valeConsumo->vale_consumo_id;

        $old_detalles = DB::table('detalle_vc')->where('vale_consumo_id', '=', $id)->get();

        if (!empty($old_detalles)) {
          foreach ($old_detalles as $detalle) {

            $stock = StockAlmacen::where('consumible_almac_id', '=', $detalle->consumible_almac_id)->first();
            if (!empty($stock)) {
                $stock->cantidad = $stock->cantidad + $detalle->cantidad;
                $stock->save();
            } else {
                $stock = new StockAlmacen();
                $stock->consumible_almac_id = $detalle->consumible_almac_id;
                $stock->cantidad = 0;
                $stock->save();
            }

          }
        }

        DB::table('detalle_vc')->where('vale_consumo_id', '=', $id)->delete();

        $detalles = $request->get('detalles');
        if (!empty($detalles))
            foreach ($detalles as $value) {
                $detalle = new DetalleValeConsumo();
                $detalle->consumible_almac_id = $value['consumible_almac_id'];
                $detalle->cantidad = $value['cantidad'];
                $detalle->vale_consumo_id = $vc_id;
                $detalle->save();

                $stock = StockAlmacen::where('consumible_almac_id', '=', $value['consumible_almac_id'])->first();
                if (!empty($stock)) {
                  $stock->cantidad = $stock->cantidad - $value['cantidad'];
                  $stock->save();
                } else {
                  $stock = new StockAlmacen();
                  $stock->consumible_almac_id = $value['consumible_almac_id'];
                  $stock->cantidad = -$value['cantidad'];
                  $stock->save();
                }

            }
        return Response::json(['data' => 'saved']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
