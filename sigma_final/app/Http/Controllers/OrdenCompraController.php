<?php

namespace App\Http\Controllers;

use App\OrdenCompra;
use App\Obra;
use App\Comprobante;
use App\DetalleOrdenCompra;
use App\StockAlmacen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class OrdenCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ocs = DB::table('obras')
                ->join('orden_compras as oc','oc.obra_id','=','obras.obra_id')
                //->get()
                ->paginate(10);
        return view('ordenCompra.index', ['ordenCompras' => $ocs]);
    }

    public function search($term)
    {
      $orden_compra = DB::table('orden_compras')
                    ->join('obras as ob','ob.obra_id','=','orden_compras.obra_id')
                    ->select('*')
                    ->where('codigo', 'like', '%'.$term.'%')
                    ->orWhere('ob.obra_id', 'like', '%'.$term.'%')
                    ->get();

      return Response::json(['items' => $orden_compra]);
    }

    public function search_detalle_oc($term){
      $detalle_oc = DB::table('detalle_oc')
                    ->join('consumibles_almacen as con_almac','con_almac.consumible_almac_id','=','detalle_oc.consumible_almac_id')
                    ->select('*')
                    ->where('detalle_oc.oc_id', '=', $term)
                    ->get();

      return Response::json(['items' => $detalle_oc]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ordenCompra.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ordenCompra = new OrdenCompra();
        $ordenCompra->codigo = $request->get('codigo');
        $ordenCompra->codigo_solicitud = $request->get('codigo_solicitud');
        $ordenCompra->fecha_ingreso = $request->get('fecha');
        $ordenCompra->total = $request->get('total');
        $ordenCompra->obra_id = $request->get('obra');
        $ordenCompra->save();

        $oc_id = $ordenCompra->oc_id;

        $comprobantes = $request->get('comprobantes');
        foreach ($comprobantes as $value) {
            $comprobante = new Comprobante();
            $comprobante->num_comprobante = $value['numero'];
            $comprobante->tipo = $value['tipo'];
            $comprobante->proveedor_id = $value['proveedorid'];
            $comprobante->oc_id = $oc_id;
            $comprobante->save();
        }

        $detalles = $request->get('detalles');
        foreach ($detalles as $value) {
            $detalle = new DetalleOrdenCompra();
            $detalle->consumible_almac_id = $value['consumible_almac_id'];
            $detalle->cantidad = $value['cantidad'];
            $detalle->precio_unitario = $value['precio_unitario'];
            $detalle->importe = $value['importe'];
            $detalle->oc_id = $oc_id;
            $detalle->save();

            $stock = StockAlmacen::where('consumible_almac_id', '=', $value['consumible_almac_id'])->first();
            if (!empty($stock)) {
                $stock->cantidad = $stock->cantidad + $value['cantidad'];
                $stock->save();
            } else {
                $stock = new StockAlmacen();
                $stock->consumible_almac_id = $value['consumible_almac_id'];
                $stock->cantidad = $value['cantidad'];
                $stock->save();
            }

        }
        return Response::json(['data' => 'saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oc = OrdenCompra::findOrFail($id);
        $obra = Obra::where('obra_id',$oc->obra_id)->first();
        $comprobantes =DB::table('proveedores as p')
                        ->join('comprobantes as comp','comp.proveedor_id','=','p.proveedor_id')
                        ->where('comp.oc_id',$oc->oc_id)
                        ->get();
        $detalle_oc =DB::table('consumibles_almacen as ca')
                        ->join('detalle_oc as d_oc','d_oc.consumible_almac_id','=','ca.consumible_almac_id')
                        ->where('d_oc.oc_id',$oc->oc_id)
                        ->get();
        // echo json_encode($detalle_oc);
        return view('ordenCompra.show' , ['ordenCompra' => $oc, 'obra'=> $obra,'comprobantes'=>$comprobantes, 'd_oc'=>$detalle_oc]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oc = OrdenCompra::findOrFail($id);
        $obra = Obra::where('obra_id',$oc->obra_id)->first();
        $comprobantes =DB::table('proveedores as p')
                        ->join('comprobantes as comp','comp.proveedor_id','=','p.proveedor_id')
                        ->where('comp.oc_id',$oc->oc_id)
                        ->get();
        $detalle_oc =DB::table('consumibles_almacen as ca')
                        ->join('detalle_oc as d_oc','d_oc.consumible_almac_id','=','ca.consumible_almac_id')
                        ->where('d_oc.oc_id',$oc->oc_id)
                        ->get();
        // echo json_encode($detalle_oc);
        return view('ordenCompra.edit' , ['ordenCompra' => $oc, 'obra'=> $obra,'comprobantes'=>$comprobantes, 'd_oc'=>$detalle_oc]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $ordenCompra = OrdenCompra::findOrFail($id);
        $ordenCompra->codigo = $request->get('codigo');
        $ordenCompra->codigo_solicitud = $request->get('codigo_solicitud');
        $ordenCompra->fecha_ingreso = $request->get('fecha');
        $ordenCompra->total = $request->get('total');
        $ordenCompra->obra_id = $request->get('obra');
        $ordenCompra->save();

        $oc_id = $ordenCompra->oc_id;

        DB::table('comprobantes')->where('oc_id', '=', $id)->delete();

        $comprobantes = $request->get('comprobantes');
        if (!empty($comprobantes))
            foreach ($comprobantes as $value) {
                $comprobante = new Comprobante();
                $comprobante->num_comprobante = $value['numero'];
                $comprobante->tipo = $value['tipo'];
                $comprobante->proveedor_id = $value['proveedorid'];
                $comprobante->oc_id = $oc_id;
                $comprobante->save();
            }

        $old_detalles = DB::table('detalle_oc')->where('oc_id', '=', $id)->get();

        if (!empty($old_detalles)) {
          foreach ($old_detalles as $detalle) {

            $stock = StockAlmacen::where('consumible_almac_id', '=', $detalle->consumible_almac_id)->first();
            if (!empty($stock)) {
                $stock->cantidad = $stock->cantidad - $detalle->cantidad;
                $stock->save();
            } else {
                $stock = new StockAlmacen();
                $stock->consumible_almac_id = $detalle->consumible_almac_id;
                $stock->cantidad = 0;
                $stock->save();
            }

          }
        }

        DB::table('detalle_oc')->where('oc_id', '=', $id)->delete();

        $detalles = $request->get('detalles');
        if (!empty($detalles))
            foreach ($detalles as $value) {
              $detalle = new DetalleOrdenCompra();
              $detalle->consumible_almac_id = $value['consumible_almac_id'];
              $detalle->cantidad = $value['cantidad'];
              $detalle->precio_unitario = $value['precio_unitario'];
              $detalle->importe = $value['importe'];
              $detalle->oc_id = $oc_id;
              $detalle->save();

              $stock = StockAlmacen::where('consumible_almac_id', '=', $value['consumible_almac_id'])->first();
              if (!empty($stock)) {
                $stock->cantidad = $stock->cantidad + $value['cantidad'];
                $stock->save();
              } else {
                $stock = new StockAlmacen();
                $stock->consumible_almac_id = $value['consumible_almac_id'];
                $stock->cantidad = $value['cantidad'];
                $stock->save();
              }
            }

        return Response::json(['data' => 'saved']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
