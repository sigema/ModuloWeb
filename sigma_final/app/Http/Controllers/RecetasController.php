<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maquinaria;
use App\Receta;
//use App\Hora;
use App\DetalleReceta;
use App\ConsumibleAlmacen;

class RecetasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idMaquinaria)
    {
        $receta = Receta::where('maquinaria_id' , $idMaquinaria )->first();
        $maquinaria = $receta->maquinaria;
        $det = $receta->detalleRecetas;
        //$horas = Hora::all();
        $detalleGeneral = DetalleReceta::where('receta_id' , $receta->receta_id)->groupBy('consumible_id')->get();
        $detalleL = $detalleGeneral->where('consumible.tipo' , 2);
        $detalleE = $detalleGeneral->where('consumible.tipo' , 3);
        $detalleF = $detalleGeneral->where('consumible.tipo' , 1);
        
        //return $det->where('consumible.tipo' , 'lubricante');
        //return $receta->detalleRecetas()->where('consumible_id' , 1)->get();
        return view('receta.index' , [ 
            'receta' => $receta  , 
            'maquinaria' => $maquinaria , 
            //'horas' => $horas , 
            'detalleL' => $detalleL,
            'detalleE' => $detalleE,
            'detalleF' => $detalleF]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idMaquinaria)
    {
        $maquinaria = Maquinaria::findOrFail($idMaquinaria);
        return view('receta.create' , ['maquinaria' => $maquinaria]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $idMaquinaria)
    {
        $this->validate($request ,[
            'periodo' => 'required',
            'horas_trabajo' => 'required',
            ]);
        $receta = new Receta();
        $receta->periodo = $request->get("periodo");
        $receta->horas_trabajo = $request->get("horas_trabajo");
        $receta->maquinaria_id = $idMaquinaria;
        $receta->save();

        return redirect()->route('maquinarias.receta.index' , [ 'idMaquinaria' => $idMaquinaria ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idMaquinaria)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idMaquinaria , $id )
    {
        $receta = Receta::findOrFail($id);
        //$horas = Hora::all();
        $maquinaria = $receta->maquinaria;
        $detalleGeneral = DetalleReceta::where('receta_id' , $receta->receta_id)->groupBy('consumible_id')->get();
        $detalleL = $detalleGeneral->where('consumible.tipo' , 2);
        $detalleE = $detalleGeneral->where('consumible.tipo' , 3);
        $detalleF = $detalleGeneral->where('consumible.tipo' , 1);
        
        //return $det->where('consumible.tipo' , 'lubricante');
        //return $receta->detalleRecetas()->where('consumible_id' , 1)->get();
        return view('receta.edit' , [ 
            'receta' => $receta  , 
            'maquinaria' => $maquinaria , 
            'detalleL' => $detalleL,
            'detalleE' => $detalleE,
            'detalleF' => $detalleF]); 
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMaquinaria,  $id)
    {
        $this->validate($request ,[
          'periodo' => 'required',
          'horas_trabajo' => 'required',
        ]);
        $receta = Receta::findOrFail($id);
        $receta->periodo = $request->get("periodo");
        $receta->horas_trabajo = $request->get("horas_trabajo");
        $receta->maquinaria_id = $idMaquinaria;
        $receta->save();

        return redirect()->route('maquinarias.receta.index' , [ 'idMaquinaria' => $idMaquinaria ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
