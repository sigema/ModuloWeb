<?php

namespace App\Http\Controllers;

use App\ConsumibleAlmacen;
use Illuminate\Http\Request;
use Response;

class ConsumiblesAlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('consumiblesAlmacen.index', ['consumiblesAlmacen'=>ConsumibleAlmacen::all()]);
    }

    public function search($term)
    {
      $consumible_almacen = ConsumibleAlmacen::where('codigo', 'like', '%'.$term.'%')
        ->orWhere('marca', 'like', '%'.$term.'%')
        ->orWhere('descripcion', 'like', '%'.$term.'%')
        ->get();
      return Response::json(['items' => $consumible_almacen]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('consumiblesAlmacen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $consumibleAlmac = new ConsumibleAlmacen();
        $consumibleAlmac->codigo = $request->get('codigo');
        $consumibleAlmac->descripcion = $request->get('descripcion');
        $consumibleAlmac->unidad = $request->get('unidad');
        $consumibleAlmac->marca = $request->get('marca');
        $consumibleAlmac->precio_ref = $request->get('precio_ref');
        $consumibleAlmac->save();
        return redirect()->route('consumibles.index');
    }

    public function save(Request $request)
    {
        $consumibleAlmac = new ConsumibleAlmacen();
        $consumibleAlmac->codigo = $request->get('codigo');
        $consumibleAlmac->descripcion = $request->get('descripcion');
        $consumibleAlmac->unidad = $request->get('unidad');
        $consumibleAlmac->marca = $request->get('marca');
        $consumibleAlmac->precio_ref = $request->get('precio_ref');
        $consumibleAlmac->tipo = $request->get('tipo');
        $consumibleAlmac->save();

        return Response::json(['result'=>$consumibleAlmac], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consumibleAlmac = ConsumibleAlmacen::where('consumible_almac_id',$id)->first();
        return view('consumiblesAlmacen.edit', ['consumiblesAlmacen'=> $consumibleAlmac]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $consumibleAlmac = ConsumibleAlmacen::findOrFail($id);
        $consumibleAlmac->codigo = $request->get('codigo');
        $consumibleAlmac->descripcion = $request->get('descripcion');
        $consumibleAlmac->unidad = $request->get('unidad');
        $consumibleAlmac->marca = $request->get('marca');
        $consumibleAlmac->precio_ref = $request->get('precio_ref');
        $consumibleAlmac->tipo = $request->get('tipo');
        $consumibleAlmac->save();
        return redirect()->route('consumibles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
