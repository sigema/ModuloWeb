<?php

namespace App\Http\Controllers;

use App\Maquinaria;
use Illuminate\Http\Request;
use App\Mcorrectivo;
use Illuminate\Support\Facades\Auth;

class McorrectivosController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idMaquinaria)
    {
        return view('mcorrectivos.create' , [ 'maquinaria' => Maquinaria::findOrFail($idMaquinaria) ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $idMaquinaria)
    {
        $this->validate($request , [
            'obra_institucion' => 'required',
            'mecanico' => 'required',
            'fecha' => 'required|date',    
        ]);

        $mcorrectivo = new Mcorrectivo();
        $mcorrectivo->obra_institucion = $request->get('obra_institucion');
        $mcorrectivo->fecha =$request->get('fecha');
        $mcorrectivo->mecanico = $request->get('mecanico');
        $mcorrectivo->reporte_falla = $request->get('reporte_falla');
        $mcorrectivo->nro_documento = $request->get('nro_documento');
        $mcorrectivo->maquinaria_id = $idMaquinaria;
        $mcorrectivo->usuario_id = Auth::user()->id;
        $mcorrectivo->save();
        return redirect()->route('maquinarias.show' , $idMaquinaria);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idMaquinaria , $id)
    {
        $mcorrectivo = Mcorrectivo::where('maquinaria_id' , $idMaquinaria)->where('mcorrectivo_id' , $id)->first();
        $maquinaria = $mcorrectivo->maquinaria;
        return view('mcorrectivos.edit' , ['mc' => $mcorrectivo , 'maquinaria' => $maquinaria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMaquinaria , $id)
    {
        $this->validate($request , [
            'obra_institucion' => 'required',
            'mecanico' => 'required',
            'fecha' => 'required|date',    
        ]);

        $mcorrectivo = Mcorrectivo::where('maquinaria_id' , $idMaquinaria)->where('mcorrectivo_id' , $id)->first();
        $mcorrectivo->obra_institucion = $request->get('obra_institucion');
        $mcorrectivo->fecha =$request->get('fecha');
        $mcorrectivo->mecanico = $request->get('mecanico');
        $mcorrectivo->reporte_falla = $request->get('reporte_falla');
        $mcorrectivo->nro_documento = $request->get('nro_documento');
        $mcorrectivo->maquinaria_id = $idMaquinaria;
        $mcorrectivo->usuario_id = Auth::user()->id;
        $mcorrectivo->save();
        return redirect()->route('maquinarias.show' , $idMaquinaria);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idMaquinaria , $id)
    {
        $mcorrectivo = Mcorrectivo::where('maquinaria_id' , $idMaquinaria)->where('mcorrectivo_id' , $id)->first();
        $mcorrectivo->delete();
        return redirect()->route('maquinarias.show' , $idMaquinaria);
    }
}
