<?php

namespace App\Http\Middleware;

use Closure;
use App\Mpreventivo;

class checkStatusMPreventivo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $mp = Mpreventivo::where('mpreventivo_id' , $request->mpreventivo)->where('maquinaria_id' , $request->maquinaria)->first();
        if($mp->status){
            return redirect()->back();
        }
        return $next($request);
    }
}
