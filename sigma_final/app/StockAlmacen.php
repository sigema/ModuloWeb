<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockAlmacen extends Model
{
    protected $table = 'stock_almacen';
    protected $primaryKey = 'stock_almac_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stock_almac_id',
        'consumible_almac_id',
        'cantidad'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function findOrCreate($id)
    {
        $obj = static::find($id);
        return $obj ?: new static;
    }

}
