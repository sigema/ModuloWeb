var glb_detalles_oc = [];
var glb_oc;


function formatResult (item) {
  if (item.loading) return item.codigo;
  return item.codigo + "--" + item.obra_id;
}

function formatSelection (item) {
  glb_oc = item;
  console.log('se selecciona',item);  
  $('.js-obra').val(glb_oc.descripcion);
  return item.codigo;
}

$(document).ready(function() {
  $(".js-select2-remote").select2({
    ajax: {
      url: function(params) {
        console.log(params);
        return '/ordencompra/search/'+params.term;
      },
      dataType: 'json',
      delay: 400,
      cache: true,
      data: function (params) {
        console.log(params);
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        console.log(data,params);
        return {
          results: $.map(data.items, function(item){
            item.id = item.oc_id;
            return item;
          }),
          pagination: {
          //more: (params.page*2) < data.total_count
            more: 0
          }
        };
      },
    },
    escapeMarckup: function(markup){return markup;},
    minimumInputLength: 2,
    templateResult: formatResult,
    templateSelection: formatSelection
  }).on('select2:close', function(){
    ///llenar la table
    $.ajax({
      url: '/detalles_oc/search/'+glb_oc.id,
      type: 'GET',
      dataType: 'json'
    })
    .done(function(res) {
      console.log("success",res);
      var total = 0;
      glb_detalles_oc = res.items;
      for (var i = 0; i < res.items.length; i++) {
        var html = '<tr>'+
                   '<td>' + res.items[i].codigo + '</td>' +
                   '<td>' + res.items[i].descripcion + '</td>' +
                   '<td>' + res.items[i].unidad + '</td>' +
                   '<td>' + res.items[i].cantidad + '</td>' +
                   '<td>' + res.items[i].precio_unitario + '</td>' +
                   '<td>' + res.items[i].importe; + '</td>' +
                   '</tr>';
                   console.log(html);
        $('#js-detalles_oc >tbody:last-child').append(html);
        total += parseFloat(res.items[i].importe);
      } 
      var total_html = total.toFixed(2).toLocaleString();
      $('.js-total').val(total_html);               
      return false;
    })
    .fail(function(fail) {
      console.log("error",fail);
    });
  });

  ///GUARDANDO EN BD
  $('.js-btn-guardar-pecosa').click(function(){
    var codigo=$("#pecosaForm [name='codigo']").val();
    var fecha=$("#pecosaForm [name='fecha']").val();
    var total=$("#pecosaForm [name='total']").val();

    var pecosa = {};
    pecosa.codigo = codigo;
    pecosa.fecha = fecha;
    pecosa.total = total;
    pecosa.obra_id = glb_oc.obra_id;
    pecosa.detalle_oc = glb_detalles_oc;
    pecosa._token = CSRF_TOKEN;

    console.log(pecosa);

    $.ajax({
      url: '/pecosas',
      type: 'POST',
      dataType: 'json',
      data: pecosa
    })
    .done(function(res) {
      console.log(res,"success");
      window.location.replace("/pecosas");
    })
    .fail(function(fail) {
      console.log(fail,"error");
    });

    return false;

  });

});
