var glb_detalles_vc = [];
var glb_obra = null;
var glb_consumible_almacen = null;
var glb_vc_id = null;

function set_vc_id(id){
  glb_vc_id = id;
}

function add_detalle_vc(consumible_almac_id,cantidad){
  var detalle_vc = {
    consumible_almac_id: consumible_almac_id,
    cantidad: cantidad,
  };
  glb_detalles_vc.push(detalle_vc);
}

function set_obra(id,num_meta,descripcion){
  var obra = {
    id: id,
    num_meta: num_meta,
    descripcion: descripcion
  };
  glb_obra = obra;
}

function formatResultObra (item) {
  if (item.loading) return item.text;
  if (item.id == '-1') return item.text;
  return item.num_meta + "--" + item.descripcion;
}

function formatSelectionObra (item) {
  if (item.descripcion) glb_obra = item;
  if (item.id == '-1') glb_obra = item;
  return item.descripcion || item.text;
}

function formatResultConsumibleAlmacen (item) {
  if (item.loading) return item.text;
  if (item.id == '-1') return item.text;
  return item.codigo + "--" + item.descripcion;
}

function formatSelectionConsumibleAlmacen (item) {
  if (item.descripcion) glb_consumible_almacen = item;
  if (item.id == '-1') glb_consumible_almacen = item;
  return item.descripcion || item.text;
}

//// UPDATE COMBO
function update_select(selector, id, texto){
  selector.append($('<option></option>').val(id).html(texto));
  selector.select2("trigger", "select", {
    data: { id: id }
  });
}

//// UPDATE DATA TABLE
function update_table_numeration(selector) {
  var count = 1;
  selector.find("[scope='row']").each(function(){
    $(this).text(count);
    count += 1;
  });

  count = 1;
  selector.find(".js-delete").each(function(){
    $(this).attr('js-id',count);
    count += 1;
  });
}

$(document).ready(function() {

  /// SELECT OBRA
  $(".js-select2-remote-obra").select2({
    ajax: {
      url: function(params) {
        return '/obras/search/'+params.term;
      },
      dataType: 'json',
      delay: 400,
      cache: true,
      data: function (params) {
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        data.items = $.map(data.items, function(item){
          item.id = item.obra_id;
          return item;
        });
        var add_opt = {
          id: '-1',
          text: '---NUEVA OBRA---'
        };
        data.items.unshift(add_opt);
        return {
          results: data.items,
          pagination: {
          //more: (params.page*2) < data.total_count
            more: 0
          }
        };
      },
    },
    escapeMarckup: function(markup){return markup;},
    minimumInputLength: 1,
    templateResult: formatResultObra,
    templateSelection: formatSelectionObra
  }).on('select2:close', function(){
    if(glb_obra == null) return;
    if(glb_obra.id == '-1') $('#nuevaObraModal').modal('show');
  });

  /// SELECT CONSUMIBLE
  $(".js-select2-consumibleAlmacen").select2({
    ajax: {
      url: function(params) {
        return '/consumiblesAlmacen/search/'+params.term;
      },
      dataType: 'json',
      delay: 400,
      cache: true,
      data: function (params) {
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        data.items = $.map(data.items, function(item){
          item.id = item.consumible_almac_id;
          return item;
        });
        var add_opt = {
          id: '-1',
          text: '---NUEVO CONSUMIBLE DE ALMACEN---'
        };
//        data.items.unshift(add_opt);
        return {
          results: data.items,
          pagination: {
          //more: (params.page*2) < data.total_count
            more: 0
          }
        };
      },
    },
    escapeMarckup: function(markup){return markup;},
    minimumInputLength: 1,
    templateResult: formatResultConsumibleAlmacen,
    templateSelection: formatSelectionConsumibleAlmacen
  }).on('select2:close', function(){
    if(glb_consumible_almacen == null) return;
    if(glb_consumible_almacen.id == '-1') $('#nuevoConsumibleModal').modal('show');
  });

  /// UPDATE DATA
  if (glb_obra!=null)
    update_select($("#valeConsumoForm [name='obra']"), glb_obra.id , glb_obra.descripcion);

  /// DETALLES VC TABLE
  $("#js-btn-detalle_vc").click(function(){
    var consumible_almac_id = $(".js-detalle_vc [name='consumibleAlmacen']").val();
    var cantidad = $(".js-detalle_vc [name='cantidad']").val();

    var detalle_vc = {};
    detalle_vc.consumible_almac_id = consumible_almac_id;
    detalle_vc.cantidad = cantidad;
    glb_detalles_vc.push(detalle_vc);

    var html = '<tr>'+
                '<td scope="row">'+glb_detalles_vc.length+'</td>'+
                '<td>'+glb_consumible_almacen.codigo+'</td>'+
                '<td>'+glb_consumible_almacen.descripcion+'</td>'+
                '<td>'+glb_consumible_almacen.marca+'</td>'+
                '<td>'+glb_consumible_almacen.unidad+'</td>'+
                '<td>'+cantidad+'</td>'+
                '<td>'+ '<button type="button" class="js-delete btn btn-outline-danger" js-id="'+glb_detalles_vc.length+'">x</button>' +'</td>'
                '</tr>';
    $('#js-detalles_vc >tbody:last-child').append(html);
    return false;
  });

  $("#js-detalles_vc").on('click', '.js-delete', function(){
    var id = $(this).attr('js-id');
    glb_detalles_vc.splice(id-1,1);
    $(this).parent().parent().remove();
    update_table_numeration($('#js-detalles_vc'));
    return false;
  });

  ///GUARDANDO EN BD
  $('.js-btn-guardar-vc').click(function(){
    var fecha=$("#valeConsumoForm [name='fecha']").val();
    var obra=$("#valeConsumoForm [name='obra']").val();
    var firma=$("#valeConsumoForm [name='firma']").val();

    var vc = {};
    vc.fecha = fecha;
    vc.obra = obra;
    vc.firma = firma;
    vc.detalles = glb_detalles_vc;
    vc._token = CSRF_TOKEN;

    var url = '/valeconsumo';
    if (glb_vc_id!=null) {
      vc.vc_id = glb_vc_id;
      url = '/valeconsumo/update/' + vc.vc_id;
    }

    console.log(vc);

    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: vc
    })
    .done(function(res) {
      console.log(res,"success");
      // redirecciono
      window.location.replace("/valeconsumo");
    })
    .fail(function(fail) {
      console.log(fail,"error");
    });

    return false;

  });



  ////////// MODALS /////////
  /// NUEVA OBRA
  $('#nuevaObraModal .js-btn-save').click(function() {
    var num_meta = $("#nuevaObraModal [name='num_meta']").val();
    var descripcion = $("#nuevaObraModal [name='descripcion']").val();
    var obra = {
      num_meta: num_meta,
      descripcion: descripcion
    };
    obra._token = CSRF_TOKEN;

    $.ajax({
      url: '/obras/save',
      type: 'POST',
      dataType: 'json',
      data: obra
    })
    .done(function(res) {
      glb_obra = res.result;
      var id = res.result.obra_id;
      var text = res.result.descripcion;
      $('#nuevaObraModal').modal('hide');

      update_select($("#valeConsumoForm [name='obra']"), id, text);
    })
    .fail(function(fail) {
      console.log(fail,'error');
    });
    return false;
  });

  /// NUEVO CONSUMIBLE ALMACEN
  $('#nuevoConsumibleModal .js-btn-save').click(function() {
    var codigo      = $("#nuevoConsumibleModal [name='codigo']").val();
    var descripcion = $("#nuevoConsumibleModal [name='descripcion']").val();
    var unidad      = $("#nuevoConsumibleModal [name='unidad']").val();
    var marca       = $("#nuevoConsumibleModal [name='marca']").val();
    var precio_ref  = $("#nuevoConsumibleModal [name='precio_ref']").val();
    var tipo        = $("#nuevoConsumibleModal [name='tipo']").val();
    var consumible = {
      codigo: codigo,
      descripcion: descripcion,
      unidad: unidad,
      marca: marca,
      precio_ref: precio_ref,
      tipo: tipo
    };
    consumible._token = CSRF_TOKEN;

    $.ajax({
      url: '/consumiblesAlmacen/save',
      type: 'POST',
      dataType: 'json',
      data: consumible
    })
    .done(function(res) {
      glb_consumible_almacen = res.result;
      var id = res.result.consumible_almac_id;
      var text = res.result.descripcion;
      $('#nuevoConsumibleModal').modal('hide');

      update_select($("#valeConsumoForm [name='consumibleAlmacen']"), id, text);
    })
    .fail(function(fail) {
      console.log(fail,'error');
    });
    return false;
  });
});
