var map;
var infowindow = null;
var markers = [];
var markers_data = [];
var maquinarias = [];
var arequipa = null;

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var CSRF_TOKEN = window.Laravel.csrfToken;

function initMap() {
  var map_dom = document.getElementById('map');
  if(map_dom != null) {
    map = new google.maps.Map(map_dom, {
      center: {lat: -15.8650459, lng: -72.9868288},
      zoom: 7
    });

    parseMarkers();
  }
}

function parseMarkers(){
  markers_data.forEach( function (marker_data) {
    var marker = new google.maps.Marker({
      position: {lat: parseFloat(marker_data.lat), lng: parseFloat(marker_data.lng)},
      map: map,
      title: marker_data.title,
      animation: google.maps.Animation.DROP
    });

    marker.addListener('click', function() {
      if (infowindow)
        infowindow.close();

      infowindow = new google.maps.InfoWindow({
        content: marker_data.title
      });
      infowindow.open(map, marker);

      $.ajax({
         url: '/ubicacion/provincia/'+marker_data.id,
          type: 'GET',
          data: {_token: CSRF_TOKEN },
          dataType: 'JSON',
          success: function (data) {
            console.log(data);
            updateListMaquinarias(marker_data.title,
              data['data']['grupos_maquinarias']);
            updateModalMaquinarias(marker_data.title,
              maquinarias = data['data']['maquinarias']);
          }
      });

    });

    markers.push(marker);
  });
}

function addMarker (id, title, lat, lng) {
  var marker = {
    id: id,
    title: title,
    lat: lat,
    lng: lng
  };
  if(id==1)
    arequipa = marker;
  markers_data.push(marker);
}

function updateListMaquinarias(title, maquinarias) {
  console.log(maquinarias);
  if (maquinarias==null || maquinarias.length<1) $('#detallesProvincia').hide();
  else $('#detallesProvincia').show();
  $("#js-provincia-title").text(title);
  $("#js-provincia-content .js-maquinaria-row").remove();
  $.each(maquinarias, function(key, maquinaria) {
    var html = $("#js-maquinaria-row").clone();
    html.removeAttr('id');
    html.removeClass("hidden");
    html.find("span").text(
      maquinaria["count(*)"] + " " +
      maquinaria["tipo"] +
      ((parseInt(maquinaria["count(*)"])>1)? 's': '')
    );
    $("#js-provincia-content").append(html);
  });
}

function updateModalMaquinarias(title, maquinarias) {
  $("#modalMaquinarias tbody").children().remove();
  $("#modalMaquinarias .modal-title").text(
    "Maquinarias en " +title
  );
  var count = 0;
  maquinarias.forEach(function(maquinaria) {
    var html = '<tr>'+
      '<th scope="row">'+count+'</th>'+
      '<td> <a href="maquinarias/'+maquinaria.maquinaria_id+'">'+maquinaria.serie+'</a></td>'+
      '<td>'+maquinaria.marca+'</td>'+
      '<td>'+maquinaria.modelo+'</td>'+
      '<td>'+maquinaria.tipo+'</td>'+
      '<td>'+maquinaria.nro_registro+'</td>'+
      '</tr>';
    $("#modalMaquinarias tbody").append(html);
    count = count+1;
  });
}

// Funciones de la Busqueda
var glb_maquinaria = null;

function formatResultUbicacion (item) {
  if (item.loading) return item.text;
  return "Registro:"+item.nro_registro + "  Serie:" + item.serie + "  Placa:" + item.placa + "  Marca:" + item.marca;
}

function formatSelectionUbicacion (item) {
  if (item.nro_registro) {
    glb_maquinaria = item;
    return "Registro:"+item.nro_registro + "  Serie:" + item.serie + "  Placa:" + item.placa;
  } 
  else 
    return item.text;
}

$( document ).ready(function() {
  $('#detallesProvincia').hide();

/// SELECT UBICACION
  $(".js-select2-remote-ubicacion").select2({
    ajax: {
      url: function(params) {
        return '/ubicacion/search/'+params.term;
      },
      dataType: 'json',
      delay: 400,
      //cache: true,
      data: function (params) {
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        data.items = $.map(data.items, function(item){
          item.id = item.maquinaria_id;
          return item;
        });
        return {
          results: data.items,
          pagination: {
          //more: (params.page*2) < data.total_count
            more: 0
          }
        };
      },
    },
    escapeMarckup: function(markup){return markup;},
    minimumInputLength: 1,
    templateResult: formatResultUbicacion,
    templateSelection: formatSelectionUbicacion
  }).on('select2:close', function(){
    if(glb_maquinaria == null) return;

    /// on select
    $.ajax({
      url: '/ubicacion/maquinaria/'+glb_maquinaria.id,
      type: 'GET',
      data: {_token: CSRF_TOKEN },
      dataType: 'JSON',
      success: function (data) {
        console.log(data);
        var provincia = null;
        var maquinaria = data['data']['maquinaria'];

        $("#js-provincia-content .js-maquinaria-row").remove();
        for (var i = 0; i < markers.length; i++)
          markers[i].setMap(null);
        $('#detallesProvincia').hide();

        if (maquinaria != null) {
          // muestra detalles de maquinaria
          for (var key in maquinaria) {
            var html = $('#js-maquinaria-data').clone();
            if (key=='anio') {
              key='Año';
            }
            if (key=='estado') {
              if(maquinaria[key]==1)
                maquinaria[key]='Operativo';
              else if(maquinaria[key]==2)
                maquinaria[key]='-------';
              else 
                maquinaria[key]='Inoperativo';
            }
            if (key=='condicion') {
              if(maquinaria[key]==1)
                maquinaria[key]='Activo';
              else 
                maquinaria[key]='Inactivo';
            }     
            if(key=='nro_registro'){
              key='Nro de Registro'
            }       
            if(!(key=='foto') && !(key=='maquinaria_id')){
              html.find('span').first().text(key.capitalize());
              html.find('span').last().text(maquinaria[key]);
              $("#js-provincia-content").append(html);
            }    
            
          }

          //muestra posicion en el mapa
          if (data['data']['provincia'].length>0) {
            provincia = {
              id: data['data']['provincia'][0]['provincia_id'],
              title: data['data']['provincia'][0]['nombre'],
              lat: data['data']['provincia'][0]['latitud'],
              lng: data['data']['provincia'][0]['longitud'],
            };
          } else {
            provincia = arequipa;
          }
          $('#js-provincia-title').text('La maquinaria se encuentra en '+
            provincia.title);

          var marker = new google.maps.Marker({
            position: {lat: parseFloat(provincia.lat), lng: parseFloat(provincia.lng)},
            map: map,
            title: provincia.title,
            animation: google.maps.Animation.DROP
          });
          markers.push(marker);
        } else {
          $('#js-provincia-title').text('No se encontro la maquinaria');
        }

      }
    });
    
  });

/******************************************************************************
 ********************************** BUSQUEDA **********************************
 ******************************************************************************/
  /*$('#form-search-maquinaria').submit(function(){
    var search = $(this).find('input').val();
    if (search.trim() != '') {

      $.ajax({
         url: '/ubicacion/maquinaria/'+search,
          type: 'GET',
          data: {_token: CSRF_TOKEN },
          dataType: 'JSON',
          success: function (data) {
            console.log(data);
            var provincia = null;
            var maquinaria = data['data']['maquinaria'];

            $("#js-provincia-content .js-maquinaria-row").remove();
            for (var i = 0; i < markers.length; i++)
              markers[i].setMap(null);
            $('#detallesProvincia').hide();

            if (maquinaria != null) {
              // muestra detalles de maquinaria
              for (var key in maquinaria) {
                var html = $('#js-maquinaria-data').clone();
                if (key=='anio') {
                  key='Año';
                }
                if (key=='estado') {
                  if(maquinaria[key]==1)
                    maquinaria[key]='Operativo';
                  else if(maquinaria[key]==2)
                    maquinaria[key]='-------';
                  else 
                    maquinaria[key]='Inoperativo';
                }
                if (key=='condicion') {
                  if(maquinaria[key]==1)
                    maquinaria[key]='Activo';
                  else 
                    maquinaria[key]='Inactivo';
                }     
                if(key=='nro_registro'){
                  key='Nro de Registro'
                }       
                if(!(key=='foto') && !(key=='maquinaria_id')){
                  html.find('span').first().text(key.capitalize());
                  html.find('span').last().text(maquinaria[key]);
                  $("#js-provincia-content").append(html);
                }    
                
              }

              //muestra posicion en el mapa
              if (data['data']['provincia'].length>0) {
                provincia = {
                  id: data['data']['provincia'][0]['provincia_id'],
                  title: data['data']['provincia'][0]['nombre'],
                  lat: data['data']['provincia'][0]['latitud'],
                  lng: data['data']['provincia'][0]['longitud'],
                };
              } else {
                provincia = arequipa;
              }
              $('#js-provincia-title').text('La maquinaria se encuentra en '+
                provincia.title);

              var marker = new google.maps.Marker({
                position: {lat: parseFloat(provincia.lat), lng: parseFloat(provincia.lng)},
                map: map,
                title: provincia.title,
                animation: google.maps.Animation.DROP
              });
              markers.push(marker);
            } else {
              $('#js-provincia-title').text('No se encontro la maquinaria');
            }

          }
      });

    }
    return false;
  });*/
/******************************** end BUSQUEDA ********************************/

/******************************************************************************
 ****************************** LIMPIAR BUSQUEDA ******************************
 ******************************************************************************/
  $('#btn-clear').click(function(){
    $('#form-search-maquinaria').val('');
    $('#js-provincia-title').text('Haga click en una provincia');
    glb_maquinaria = null;

    //$('#form-search-maquinaria [name="ubicacion"]').append($('<option></option>').val(id).html(texto));
    $('#form-search-maquinaria [name="ubicacion"]').select2("trigger", "select", {
      data: { id: 0 }
    });

    $("#js-provincia-content .js-maquinaria-row").remove();
    for (var i = 0; i < markers.length; i++)
      markers[i].setMap(null);
    parseMarkers();

    return false;
 });
/**************************** end LIMPIAR BUSQUEDA ****************************/
});


