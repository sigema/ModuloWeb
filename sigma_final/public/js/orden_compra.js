var glb_comprobantes = [];
var glb_detalles_oc = [];
var glb_obra = null;
var glb_proveedor = null;
var glb_consumible_almacen = null;
var glb_oc_id = null;

function set_oc_id(id){
  glb_oc_id = id;
}

function add_comprobante(nombre,tipo,numero,proveedorid){
  var comprobante = {
    nombre: nombre,
    tipo: tipo,
    numero: numero,
    proveedorid: proveedorid
  };
  glb_comprobantes.push(comprobante);
}

function add_detalle_oc(consumible_almac_id,cantidad,precio_unitario,importe){
  var detalle_oc = {
    consumible_almac_id: consumible_almac_id,
    cantidad: cantidad,
    precio_unitario: precio_unitario,
    importe: importe
  };
  glb_detalles_oc.push(detalle_oc);
}

function set_obra(id,num_meta,descripcion){
  var obra = {
    id: id,
    num_meta: num_meta,
    descripcion: descripcion
  };
  glb_obra = obra;
}

function formatResultObra (item) {
  if (item.loading) return item.text;
  if (item.id == '-1') return item.text;
  return item.num_meta + "--" + item.descripcion;
}

function formatSelectionObra (item) {
  if (item.descripcion) glb_obra = item;
  if (item.id == '-1') glb_obra = item;
  return item.descripcion || item.text;
}

function formatResultProveedor (item) {
  if (item.loading) return item.text;
  if (item.id == '-1') return item.text;
  return item.ruc + "--" + item.nombre;
}

function formatSelectionProveedor (item) {
  if (item.nombre) glb_proveedor = item;
  if (item.id == '-1') glb_proveedor = item;
  return item.nombre || item.text;
}

function formatResultConsumibleAlmacen (item) {
  if (item.loading) return item.text;
  if (item.id == '-1') return item.text;
  return item.codigo + "--" + item.descripcion;
}

function formatSelectionConsumibleAlmacen (item) {
  if (item.descripcion) glb_consumible_almacen = item;
  if (item.id == '-1') glb_consumible_almacen = item;
  return item.descripcion || item.text;
}

//// UPDATE COMBO
function update_select(selector, id, texto){
  selector.append($('<option></option>').val(id).html(texto));
  selector.select2("trigger", "select", {
    data: { id: id }
  });
}

//// UPDATE DATA TABLE
function update_table_numeration(selector) {
  var count = 1;
  selector.find("[scope='row']").each(function(){
    $(this).text(count);
    count += 1;
  });

  count = 1;
  selector.find(".js-delete").each(function(){
    $(this).attr('js-id',count);
    count += 1;
  });
}

$(document).ready(function() {

  /// SELECT OBRA
  $(".js-select2-remote-obra").select2({
    ajax: {
      url: function(params) {
        return '/obras/search/'+params.term;
      },
      dataType: 'json',
      delay: 400,
      cache: true,
      data: function (params) {
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        data.items = $.map(data.items, function(item){
          item.id = item.obra_id;
          return item;
        });
        var add_opt = {
          id: '-1',
          text: '---NUEVA OBRA---'
        };
        data.items.unshift(add_opt);
        return {
          results: data.items,
          pagination: {
          //more: (params.page*2) < data.total_count
            more: 0
          }
        };
      },
    },
    escapeMarckup: function(markup){return markup;},
    minimumInputLength: 1,
    templateResult: formatResultObra,
    templateSelection: formatSelectionObra
  }).on('select2:close', function(){
    if(glb_obra == null) return;
    if(glb_obra.id == '-1') $('#nuevaObraModal').modal('show');
  });

  /// SELECT PROVEEDOR
  $(".js-select2-proveedor").select2({
    ajax: {
      url: function(params) {
        return '/proveedores/search/'+params.term;
      },
      dataType: 'json',
      delay: 400,
      cache: true,
      data: function (params) {
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        data.items = $.map(data.items, function(item){
          item.id = item.proveedor_id;
          return item;
        });
        var add_opt = {
          id: '-1',
          text: '---NUEVO PROVEEDOR---'
        };
        data.items.unshift(add_opt);
        return {
          results: data.items,
          pagination: {
          //more: (params.page*2) < data.total_count
            more: 0
          }
        };
      },
    },
    escapeMarckup: function(markup){return markup;},
    minimumInputLength: 1,
    templateResult: formatResultProveedor,
    templateSelection: formatSelectionProveedor
  }).on('select2:close', function(){
    if(glb_proveedor == null) return;
    if(glb_proveedor.id == '-1') $('#nuevoProveedorModal').modal('show');
  });


  /// SELECT CONSUMIBLE
  $(".js-select2-consumibleAlmacen").select2({
    ajax: {
      url: function(params) {
        return '/consumiblesAlmacen/search/'+params.term;
      },
      dataType: 'json',
      delay: 400,
      cache: true,
      data: function (params) {
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        params.page = params.page || 1;
        data.items = $.map(data.items, function(item){
          item.id = item.consumible_almac_id;
          return item;
        });
        var add_opt = {
          id: '-1',
          text: '---NUEVO CONSUMIBLE DE ALMACEN---'
        };
        data.items.unshift(add_opt);
        return {
          results: data.items,
          pagination: {
          //more: (params.page*2) < data.total_count
            more: 0
          }
        };
      },
    },
    escapeMarckup: function(markup){return markup;},
    minimumInputLength: 1,
    templateResult: formatResultConsumibleAlmacen,
    templateSelection: formatSelectionConsumibleAlmacen
  }).on('select2:close', function(){
    if(glb_consumible_almacen == null) return;
    if(glb_consumible_almacen.id == '-1') $('#nuevoConsumibleModal').modal('show');
  });

  /// UPDATE DATA
  if (glb_obra!=null)
    update_select($("#ordenCompraForm [name='obra']"), glb_obra.id , glb_obra.descripcion);


  /// COMPROBANTES TABLE
  $("#js-btn-comprobante").click(function(){
    var proveedor_id = $(".js-comprobante [name='proveedor']").val();
    var tipo = $(".js-comprobante [name='tipo']").val();
    var numero = $(".js-comprobante [name='numero']").val();

    var comprobante = {};
    comprobante.proveedorid = proveedor_id;
    comprobante.tipo = tipo;
    comprobante.numero = numero;
    glb_comprobantes.push(comprobante);

    var html = '<tr>'+
                '<td scope="row">'+glb_comprobantes.length+'</td>'+
                '<td>'+glb_proveedor.nombre+'</td>'+
                '<td>'+((comprobante.tipo==1)?'Factura':'Guia')+'</td>'+
                '<td>'+comprobante.numero+'</td>'+
                '<td>'+ '<button type="button" class="js-delete btn btn-outline-danger" js-id="'+glb_comprobantes.length+'">x</button>' +'</td>'
                '</tr>';

    $('#js-comprobantes >tbody:last-child').append(html);
    return false;
  });
  $("#js-comprobantes").on('click', '.js-delete', function(){
    var id = $(this).attr('js-id');
    glb_comprobantes.splice(id-1,1);
    $(this).parent().parent().remove();
    update_table_numeration($('#js-comprobantes'));
    return false;
  });

  /// DETALLES OC TABLE
  $("#js-btn-detalle_oc").click(function(){
    var consumible_almac_id = $(".js-detalle_oc [name='consumibleAlmacen']").val();
    var cantidad = $(".js-detalle_oc [name='cantidad']").val();
    var precio_unitario = $(".js-detalle_oc [name='precio_unitario']").val();
    var importe = cantidad*precio_unitario;
    importe = importe.toFixed(2);

    var detalle_oc = {};
    detalle_oc.consumible_almac_id = consumible_almac_id;
    detalle_oc.cantidad = cantidad;
    detalle_oc.precio_unitario = precio_unitario;
    detalle_oc.importe = importe;
    glb_detalles_oc.push(detalle_oc);
    console.log(glb_consumible_almacen);

    var html = '<tr>'+
                '<td scope="row">'+glb_detalles_oc.length+'</td>'+
                '<td>'+glb_consumible_almacen.codigo+'</td>'+
                '<td>'+glb_consumible_almacen.descripcion+'</td>'+
                '<td>'+glb_consumible_almacen.marca+'</td>'+
                '<td>'+glb_consumible_almacen.unidad+'</td>'+
                '<td>'+cantidad+'</td>'+
                '<td>'+precio_unitario+'</td>'+
                '<td>'+importe.toLocaleString()+'</td>'+
                '<td>'+ '<button type="button"class="js-delete btn btn-outline-danger" js-id="'+glb_detalles_oc.length+'">x</button>' +'</td>'
                '</tr>';
    $('#js-detalles_oc >tbody:last-child').append(html);
    var total = 0;
    for (var i = 0; i < glb_detalles_oc.length; i++) {
        total += parseFloat(glb_detalles_oc[i].importe);
    }
    var total_html = total.toFixed(2).toLocaleString();
    $('.js-total').val(total_html);
    return false;
  });

  $("#js-detalles_oc").on('click', '.js-delete', function(){
    var id = $(this).attr('js-id');
    console.log(id);
    glb_detalles_oc.splice(id-1,1);
    $(this).parent().parent().remove();
    update_table_numeration($('#js-detalles_oc'));
    return false;
  });

  ///GUARDANDO EN BD
  $('.js-btn-guardar-oc').click(function(){
    var codigo=$("#ordenCompraForm [name='codigo']").val();
    var codigo_solicitud=$("#ordenCompraForm [name='codigo_solicitud']").val();
    var fecha=$("#ordenCompraForm [name='fecha']").val();
    var total=$("#ordenCompraForm [name='total']").val();
    var obra=$("#ordenCompraForm [name='obra']").val();

    var oc = {};
    oc.codigo = codigo;
    oc.codigo_solicitud = codigo_solicitud;
    oc.fecha = fecha;
    oc.total = total;
    oc.obra = obra;
    oc.comprobantes = glb_comprobantes;
    oc.detalles = glb_detalles_oc;
    oc._token = CSRF_TOKEN;

    var url = '/ordencompra';
    if (glb_oc_id!=null) {
      oc.oc_id = glb_oc_id;
      url = '/ordencompra/update/' + oc.oc_id;
    }
    
    console.log(oc);

    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: oc
    })
    .done(function(res) {
      console.log(res,"success");
      // redirecciono
      window.location.replace("/ordencompra");
    })
    .fail(function(fail) {
      console.log(fail,"error");
    });

    return false;

  });



  ////////// MODALS /////////
  /// NUEVA OBRA
  $('#nuevaObraModal .js-btn-save').click(function() {
    var num_meta = $("#nuevaObraModal [name='num_meta']").val();
    var descripcion = $("#nuevaObraModal [name='descripcion']").val();
    var obra = {
      num_meta: num_meta,
      descripcion: descripcion
    };
    obra._token = CSRF_TOKEN;

    $.ajax({
      url: '/obras/save',
      type: 'POST',
      dataType: 'json',
      data: obra
    })
    .done(function(res) {
      glb_obra = res.result;
      var id = res.result.obra_id;
      var text = res.result.descripcion;
      $('#nuevaObraModal').modal('hide');

      $("#ordenCompraForm [name='obra']").append($('<option></option>').val(id).html(text));
      $("#ordenCompraForm [name='obra']").select2("trigger", "select", {
        data: { id: id }
      });
    })
    .fail(function(fail) {
      console.log(fail,'error');
    });
    return false;
  });

  /// NUEVO PROVEEDOR
  $('#nuevoProveedorModal .js-btn-save').click(function() {
    var nombre    = $("#nuevoProveedorModal [name='nombre']").val();
    var ruc       = $("#nuevoProveedorModal [name='ruc']").val();
    var telefono  = $("#nuevoProveedorModal [name='telefono']").val();
    var direccion = $("#nuevoProveedorModal [name='direccion']").val();
    var proveedor = {
      nombre: nombre,
      ruc: ruc,
      telefono: telefono,
      direccion: direccion
    };
    proveedor._token = CSRF_TOKEN;

    $.ajax({
      url: '/proveedores/save',
      type: 'POST',
      dataType: 'json',
      data: proveedor
    })
    .done(function(res) {
      glb_proveedor = res.result;
      var id = res.result.proveedor_id;
      var text = res.result.nombre;
      $('#nuevoProveedorModal').modal('hide');

      $("#ordenCompraForm [name='proveedor']").append($('<option></option>').val(id).html(text));
      $("#ordenCompraForm [name='proveedor']").select2("trigger", "select", {
        data: { id: id }
      });
    })
    .fail(function(fail) {
      console.log(fail,'error');
    });
    return false;
  });

  /// NUEVO CONSUMIBLE ALMACEN
  $('#nuevoConsumibleModal .js-btn-save').click(function() {
    var codigo      = $("#nuevoConsumibleModal [name='codigo']").val();
    var descripcion = $("#nuevoConsumibleModal [name='descripcion']").val();
    var unidad      = $("#nuevoConsumibleModal [name='unidad']").val();
    var marca       = $("#nuevoConsumibleModal [name='marca']").val();
    var precio_ref  = $("#nuevoConsumibleModal [name='precio_ref']").val();
    var tipo  = $("#nuevoConsumibleModal [name='tipo']").val();
    var consumible = {
      codigo: codigo,
      descripcion: descripcion,
      unidad: unidad,
      marca: marca,
      precio_ref: precio_ref,
      tipo: tipo
    };
    consumible._token = CSRF_TOKEN;

    $.ajax({
      url: '/consumiblesAlmacen/save',
      type: 'POST',
      dataType: 'json',
      data: consumible
    })
    .done(function(res) {
      glb_consumible_almacen = res.result;
      var id = res.result.consumible_almac_id;
      var text = res.result.descripcion;
      $('#nuevoConsumibleModal').modal('hide');

      update_select($("#ordenCompraForm [name='consumibleAlmacen']"), id, text);

//      $("#ordenCompraForm [name='consumibleAlmacen']").append($('<option></option>').val(id).html(text));
//      $("#ordenCompraForm [name='consumibleAlmacen']").select2("trigger", "select", {
//        data: { id: id }
//      });
    })
    .fail(function(fail) {
      console.log(fail,'error');
    });
    return false;
  });
});
