<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/maquinarias/list', 'MaquinariasController@data');

Route::resource('maquinarias', 'MaquinariasController');
Route::resource('maquinarias.mpreventivos', 'MpreventivosController' );

Route::get('/ubicacion/provincia/{id}', 'UbicacionController@maquinarias');
Route::get('/ubicacion/maquinaria/{serie}', 'UbicacionController@buscarMaquinaria');

Route::get('/ubicacion/search/{term}', 'UbicacionController@search');

Route::resource('ubicacion', 'UbicacionController');

Route::resource('admin','AdminController');


Route::resource('maquinarias.mcorrectivos', 'McorrectivosController' );

Route::resource('maquinarias.receta', 'RecetasController' );

Route::resource('maquinarias.movimientos', 'MovimientosController' );

Route::get('/maquinarias/{idMaquinaria}/receta/{idReceta}/detalle/create', 'DetalleRecetaController@create');
Route::post('/detalle', 'DetalleRecetaController@store');
Route::delete('/detalle', 'DetalleRecetaController@destroy');

Route::resource('obras','ObrasController');

Route::resource('proveedores','ProveedoresController');

Route::get('/obras/search/{term}', 'ObrasController@search');
Route::post('/obras/save', 'ObrasController@save');

Route::get('/proveedores/search/{term}', 'ProveedoresController@search');
Route::post('/proveedores/save', 'ProveedoresController@save');

Route::resource('ordencompra','OrdenCompraController');

Route::resource('consumiblesAlmacen','ConsumiblesAlmacenController');

Route::get('/consumiblesAlmacen/search/{term}', 'ConsumiblesAlmacenController@search');
Route::post('/consumiblesAlmacen/save', 'ConsumiblesAlmacenController@save');

Route::resource('consumibles', 'ConsumiblesAlmacenController');

Route::resource('pecosas','PecosasController');	

Route::get('/ordencompra/search/{term}', 'OrdenCompraController@search');

Route::get('/detalles_oc/search/{term}', 'OrdenCompraController@search_detalle_oc');

Route::post('/ordencompra/update/{id}','OrdenCompraController@update');

Route::resource('stockalmacen','StockAlmacenController');

Route::resource('valeconsumo','ValeConsumoController');
Route::post('/valeconsumo/update/{id}','ValeConsumoController@update');
