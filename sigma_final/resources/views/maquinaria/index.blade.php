@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Maquinarias
				</div>
				<div class="card-block">
					<div class="row ">
						<div class="col">
							<div class="d-flex mb-3 justify-content-end">
								<a href="{{ route('maquinarias.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<!--<div class="table-responsive">-->
							
							<table id="maquinarias" class="display" cellspacing="0" width="100%">
								<thead class="thead-inverse">
									<tr>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Placa</th>
										<th>Tipo</th>
										<th>Nro de registro</th>
										<th>Serie</th>
										<th>Estado</th>
										<th>Horometro</th>
										<th>Condicion</th>
										<th>Año</th>
										<th></th>
									</tr>
								</thead>
								
								<tbody>
									@foreach($maquinarias as $maquinaria)
									<tr>
										
										<td>{{ $maquinaria->marca }}</td>
										<td>{{ $maquinaria->modelo }}</td>
										<td>{{ $maquinaria->placa }}</td>
										<td>{{ $maquinaria->tipo }}</td>
										<td>{{ $maquinaria->nro_registro }}</td>
										<td>{{ $maquinaria->serie }}</td>
										@if ( $maquinaria->estado == 1)
										<td class="text-success font-weight-bold">Operativo</td>
										@elseif ( $maquinaria->estado == 2)
										<td class="text-warning font-weight-bold">------</td>
										@else
										<td class="text-danger font-weight-bold">Inoperativo</td>
										@endif
										
										<td>{{ $maquinaria->horometro }}</td>
										<td class="font-weight-bold {{ ($maquinaria->condicion == 1) ? 'text-success' : 'text-danger'}}">{{ ($maquinaria->condicion == 1) ? 'Activo' : 'Inactivo' }}</td>
										
										<td>{{ $maquinaria->anio }}</td>
										<td>
											<div class="btn-group" role="group">
												<a href="{{ route('maquinarias.show' , $maquinaria->maquinaria_id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
											
												<a href="{{ route('maquinarias.edit' , $maquinaria->maquinaria_id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
											
												<!--<a class="btn btn-danger">
													{!! Form::open([
													'method' => 'DELETE',
													'route' => ['maquinarias.destroy', $maquinaria->maquinaria_id]
													]) !!}
													{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn-danger btn' , 'type' => 'submit']) !!}
													{!! Form::close() !!}
												</a>-->
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							
							<!--</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$(document).ready(function() {
		$('#maquinarias').DataTable({
			"language": {
			    "sProcessing":     "Procesando...",
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningún dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "sLoadingRecords": "Cargando...",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			},
			dom: 'Bfrtip',
            buttons: [  {
                extend: 'pdf',
                text: 'Exportar PDF',
                message:'Lista de las maquinarias registradas en el sistema',
                header:true,
                footer:true,
                orientation:'landscape',

                filename:'Maquinarias',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            },{
                extend: 'excel',
                text: 'Exportar Excel',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            },{
                extend: 'print',
                text: 'Imprimir',
                autoPrint: true
            } ],
			"scrollX": true,
		});
	} );
</script>
@endsection