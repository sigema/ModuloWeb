@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row  justify-content-md-center" >
		<div class="col-md-7">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="row  justify-content-md-center" >
		<div class="col-md-7">
			{!! Form::open([
			'route' => ['maquinarias.update' , $maquinaria->maquinaria_id],
			'method' => 'PUT',
			'files' => true
			]) !!}
			<div class="form-group row">
				{!! Form::label('marca' , 'Marca :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::text('marca' , $maquinaria->marca , ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('modelo' , 'Modelo ;' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::text('modelo' , $maquinaria->modelo , ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('placa' , 'Placa ;' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::text('placa' , $maquinaria->placa , ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('tipo' , 'Tipo :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::text('tipo' , $maquinaria->tipo , ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('nro_registro' , 'Nro de registro :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::text('nro_registro' , $maquinaria->nro_registro , ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('serie' , 'Serie :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::text('serie' , $maquinaria->serie , ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('estado' , 'Estado :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							{!! Form::radio('estado' , 1 , ($maquinaria->estado == 1) ? true : false , [ 'class' => 'form-check-input'])!!}
							Operativo
						</label>
					</div>
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							{!! Form::radio('estado' , 2 , ($maquinaria->estado == 2) ? true : false , [ 'class' => 'form-check-input']) !!}
							-------
						</label>
					</div>
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							{!! Form::radio('estado' , 3 , ($maquinaria->estado == 3) ? true : false , [ 'class' => 'form-check-input']) !!}
							Inoperativo
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('horometro' , 'Horometro :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::number('horometro' , $maquinaria->horometro , ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Condición</label>
				<div class="col-sm-9">
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							{!! Form::radio('condicion' , 1 , ($maquinaria->condicion == 1) ? true : false , [ 'class' => 'form-check-input']) !!}
							Activo
						</label>
					</div>
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							{!! Form::radio('condicion' , 2 , ($maquinaria->condicion == 2) ? true : false, [ 'class' => 'form-check-input']) !!}
							Inactivo
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('foto' , 'Foto :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::file('foto' , ['class' => 'form-control-file']) !!}
				</div>
			</div>
			<div class="form-group row">
				{!! Form::label('anio' , 'Año :' , ['class' => 'col-form-label col-sm-3']) !!}
				<div class="col-sm-9">
					{!! Form::number('anio' , $maquinaria->anio , ['class' => 'form-control']) !!}
				</div>
			</div>
			
			<div class="form-group">
				{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection