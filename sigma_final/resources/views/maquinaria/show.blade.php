@extends('layouts.app')
@section('style')
<style>
	.parpadea {
	animation-name: parpadeo;
	animation-duration: 1s;
	animation-timing-function: linear;
	animation-iteration-count: infinite;
	-webkit-animation-name:parpadeo;
	-webkit-animation-duration: 1s;
	-webkit-animation-timing-function: linear;
	-webkit-animation-iteration-count: infinite;
	}
	@-moz-keyframes parpadeo{
	0% { opacity: 1.0; }
	50% { opacity: 0.0; }
	100% { opacity: 1.0; }
	}
	@-webkit-keyframes parpadeo {
	0% { opacity: 1.0; }
	50% { opacity: 0.0; }
	100% { opacity: 1.0; }
	}
	@keyframes parpadeo {
	0% { opacity: 1.0; }
	50% { opacity: 0.0; }
	100% { opacity: 1.0; }
	}
</style>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<di class="d-flex justify-content-between align-items-center">
					<div>
						<div class="btn-group" role="group" aria-label="Basic example">
							<button type="button" class="btn btn-success active {{ ($state == 'success') ? 'parpadea' : ''}}">O</button>
							<button type="button" class="btn btn-warning active {{ ($state == 'warning') ? 'parpadea' : ''}}">--</button>
							<button type="button" class="btn btn-danger active {{ ($state == 'danger') ? 'parpadea' : ''}}">I</button>
						</div>
					</div>
					<div>
						<h2>{{ $maquinaria->tipo . ' ' . $maquinaria->modelo . ' ' . $maquinaria->nro_registro }}</h2>
					</div>
					<div>
						<h4><strong>Horometro: </strong>{{ $maquinaria->horometro }} hrs</h4>
					</div>
					<a href="{{ route('maquinarias.edit' , $maquinaria->maquinaria_id) }}" class="btn btn-success"><i class="fa fa-pencil"></i> editar</a>
					</di>
				</div>
				<div class="card-block">
					<div class="row">
						<div class="col-md-6">
							<img src="{{URL::asset($maquinaria->foto)}}" class="img-fluid" style="width: 100%">
						</div>
						<!-- Creando una vista de maquinaria-->
						<div class="col-md-6">
							<div class="row">
								<dt class="col-sm-3">Marca :</dt>
								<dd class="col-sm-9">{{ $maquinaria->marca }}</dd>
								<dt class="col-sm-3">Modelo :</dt>
								<dd class="col-sm-9">{{ $maquinaria->modelo }}</dd>
								<dt class="col-sm-3">Placa :</dt>
								<dd class="col-sm-9">{{ $maquinaria->placa }}</dd>
								<dt class="col-sm-3">Tipo :</dt>
								<dd class="col-sm-9">{{ $maquinaria->tipo }}</dd>
								<dt class="col-sm-3">Nro de registro :</dt>
								<dd class="col-sm-9">{{ $maquinaria->nro_registro }}</dd>
								<dt class="col-sm-3">Serie :</dt>
								<dd class="col-sm-9">{{ $maquinaria->serie }}</dd>
								<dt class="col-sm-3">Año :</dt>
								<dd class="col-sm-9">{{ $maquinaria->anio }}</dd>
								
								<dt class="col-sm-3">Estado :</dt>
								<dd class="col-sm-9">
								@if ( $maquinaria->estado == 1)
								<span class="text-success font-weight-bold">Operativo</span>
								@elseif ( $maquinaria->estado == 2)
								<span class="text-warning font-weight-bold">------</span>
								@else
								<span class="text-danger font-weight-bold">Inoperativo</p>
								@endif</dd>
								<dt class="col-sm-3">Horometro :</dt>
								<dd class="col-sm-9">{{ $maquinaria->horometro }} hrs</dd>
								<dt class="col-sm-3">Condicion :</dt>
								<dd class="col-sm-9 font-weight-bold {{ ($maquinaria->condicion == 1) ? 'text-success' : 'text-danger'}}">{{ ($maquinaria->condicion == 1) ? 'Activo' : 'Inactivo' }}</dd>
								<!--dt class="col-sm-3"><a href=" url('pdf/maquinaria/'. $maquinaria->maquinaria_id) }}" class="btn btn-secondary btn-lg"><i class="fa fa-file-pdf-o"></i> Exportar a pdf</a></dt-->
							</div>
						</div>
					</div>
					<!-- Creando una vista de maquinaria-->
					<!--table class="table mb-2 table-responsive">
						<thead class="thead-inverse">
								<tr>
											<th>Marca</th>
											<th>Modelo</th>
											<th>Tipo</th>
											<th>Nro de registro</th>
											<th>Serie</th>
											<th>Estado</th>
											<th>Horometro</th>
											<th>Condicion</th>
											<th>Foto</th>
											<th>Año</th>
								</tr>
						</thead>
						<tbody>
								<tr>
											<td>{ $maquinaria->marca }}</td>
											<td>{ $maquinaria->modelo }}</td>
											<td>{ $maquinaria->tipo }}</td>
											<td>{ $maquinaria->nro_registro }}</td>
											<td>{ $maquinaria->serie }}</td>
											if ( $maquinaria->estado == 1)
												<td>Operativo</td>
											elseif ( $maquinaria->estado == 2)
												<td>------</td>
											else
												<td>Inoperativo</td>
											endif
											<td>{ $maquinaria->horometro }}</td>
											<td>{ ($maquinaria->condicion == 1) ? 'Activo' : 'Inactivo' }}</td>
											<td>{ $maquinaria->foto }}</td>
											<td>{ $maquinaria->anio }}</td>
								</tr>
						</tbody>
				</table-->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" href="#mpreventivos" role="tab" data-toggle="tab">Mantenimientos Preventivos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#mcorrectivos" role="tab" data-toggle="tab">Mantenimientos Correctivos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#movimientos" role="tab" data-toggle="tab">Movimientos</a>
					</li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane in active" id="mpreventivos">
						@include('mpreventivos.show' , ['mps' => $mps , 'receta' => $receta])
					</div>
					<div role="tabpanel" class="tab-pane fade" id="mcorrectivos">
						@include('mcorrectivos.show' , ['mcs' => $mcs])
					</div>
					<div role="tabpanel" class="tab-pane fade" id="movimientos">
						@include('movimientos.show' , ['mov' => $mov])
					</div>
				</div>
				
				<div class="d-flex">
					@if($receta != null)
					<a href="{{ route('maquinarias.receta.index' , $maquinaria->maquinaria_id) }}" class="btn btn-primary" >Programa de Mantenimiento</a>
					@else
					<a href="{{ route('maquinarias.receta.create' , $maquinaria->maquinaria_id) }}" class="btn btn-danger" >Crear Programa de Mantenimiento</a>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection