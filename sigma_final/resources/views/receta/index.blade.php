@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<p class="lead">Maquinaria : {{ $maquinaria->nro_registro }}</p>
				</div>
				<div class="card-block">
					<div class="d-flex justify-content-between align-items-center">
						<div>
							<p><div class="font-weight-bold">Periodo : </div> {{$receta->periodo}}</p>
							<p><div class="font-weight-bold">Horas de trabajo : </div> {{$receta->horas_trabajo}}</p>
						</div>
						<div>
							<a href="{{ route('maquinarias.receta.edit' , [ 'idMaquinaria' => $maquinaria->maquinaria_id , 'idReceta' => $receta->receta_id ]) }}" class="btn btn-success"><i class="fa fa-pencil"></i> Editar Receta</a>
						</div>
					</div>
					<div class="d-flex mb-3 justify-content-end">
						<a href="{{ url( '/maquinarias/' . $maquinaria->maquinaria_id . '/receta/' . $receta->receta_id . '/detalle/create' ) }}" class="btn btn-primary">Agregar</a>
					</div>
					<h3>Lubricantes</h3>
					<div class="table-responsive">
						<table class="table">
							<thead class="thead-inverse">
								<th>Consumible</th>
								<th>Unidad</th>
								@for($i = 250 ; $i <= 5000 ; $i+=250)
								<th>{{$i}}</th>
								@endfor
								<th>Eliminar</th>
							</thead>
							<tbody>

								@foreach($detalleL as $detalle)
								<tr>
									<td>{{$detalle->consumible->descripcion}}</td>
									<td>{{$detalle->consumible->unidad}}</td>
									
									@foreach($receta->detalleRecetas()->where('consumible_id' , $detalle->consumible->consumible_almac_id)->get() as $detalleS)
									<td>
										{{ $detalleS->cantidad}}
									</td>
									@endforeach
									<td>
										{!! Form::open([
										'method' => 'DELETE',
										'url' => '/detalle/' ]) !!}
										{!! Form::text('maquinaria' , $maquinaria->maquinaria_id , [ 'hidden' ])!!}
										{!! Form::text('receta' , $receta->receta_id , [ 'hidden' ])!!}
										{!! Form::text('consumible' , $detalle->consumible->consumible_almac_id , [ 'hidden' ])!!}
										{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
										{!! Form::close() !!}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<h3>Filtros</h3>
					<div class="table-responsive">
						<table class="table">
							<thead class="thead-inverse">
								<th>Consumible</th>
								<th>Unidad</th>
								@for($i = 250 ; $i <= 5000 ; $i+=250)
								<th>{{$i}}</th>
								@endfor
								<th>Eliminar</th>
							</thead>
							<tbody>

								@foreach($detalleF as $detalle)
								<tr>
									<td>{{$detalle->consumible->descripcion}}</td>
									<td>{{$detalle->consumible->unidad}}</td>
									
									@foreach($receta->detalleRecetas()->where('consumible_id' , $detalle->consumible->consumible_almac_id)->get() as $detalleS)
									<td>
										{{ $detalleS->cantidad}}
									</td>
									@endforeach
									<td>
										{!! Form::open([
										'method' => 'DELETE',
										'url' => '/detalle/' ]) !!}
										{!! Form::text('maquinaria' , $maquinaria->maquinaria_id , [ 'hidden' ])!!}
										{!! Form::text('receta' , $receta->receta_id , [ 'hidden' ])!!}
										{!! Form::text('consumible' , $detalle->consumible->consumible_almac_id , [ 'hidden' ])!!}
										{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
										{!! Form::close() !!}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<h3>Elementos de desgaste</h3>
					<div class="table-responsive">
						<table class="table">
							<thead class="thead-inverse">
								<th>Consumible</th>
								<th>Unidad</th>
								@for($i = 250 ; $i <= 5000 ; $i+=250)
								<th>{{$i}}</th>
								@endfor
								<th>Eliminar</th>
							</thead>
							<tbody>

								@foreach($detalleE as $detalle)
								<tr>
									<td>{{$detalle->consumible->descripcion}}</td>
									<td>{{$detalle->consumible->unidad}}</td>
									
									@foreach($receta->detalleRecetas()->where('consumible_id' , $detalle->consumible->consumible_almac_id)->get() as $detalleS)
									<td>
										{{ $detalleS->cantidad}}
									</td>
									@endforeach
									<td>
										{!! Form::open([
										'method' => 'DELETE',
										'url' => '/detalle/' ]) !!}
										{!! Form::text('maquinaria' , $maquinaria->maquinaria_id , [ 'hidden' ])!!}
										{!! Form::text('receta' , $receta->receta_id , [ 'hidden' ])!!}
										{!! Form::text('consumible' , $detalle->consumible->consumible_almac_id , [ 'hidden' ])!!}
										{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
										{!! Form::close() !!}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection