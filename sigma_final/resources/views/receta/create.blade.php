@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row  justify-content-md-center" >
		<div class="col-md-7">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<p class="lead">Maquinaria : {{ $maquinaria->nro_registro }}</p>
				</div>
				<div class="card-block">
					{!! Form::open([
					'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/receta',
					'method' => 'POST'
					]) !!}
					<div class="form-group row">
						{!! Form::label('periodo' , 'Periodo :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('periodo' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('horas_trabajo' , 'Horas de trabajo :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('horas_trabajo' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="d-flex justify-content-end">
						{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection