@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row  justify-content-md-center" >
		<div class="col-md-7">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			

		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<p class="lead">Maquinaria : {{ $maquinaria->nro_registro }}</p>
				</div>
				<div class="card-block">
					{!! Form::open([
					'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/receta/' . $receta->receta_id  ,
					'method' => 'PUT'
					]) !!}
					<div class="form-group row">
						{!! Form::label('periodo' , 'Periodo :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('periodo' , $receta->periodo , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('horas_trabajo' , 'Horas de trabajo :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('horas_trabajo' , $receta->horas_trabajo , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="d-flex justify-content-end">
						{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
					</div>
					{!! Form::close() !!}


					<br>
					
					<h3>Lubricantes</h3>
					<div class="table-responsive">
						<table class="table">
							<thead class="thead-inverse">
								<th>Consumible</th>
								<th>Unidad</th>
								@for($i = 250 ; $i <= 5000 ; $i+=250)
								<th>{{$i}}</th>
								@endfor
								<th>Eliminar</th>
							</thead>
							<tbody>

								@foreach($detalleL as $detalle)
								<tr>
									<td>{{$detalle->consumible->descripcion}}</td>
									<td>{{$detalle->consumible->unidad}}</td>
									
									@foreach($receta->detalleRecetas()->where('consumible_id' , $detalle->consumible->consumible_almac_id)->get() as $detalleS)
									<td>
										{{ $detalleS->cantidad}}
									</td>
									@endforeach
									<td>
										{!! Form::open([
										'method' => 'DELETE',
										'url' => '/detalle/' ]) !!}
										{!! Form::text('maquinaria' , $maquinaria->maquinaria_id , [ 'hidden' ])!!}
										{!! Form::text('receta' , $receta->receta_id , [ 'hidden' ])!!}
										{!! Form::text('consumible' , $detalle->consumible->consumible_almac_id , [ 'hidden' ])!!}
										{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
										{!! Form::close() !!}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<h3>Filtros</h3>
					<div class="table-responsive">
						<table class="table">
							<thead class="thead-inverse">
								<th>Consumible</th>
								<th>Unidad</th>
								@for($i = 250 ; $i <= 5000 ; $i+=250)
								<th>{{$i}}</th>
								@endfor
								<th>Eliminar</th>
							</thead>
							<tbody>

								@foreach($detalleF as $detalle)
								<tr>
									<td>{{$detalle->consumible->descripcion}}</td>
									<td>{{$detalle->consumible->unidad}}</td>
									
									@foreach($receta->detalleRecetas()->where('consumible_id' , $detalle->consumible->consumible_almac_id)->get() as $detalleS)
									<td>
										{{ $detalleS->cantidad}}
									</td>
									@endforeach
									<td>
										{!! Form::open([
										'method' => 'DELETE',
										'url' => '/detalle/' ]) !!}
										{!! Form::text('maquinaria' , $maquinaria->maquinaria_id , [ 'hidden' ])!!}
										{!! Form::text('receta' , $receta->receta_id , [ 'hidden' ])!!}
										{!! Form::text('consumible' , $detalle->consumible->consumible_almac_id , [ 'hidden' ])!!}
										{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
										{!! Form::close() !!}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<h3>Elementos de desgaste</h3>
					<div class="table-responsive">
						<table class="table">
							<thead class="thead-inverse">
								<th>Consumible</th>
								<th>Unidad</th>
								@for($i = 250 ; $i <= 5000 ; $i+=250)
								<th>{{$i}}</th>
								@endfor
								<th>Eliminar</th>
							</thead>
							<tbody>

								@foreach($detalleE as $detalle)
								<tr>
									<td>{{$detalle->consumible->descripcion}}</td>
									<td>{{$detalle->consumible->unidad}}</td>
									
									@foreach($receta->detalleRecetas()->where('consumible_id' , $detalle->consumible->consumible_almac_id)->get() as $detalleS)
									<td>
										{{ $detalleS->cantidad}}
									</td>
									@endforeach
									<td>
										{!! Form::open([
										'method' => 'DELETE',
										'url' => '/detalle/' ]) !!}
										{!! Form::text('maquinaria' , $maquinaria->maquinaria_id , [ 'hidden' ])!!}
										{!! Form::text('receta' , $receta->receta_id , [ 'hidden' ])!!}
										{!! Form::text('consumible' , $detalle->consumible->consumible_almac_id , [ 'hidden' ])!!}
										{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
										{!! Form::close() !!}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection