@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row  justify-content-md-center" >
		<div class="col-md-12">
			@if (session('flash_message'))
			<div class="alert alert-danger">
				<ul>
					<li>{{ session('flash_message') }}</li>
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<p class="lead">Nuevo</p>
				</div>
				<div class="card-block">
					
					
					{!! Form::open([
					'url' => '/detalle',
					'method' => 'POST'
					]) !!}
					<input type="text" hidden name="maquinaria" value="{{ $maquinaria->maquinaria_id }}" class="form-control">
					<input type="text" hidden name="receta" value="{{ $receta->receta_id }}" class="form-control">
					<div class="form-group row">
						{!! Form::label('consumible' , 'Consumible :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							
							<select id="consumible" name="consumible">
								@foreach($consumibles as $consumible)
								<option value="{{ $consumible->consumible_almac_id }}">{{$consumible->descripcion}}</option>
								@endforeach
							</select>
						</div>
					</div>
					@for($i = 250 ; $i <= 5000 ; $i+=250)
					<div class="form-group row">
						{!! Form::label('hora . $i' , $i . ' :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">

							{!! Form::number('hora' . $i , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					@endfor
					<!--<div class="table-responsive">
								<table class="table">
											<thead class="thead-inverse">
														<td>Consumible</td>
														@for($i = 250 ; $i <= 5000 ; $i+=250)
														<td>{{$i}}</td>
														@endfor
														<td>Accion</td>
											</thead>
											<tbody>
														<tr>
																	<td>
																				<select id="consumible" name="consumible">
																							@foreach($consumibles as $consumible)
																							<option value="{{ $consumible->consumible_id }}">{{$consumible->descripcion}}</option>
																							@endforeach
																				</select>
																	</td>
																	@for($i = 250 ; $i <= 5000 ; $i+=250)
																	<td><input type="text" name="hora{{$i}}" class="form-control"></td>
																	@endfor
																	<td>{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}</td>
														</tr>
											</tbody>
								</table>
					</div>-->
					{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$(document).ready(function() {
$("#consumible").select2();
});
</script>
@endsection