@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="card">
    <div class="card-header">
        Ver Orden de Compra
    </div>
    <div id="ordenCompraForm" class="card-block">
      {!! Form::open([
          'route' => ['ordencompra.show' , $ordenCompra->oc_id],
          ]) !!}
      <div class="row" >
        <div class="col-12 col-md-8 offset-md-2">
        <fieldset disabled>
          <div class="form-group row">
            {!! Form::label('codigo' , 'Código de Orden de Compra :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('codigo' , $ordenCompra->codigo , ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('codigo_solicitud' , 'Código de Solicitud ;' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('codigo_solicitud' , $ordenCompra->codigo_solicitud , ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('fecha' , 'Fecha Ingreso:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('fecha' , $ordenCompra->fecha_ingreso , ['class' => 'form-control']) !!}
            </div>
          </div>          
          <div class="form-group row">
            {!! Form::label('Obra' , 'Obra:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('Obra' , $obra->descripcion , ['class' => 'form-control']) !!}
            </div>
          </div>          
        </div>
        </fieldset>
      </div>
      <div class="row">
        <div class="col-12 col-md-10 offset-md-1">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Comprobantes
                  </a>
                </h5>
              </div>

              <div id="collapseOne">
                <div class="card-block">
                  <div class="container">
                    <div class="card-block">
                    <div class="row">
                        <table class="table" id="js-comprobantes">
                          <thead class="thead-inverse">
                            <tr>                              
                              <th>#</th>
                              <th>Nombre Proveedor</th>
                              <th>Tipo</th>
                              <th>Numero</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($comprobantes as $item)
                          <tr>
		                    <td scope="row">{{ $loop->iteration }}</td>
		                    <td>{{ $item->nombre }}</td>
		                    <td>{{ $item->tipo == 1  ? 'Factura' : 'Guía'	}}</td>
		                    <td>{{ $item->num_comprobante }}</td>                    
		                  </tr>
		                  @endforeach
                          </tbody>
                        </table>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-controls="collapseTwo">
                    Detalle de Orden de Compra
                  </a>
                </h5>
              </div>
              <div id="collapseTwo">
                <div class="card-block">
                  <div class="container">
                    <div class="card-block">
                    <div class="row">
                        <table class="table" id="js-detalles_oc">
                          <thead class="thead-inverse">
                            <tr>                              
                              <th>#</th>
                              <th>Código</th>
                              <th>Descripción</th>
                              <th>Marca</th>
                              <th>Unidad</th>
                              <th>Cantidad</th>
                              <th>Precio Unitario</th>
                              <th>Importe</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($d_oc as $item)
                          <tr>
		                    <td scope="row">{{ $loop->iteration }}</td>
		                    <td>{{ $item->codigo }}</td>
		                    <td>{{ $item->descripcion }}</td>
		                    <td>{{ $item->marca }}</td>
		                    <td>{{ $item->unidad }}</td>
		                    <td>{{ $item->cantidad }}</td>
		                    <td>{{ $item->precio_unitario }}</td>
		                    <td>{{ $item->importe }}</td>
		                  </tr>
		                  @endforeach
                          </tbody>
                        </table>
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <br>
  
        <div class="row">
          <div class="col-8 offset-md-2">
            <fieldset disabled>  
            <div class="form-group row">

              {!! Form::label('Total' , 'Total:' , ['class' => 'col-form-label col-sm-3']) !!}
              <div class="col-sm-9">
                {!! Form::text('total' , $ordenCompra->total , ['class' => 'form-control js-total']) !!}
              </div>
            </div>
            </fieldset>
          </div>  
        </div>
      </div>
      {!! Form::close() !!}
            
    </div>    
  </div>
</div>

@endsection
@section('script')
<script src="{{ asset('js/orden_compra.js') }}"></script>
<script>

set_oc_id( '{{$ordenCompra->oc_id}}' );
set_obra( '{{$obra->obra_id}}',
    '{{$obra->num_meta}}',
    '{{$obra->descripcion}}');

@foreach($comprobantes as $item)
add_comprobante(
    '{{$item->nombre}}',
    '{{$item->tipo}}',
    '{{$item->num_comprobante}}',
    '{{$item->proveedor_id}}');
@endforeach
@foreach($d_oc as $item)
add_detalle_oc(
    '{{$item->consumible_almac_id}}',
    '{{$item->cantidad}}',
    '{{$item->precio_unitario}}',
    '{{$item->importe}}');
@endforeach
</script>



@endsection
