@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="card">
    <div class="card-header">
        Crear Orden de Compra
    </div>
    <div id="ordenCompraForm" class="card-block">
      {!! Form::open([
          'route' => 'ordencompra.store',
          'method' => 'POST'
          ]) !!}
      <div class="row" >
        <div class="col-12 col-md-8 offset-md-2">
          <div class="form-group row">
            {!! Form::label('codigo' , 'Código de Orden de Compra :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('codigo' , null , ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('codigo_solicitud' , 'Código de Solicitud ;' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('codigo_solicitud' , null , ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('fecha' , 'Fecha Ingreso:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::date('fecha' , \Carbon\Carbon::now() , ['class' => 'form-control']) !!}
            </div>
          </div>          
          <div class="form-group row">
            {!! Form::label('obra' , 'Obra :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              <select name="obra" class="js-select2-remote-obra" style="width: 100%; height: 100%;">
                <option value="0" selected="selected">Obra</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-10 offset-md-1">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Comprobantes
                  </a>
                </h5>
              </div>

              <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                  <div class="row js-comprobante">
                    <div class="col-12 col-md-3">
                      <select name="proveedor" class="js-select2-proveedor" style="width: 100%; height: 100%;">
                        <option value="0" selected="selected">Proveedor</option>
                      </select>
                    </div>
                    <div class="col-12 col-md-3">
                      <select name="tipo" id="" class="form-control">
                        <option value="1">
                          Factura
                        </option>
                        <option value="2">
                          Guía
                        </option>
                      </select>
                    </div>
                    <div class="col-12 col-md-3">
                      <input type="text" name="numero" class="form-control" placeholder="numero">                      
                    </div>
                    <div class="col-12 col-md-3">
                      <button id="js-btn-comprobante" class="btn btn-block btn-primary">Agregar</button>
                    </div>
                  </div>                  
                  <div class="container">
                    <div class="card-block">
                    <div class="row">
                        <table class="table" id="js-comprobantes">
                          <thead class="thead-inverse">
                            <tr>
                              <th>#</th>
                              <th>Nombre Proveedor</th>
                              <th>Tipo</th>
                              <th>Numero</th>
                              <th>Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Detalle de Orden de Compra
                  </a>
                </h5>
              </div>
              <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="card-block">
                  <div class="row js-detalle_oc">
                    <div class="col-12 col-md-3">
                      <select name="consumibleAlmacen" class="js-select2-consumibleAlmacen" style="width: 100%; height: 100%;">
                        <option value="0" selected="selected">Consumible</option>
                      </select>
                    </div>
                    <div class="col-12 col-md-3">
                      <input type="text" name="cantidad" class="form-control" placeholder="cantidad">
                    </div>
                    <div class="col-12 col-md-3">
                      <input type="text" name="precio_unitario" class="form-control" placeholder="precio unitario">
                    </div>
                    <div class="col-12 col-md-3">
                      <button id="js-btn-detalle_oc" class="btn btn-block btn-primary">Agregar</button>
                    </div>
                  </div>
                  <div class="container">
                    <div class="card-block">
                    <div class="row">
                        <table class="table table-responsive" id="js-detalles_oc">
                          <thead class="thead-inverse">
                            <tr>
                              <th>#</th>
                              <th>Código</th>
                              <th>Descripción</th>
                              <th>Marca</th>
                              <th>Unidad</th>
                              <th>Cantidad</th>
                              <th>Precio Unitario</th>
                              <th>Importe</th>
                              <th>Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <br>
  
        <div class="row">
          <div class="col-8 offset-md-2">
            <fieldset disabled>  
            <div class="form-group row">

              {!! Form::label('Total' , 'Total:' , ['class' => 'col-form-label col-sm-3']) !!}
              <div class="col-sm-9">
                {!! Form::text('total' , null , ['class' => 'form-control js-total']) !!}
              </div>
            </div>
            </fieldset>
          </div>  
        </div>
      </div>
      <div class="row">
        <div class="col-8 offset-md-2">
          <div class="form-group float-right" style="margin-right: 15px;">
            {!! Form::submit('Guardar' , ['class' => 'btn btn-success js-btn-guardar-oc']) !!}
          </div>  
        </div>        
      </div>
      {!! Form::close() !!}
            
    </div>    
  </div>
</div>


<!-- MODAL DE NUEVA OBRA -->
<div class="modal" id="nuevaObraModal" tabindex="-1" role="dialog" aria-labelledby="nuevaObraModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva Obra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="form-group row">
                <label for="num_meta" class="col-form-label col-4">Nro de Meta :</label>
                <div class="col-8"><input name="num_meta" type="text" id="num_meta" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="descripcion" class="col-form-label col-4">Descripcion :</label>
                <div class="col-8"><input name="descripcion" type="text" id="descripcion" class="form-control"></div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary js-btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DE NUEVO PROVEEDOR -->
<div class="modal" id="nuevoProveedorModal" tabindex="-1" role="dialog" aria-labelledby="nuevoProveedorModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="form-group row">
                <label for="nombre" class="col-form-label col-4">Nombre :</label>
                <div class="col-8"><input name="nombre" type="text" id="nombre" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="ruc" class="col-form-label col-4">Ruc :</label>
                <div class="col-8"><input name="ruc" type="text" id="ruc" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="telefono" class="col-form-label col-4">Telefono :</label>
                <div class="col-8"><input name="telefono" type="text" id="telefono" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="direccion" class="col-form-label col-4">Direccion :</label>
                <div class="col-8"><input name="direccion" type="text" id="direccion" class="form-control"></div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary js-btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL DE NUEVO CONSUMIBLE ALMACEN -->
<div class="modal" id="nuevoConsumibleModal" tabindex="-1" role="dialog" aria-labelledby="nuevaConsumibleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Consumible de Almacen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="form-group row">
                <label for="codigo" class="col-form-label col-4">Codigo :</label>
                <div class="col-8"><input name="codigo" type="text" id="codigo" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="descripcion" class="col-form-label col-4">Descripcion :</label>
                <div class="col-8"><input name="descripcion" type="text" id="descripcion" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="unidad" class="col-form-label col-4">Unidad :</label>
                <div class="col-8"><input name="unidad" type="text" id="unidad" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="marca" class="col-form-label col-4">Marca :</label>
                <div class="col-8"><input name="marca" type="text" id="marca" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="precio_ref" class="col-form-label col-4">Precio Referencial :</label>
                <div class="col-8"><input name="precio_ref" type="text" id="precio_ref" class="form-control"></div>
              </div>
              <div class="form-group row">
               <label for="precio_ref" class="col-form-label col-4">Tipo :</label>
                <div class="col-8">
                <select name="tipo" id="tipo" class="form-control">
                  <option value="1">Filtros</option>
                  <option value="2">Lubricantes</option>
                  <option value="3">Elementos de desgaste</option>
                </select>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary js-btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section('script')
<script src="{{ asset('js/orden_compra.js') }}"></script>
@endsection
