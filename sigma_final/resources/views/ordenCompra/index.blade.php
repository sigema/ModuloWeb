@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Ordenes de Compras
        </div>
        <div class="card-block">
          <div class="row ">
            <div class="col">
              <div class="d-flex mb-3 justify-content-end">
                <a href="{{ route('ordencompra.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <!--<div class="table-responsive">-->
              <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead class="thead-inverse">
                  <tr>
                    <th>#</th>
                    <th>Codigo</th>
                    <th>Codigo de Solicitud</th>
                    <th>Fecha de Ingreso</th>
                    <th>Total</th>
                    <th>Obra</th>
                    <th>Ver</th>
                    <th>Editar</th>
                    <!-- <th>Eliminar</th> -->
                  </tr>
                </thead>
                <tbody>
                  @foreach($ordenCompras as $item)
                  <tr>
                    <td scope="row">{{ $loop->iteration }}</td>
                    <td>{{ $item->codigo }}</td>
                    <td>{{ $item->codigo_solicitud }}</td>
                    <td>{{ $item->fecha_ingreso }}</td>
                    <td>{{ $item->total }}</td>
                    <td>{{ $item->descripcion }}</td>
                    <td>
                      <a href="{{ route('ordencompra.show' , $item->oc_id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    </td>
                    <td>
                      <a href="{{ route('ordencompra.edit' , $item->oc_id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                    </td>
                    <!-- <td>
                      <a>
                        {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['ordencompra.destroy', $item->oc_id]
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                      </a>
                    </td> -->
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div>
                {{ $ordenCompras->links('vendor.pagination.bootstrap-4') }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    var table = $('#dataTable').DataTable({
      dom: 'Bfrtip',
    buttons: [
        'pdf', 'excel', 'print'
    ],
      "scrollX": true,
    });
  } );
</script>
@endsection
