@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Pedido comprobante de salida
        </div>
        <div class="card-block">
          <div class="row ">
            <div class="col">
              <div class="d-flex mb-3 justify-content-end">
                <a href="{{ route('pecosas.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <!--<div class="table-responsive">-->
              <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead class="thead-inverse">
                  <tr>
                    <th>#</th>
                    <th>Nro de meta</th>                    
                    <th>Codigo de pecosa</th>
                    <th>Obra</th>
                    <th>Fecha de Salida</th>                    
                    <th>Ver</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($pecosas as $item)
                  <tr>
                    <td scope="row">{{ $loop->iteration }}</td>
                    <td>{{ $item->num_meta }}</td>
                    <td>{{ $item->codigo }}</td>
                    <td>{{ $item->descripcion}}</td>
                    <td>{{ $item->fecha_salida }}</td>
                    <td>
                      <a href="{{ route('pecosas.show' , $item->pecosa_id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div>
                {{ $pecosas->links('vendor.pagination.bootstrap-4') }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    var table = $('#dataTable').DataTable({
      dom: 'Bfrtip',
    buttons: [
        'pdf', 'excel', 'print'
    ],
      "scrollX": true,
    });
  } );
</script>
@endsection
