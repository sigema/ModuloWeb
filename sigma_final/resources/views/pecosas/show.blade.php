@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="card">
    <div class="card-header">
        Ver Pecosa
    </div>
    <div id="ordenCompraForm" class="card-block">
      <div class="row" >
        <div class="col-12 col-md-8 offset-md-2">
        <fieldset disabled>
          <div class="form-group row">
            {!! Form::label('codigo' , 'Código de Pecosa:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('codigo' , $pecosa->codigo , ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('fecha' , 'Fecha Salida:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('codigo' , $pecosa->fecha_salida , ['class' => 'form-control']) !!}
            </div>
          </div>          
          <!-- <div class="form-group row">
            {!! Form::label('oc' , 'Orden de compra :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              <select name="oc" class="js-select2-remote" style="width: 100%; height: 100%;">
                <option value="0">Orden de compra</option>
              </select>
            </div>
          </div>  -->
          <div class="form-group row">
            {!! Form::label('Obra' , 'Obra:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('Obra' , $obra->descripcion , ['class' => 'form-control']) !!}
            </div>
          </div>  
        </fieldset>
        </div>
        
      </div>
      <div class="row">
        <div class="col-12 col-md-10 offset-md-1">
            <div class="card">
              <div class="card-header" role="tab" id="headingTwo">
                <h5>
                    Detalle de Orden de Compra                  
                </h5>
              </div>
              <div>
                <div class="card-block">
                  <div class="container">
                    <div class="card-block">
                    <div class="row">
                        <table class="table" id="js-detalles_oc">
                          <thead class="thead-inverse">
                            <tr>                              
                              <th>Código</th>
                              <th>Descripción</th>
                              <th>Unidad</th>
                              <th>Cantidad</th>
                              <th>Precio Unitario</th>
                              <th>Importe</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($d_pecosa as $item)
                          <tr>
                            <!-- <td scope="row">{{ $loop->iteration }}</td> -->
                            <td>{{ $item->codigo }}</td>
                            <td>{{ $item->descripcion }}</td>
                            <td>{{ $item->unidad }}</td>
                            <td>{{ $item->cantidad }}</td>
                            <td>{{ $item->precio_unitario }}</td>
                            <td>{{ $item->importe }}</td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <br>
  
        <div class="row">
          <div class="col-8 offset-md-2">
            <fieldset disabled>  
            <!-- <div class="form-group row">

              {!! Form::label('Total' , 'Total:' , ['class' => 'col-form-label col-sm-3']) !!}
              <div class="col-sm-9">
                {!! Form::text('total' , null , ['class' => 'form-control js-total']) !!}
              </div>
            </div> -->
            </fieldset>
          </div>  
        </div>
      </div>
      {!! Form::close() !!}
            
    </div>    
  </div>
</div>

@endsection
@section('script')
<script src="{{ asset('js/pecosa.js') }}"></script>
@endsection