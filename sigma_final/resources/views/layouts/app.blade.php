<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'SIGMA') }}</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Scripts -->
        @yield('style')
        <script>
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!};
        </script>
    </head>
    <body>
        <div id="app">
            @if (!Auth::guest())
            <nav class="navbar navbar-toggleable-md navbar-inverse bg-primary">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/ubicacion') }}">SIGMA</a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        @if (!Auth::guest())
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('ubicacion.index') }}">Ubicación</a>
                        </li>
                        @if(Auth::user()->tipo == 1 || Auth::user()->tipo == 3)
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('maquinarias.index') }}">Maquinarias</a>
                        </li>
                        @endif
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('stockalmacen.index') }}">Stock de Almacen</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('ordencompra.index') }}">Orden de Compras</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('pecosas.index') }}">Pecosas</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('valeconsumo.index') }}">Vales de Consumo</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('obras.index') }}">Obras</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('proveedores.index') }}">Proveedores</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('consumibles.index') }}">Consumibles</a>
                        </li>
                        @endif
                    </ul>
                    <ul class="navbar-nav my-2 my-lg-0">
                        @if (Auth::guest())
                        <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                        @else
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('admin.index') }}">
                                    Admin
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>
            @endif
            @yield('content')
        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/ubicacion.js') }}"></script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGmfkx8itWNZGISgI9zdrH-9M8NGrTjQg&callback=initMap">
        </script>
        @yield('script')
    </body>
</html>
