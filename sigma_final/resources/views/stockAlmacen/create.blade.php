@extends('layouts.app') 
@section('content') 
<div class="container"> 
  <div class="row  justify-content-md-center" > 
    <div class="col-md-7"> 
      @if (count($errors) > 0) 
      <div class="alert alert-danger"> 
        <ul> 
          @foreach ($errors->all() as $error) 
          <li>{{ $error }}</li> 
          @endforeach 
        </ul> 
      </div> 
      @endif 
    </div> 
  </div> 
  <div class="row  justify-content-md-center" > 
    <div class="col-md-7"> 
      {!! Form::open([ 
      'route' => ['obras.store'],
      'method' => 'POST' 
      ]) !!} 
 
      <div class="form-group row">
        {!! Form::label('num_meta' , 'Nro de Meta :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9"> 
          {!! Form::text('num_meta' , null , ['class' => 'form-control']) !!}
        </div> 
      </div> 
 
      <div class="form-group row">
        {!! Form::label('descripcion' , 'Descripcion :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('descripcion' , null , ['class' => 'form-control']) !!}
        </div>
      </div>
 
      <div class="form-group">
        {!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!} 
      </div> 
      {!! Form::close() !!} 
    </div> 
  </div> 
</div> 
@endsection
