@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Stock en Almacen
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-md-12">
              <!--<div class="table-responsive">-->
              
              <table id="obras" class="display" cellspacing="0" width="100%">
                <thead class="thead-inverse">
                  <tr>
                    <th>#</th>
                    <th>Codigo</th>
                    <th>Descripcion</th>
                    <th>Unidad</th>
                    <th>Marca</th>
                    <th>Precio Ref.</th>
                    <th>Stock (Cantidad)</th>
                  </tr>
                </thead>
                
                <tbody>
                  @foreach($stockAlmacen as $item)
                  <tr>
                    <td scope="row">{{ $loop->iteration }}</td>
                    <td>{{ $item->codigo }}</td>
                    <td>{{ $item->descripcion }}</td>
                    <td>{{ $item->unidad }}</td>
                    <td>{{ $item->marca }}</td>
                    <td>{{ $item->precio_ref }}</td>
                    <td>{{ $item->cantidad }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              
              <!--</div>-->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    var table = $('#obras').DataTable({
      dom: 'Bfrtip',
    buttons: [
        'pdf'
    ],
      "scrollX": true,
    });
  } );
</script>
@endsection
