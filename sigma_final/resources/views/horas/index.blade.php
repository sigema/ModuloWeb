@extends('layouts.app')
@section('content')
<div class="container">
	
	<div class="row ">
		<div class="col">
			<div class="d-flex mb-3 justify-content-end">
				<a href="{{ route('horas.create' ) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
			</div>
			
		</div>
	</div>
	<div class="table-responsive">
		<table class="table">
			<thead class="thead-inverse">
				<tr>
					<th>hora</th>
					<th colspan="2">Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($horas as $hora)
				<tr>
					<td>{{ $hora->cantidad }}</td>
					
					<td>
						<a href="{{ route('horas.edit' , ['hora' => $hora->hora_id]) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
					</td>
					<td>
						<a>
							{!! Form::open([
							'method' => 'DELETE',
							'route' =>  [ 'horas.destroy' , $hora->hora_id] ]) !!}
							{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
							{!! Form::close() !!}
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection