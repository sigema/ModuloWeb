@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row  justify-content-md-center" >
            <div class="col-md-10">
                {!! Form::open([
                    'route' => ['admin.update', $usuario->id],
                    'method' => 'PUT',
                ]) !!}
                <div class="form-group row">
                    {!! Form::label('usuario_name' , 'Nombre :' , ['class' => 'col-form-label col-sm-3']) !!}
                    <div class="col-sm-9">
                        {!! Form::text('usuario_name' , $usuario->name, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('usuario_email' , 'Correo Electronico ;' , ['class' => 'col-form-label col-sm-3']) !!}
                    <div class="col-sm-9">
                        {!! Form::email('usuario_email' , $usuario->email , ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    {!! Form::label('usuario_password' , 'Nueva Contraseña :' , ['class' => 'col-form-label col-sm-3']) !!}

                    {!! Form::password('usuario_password' , ['placeholder'=>"Introduzca una contraseña" ] , ['class' => 'form-control']) !!}
                    <div class="hide-show">
                        <span class="input-group-addon">Mostrar</span>
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('usuario_tipo' , 'Nivel de Usuario :' , ['class' => 'col-form-label col-sm-3']) !!}
                    <div class="col-sm-9">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                {!! Form::radio('usuario_tipo' , 1 , ($usuario->tipo == 1) ? true : false , [ 'class' => 'form-check-input'])!!}
                                Ingenieria
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                {!! Form::radio('usuario_tipo' , 2 ,  ($usuario->tipo == 2) ? true : false , [ 'class' => 'form-check-input']) !!}
                                Álmacen
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                {!! Form::radio('usuario_tipo' , 3 ,  ($usuario->tipo == 3) ? true : false , [ 'class' => 'form-check-input']) !!}
                                Administración
                            </label>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>



@endsection

@section('script')
    <script>
        $(function(){
            $('.hide-show').show();
            $('.hide-show span').addClass('show')

            $('.hide-show span').click(function(){
                if( $(this).hasClass('show') ) {
                    $(this).text('Ocultar');
                    $('input[name="usuario_password"]').attr('type','text');
                    $(this).removeClass('show');
                } else {
                    $(this).text('Mostrar');
                    $('input[name="usuario_password"]').attr('type','password');
                    $(this).addClass('show');
                }
            });

            $('form button[type="submit"]').on('click', function(){
                $('.hide-show span').text('Show').addClass('show');
                $('.hide-show').parent().find('input[name="usuario_password"]').attr('type','password');
            });
        });
    </script>
@endsection