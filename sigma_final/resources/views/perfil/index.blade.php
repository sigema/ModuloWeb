@extends('layouts.app')

@section('content')

<div class="container">

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-controls="home">Bitácora de Acciones</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile">Usuarios</a>
        </li>
        <!--li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-controls="messages">Base de Datos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-controls="settings">Configuración</a>
        </li-->
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="d-flex mb-3 justify-content-end">
                <a type="button" class="btn btn-outline-warning" href="/admin">Actualizar bitácora</a>
            </div>
            <div class="row">
                <div class="col-md-12">
                <table id="bitacoras" class="display"  cellspacing="10" width="100%">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Acción</th>
                        <th>Tabla</th>
                        <th>Maquinaria</th>
                        <th>Usuario</th>
                        <th>Hora y fecha</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($bitacoras as $bitacora)

                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{$bitacora->accion}}</td>
                        <td>{{$bitacora->tabla}}</td>
                        <td>{{$bitacora->nro_registro}}</td>
                        <td>{{$bitacora->name}}</td>
                        <td>{{$bitacora->created_at}}</td>

                    </tr>
                    @endforeach
                    <!--tr>
                        <th scope="row">1</th>
                        <td>Insertar Dato</td>
                        <td>Mantenimiento Preventivo</td>
                        <td>SCU-1234</td>
                        <td>Benjamin Pareja</td>
                        <td>12-02-2017 07:12</td>

                    </tr-->

                    </tbody>
                </table>
            </div>

            </div>

        </div>

        <div class="tab-pane" id="profile" role="tabpanel">
            <div class="d-flex mb-3 justify-content-end">
                <a class="btn btn-outline-primary" href="{{ url('admin/'.'create') }}">Ingresar nuevo Usuario</a>
            </div>
                <div class="row">
                        <div class="col-md-12">
                            <table id="usuarios" class="display"  cellspacing="10" width="100%">
                                <thead class="thead-inverse">
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Tipo de Usuario</th>
                    <th>Fecha de Creación</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($usuarios as $usuario)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{$usuario->name}}</td>
                    <td>{{$usuario->email}}</td>
                    @if ( $usuario->tipo == 1)
                        <td>Ingenieria</td>
                    @elseif ( $usuario->tipo == 2)
                        <td>Álmacen</td>
                    @elseif ( $usuario->tipo == 3)
                        <td>Administración</td>
                    @endif
                    <td>{{$usuario->created_at}}</td>
                    <td><a href="{{ url('/admin/'. $usuario->id.'/edit') }}" class="btn btn-success">Editar</a>
                    {!! Form::open([
                                'method' => 'DELETE',
                                'route' => ['admin.destroy', $usuario->id]
                                ]) !!}
                    {!! Form::submit('Borrar', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>

        </div>

        </div>
        <div class="tab-pane" id="messages" role="tabpanel">

            <div class="row">
<div class="container">
    <p class="lead">
        Elige un lugar para guardar el archivo de copia de seguridad
    </p>
</div>
              <div>

            <label class="custom-file">
                <input type="file" id="file" class="custom-file-input">
                <span class="custom-file-control"></span>
            </label>


                  <p class="lead">
                      Ingrese el Nombre del Archivo
                  </p>

                  <input type="text" class="form-control" id="extampleField" placeholder="db170802.sql">

                  <div class="form-check">
                      <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="exampleRadios"
                                 id="exampleRadios1" value="option1" checked>
                          Copia de seguridad completa
                      </label>
                  </div>
                  <div class="form-check">
                      <label class="form-check-label">
                          <input class="form-check-input" type="radio" name="exampleRadios"
                                 id="exampleRadios3" value="option2">
                          Copia de seguridad (Solo la base de datos)
                      </label>
                  </div>

                  <button type="button" class="btn btn-outline-primary">Guardar</button>
              </div>
        </div>
    </div>

        <div class="tab-pane" id="settings" role="tabpanel">




        </div>
    </div>
</div>


@endsection
@section('script')
    <script>
        $(function () {
            $('#myTab a:last').tab('show')
        })
    </script>

    <script>

        $(document).ready(function() {
            var table = $('#usuarios').DataTable({
                dom: 'Bfrtip',
                select: true,
                "scrollX": true,
                lengthChange: false,
                buttons: [  {
                    extend: 'pdf',
                    text: 'Exportar PDF',
                    message:'Lista de los usuarios registrados en el sistema',
                    header:true,
                    footer:true,
                    filename:'Usuarios',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },{
                    extend: 'excel',
                    text: 'Exportar Excel',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    }
                },{
                    extend: 'print',
                    text: 'Imprimir',
                    autoPrint: true
                } ]
            } );

            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        } );
        $(document).ready(function() {
            var table = $('#bitacoras').DataTable({
                dom: 'Bfrtip',
                select: true,
                lengthChange: false,
                buttons: [ 'pdf', 'excel', 'print' ]
            } );

            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        } );

    </script>
@endsection