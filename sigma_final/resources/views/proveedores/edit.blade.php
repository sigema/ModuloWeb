@extends('layouts.app') 
@section('content') 
<div class="container"> 
  <div class="row  justify-content-md-center" > 
    <div class="col-md-7"> 
      @if (count($errors) > 0) 
      <div class="alert alert-danger"> 
        <ul> 
          @foreach ($errors->all() as $error) 
          <li>{{ $error }}</li> 
          @endforeach 
        </ul> 
      </div> 
      @endif 
    </div> 
  </div> 
  <div class="row  justify-content-md-center" > 
    <div class="col-md-7"> 
      {!! Form::open([ 
        'url' => '/proveedores/' . $prov->proveedor_id ,
        'method' => 'PUT' 
      ]) !!}


      <div class="form-group row">
        {!! Form::label('nombre' , 'Nombre de la Empresa :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('nombre' , $prov->nombre , ['class' => 'form-control']) !!}
        </div>
      </div>

      <div class="form-group row">
        {!! Form::label('ruc' , 'RUC :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('ruc' , $prov->ruc , ['class' => 'form-control']) !!}
        </div>
      </div>

      <div class="form-group row">
        {!! Form::label('telefono' , 'Teléfono :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('telefono' , $prov->telefono , ['class' => 'form-control']) !!}
        </div>
      </div>

      <div class="form-group row">
        {!! Form::label('direccion' , 'Dirección :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('direccion' , $prov->direccion , ['class' => 'form-control']) !!}
        </div>
      </div>


      <div class="form-group"> 
        {!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!} 
      </div> 
      {!! Form::close() !!} 
    </div> 
  </div> 
</div> 
@endsection
