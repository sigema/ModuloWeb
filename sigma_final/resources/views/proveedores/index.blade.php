@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Obras
				</div>
				<div class="card-block">
					<div class="row ">
						<div class="col">
							<div class="d-flex mb-3 justify-content-end">
								<a href="{{ route('proveedores.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i>Agregar</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<!--<div class="table-responsive">-->
							
							<table id="obras" class="display" cellspacing="0" width="100%">
								<thead class="thead-inverse">
									<tr>
										<th>#</th>
										<th>Nombre</th>
										<th>Ruc</th>
										<th>Teléfono</th>
										<th>Dirección</th>
										<th>Acciones</th>
									</tr>
								</thead>
								
								<tbody>
									@foreach($provs as $prov)
									<tr>
										<td scope="row">{{ $loop->iteration }}</td>
										<td>{{ $prov->nombre }}</td>
										<td>{{ $prov->ruc }}</td>
										<td>{{ $prov->telefono }}</td>
										<td>{{ $prov->direccion }}</td>
										<td><a href="{{ route('proveedores.edit' , $prov->proveedor_id) }}" class="btn btn-success">Editar</a>
										{!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['proveedores.destroy', $prov->proveedor_id]
                                                    ]) !!}
										{!! Form::submit('Borrar', ['class' => 'btn btn-danger']) !!}
										{!! Form::close() !!}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							
							<!--</div>-->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$(document).ready(function() {
		var table = $('#obras').DataTable({
			dom: 'Bfrtip',
	        buttons: [
	            'pdf'
	        ],
			"scrollX": true,
		});
	} );
</script>
@endsection