@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Vales de Consumo
        </div>
        <div class="card-block">
          <div class="row ">
            <div class="col">
              <div class="d-flex mb-3 justify-content-end">
                <a href="{{ route('valeconsumo.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <!--<div class="table-responsive">-->
              <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead class="thead-inverse">
                  <tr>
                    <th>#</th>
                    <th>Obra</th>
                    <th>Fecha de Salida</th>
                    <th>Firma</th>
                    <th>Ver</th>
                    <th>Editar</th>
                    <!-- <th>Eliminar</th> -->
                  </tr>
                </thead>
                <tbody>
                  @foreach($items as $item)
                  <tr>
                    <td scope="row">{{ $loop->iteration }}</td>
                    <td>{{ $item->descripcion }}</td>
                    <td>{{ $item->fecha_salida }}</td>
                    <td>{{ $item->firma }}</td>
                    <td>
                      <a href="{{ route('valeconsumo.show' , $item->vale_consumo_id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    </td>
                    <td>
                      <a href="{{ route('valeconsumo.edit' , $item->vale_consumo_id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                    </td>
                    <!-- <td>
                      <a>
                        {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['ordencompra.destroy', $item->vale_consumo_id]
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                      </a>
                    </td> -->
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    var table = $('#dataTable').DataTable({
      dom: 'Bfrtip',
    buttons: [
        'pdf', 'excel', 'print'
    ],
      "scrollX": true,
    });
  } );
</script>
@endsection
