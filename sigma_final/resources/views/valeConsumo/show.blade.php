@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="card">
    <div class="card-header">
        Ver Vale de Consumo
    </div>
    <div id="valeConsumoForm" class="card-block">
      {!! Form::open([
          ]) !!}
      <div class="row" >
        <div class="col-12 col-md-8 offset-md-2">
        <fieldset disabled>
          <div class="form-group row">
            {!! Form::label('Obra' , 'Obra:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('Obra' , $obra->descripcion , ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('fecha' , 'Fecha de Salida:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('fecha' , $valeConsumo->fecha_salida, ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('firma' , 'Firma:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('firma' , $valeConsumo->firma , ['class' => 'form-control']) !!}
            </div>
          </div>
        </div>
        </fieldset>
      </div>
      <div class="row">
        <div class="col-12 col-md-10 offset-md-1">

            <div class="card">
              <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">

                    Detalle de Vale de Consumo

                </h5>
              </div>
              <div id="collapseTwo">
                <div class="card-block">
                  <div class="container">
                    <div class="card-block">
                    <div class="row">
                        <table class="table" id="js-detalles_vc">
                          <thead class="thead-inverse">
                            <tr>
                              <th>#</th>
                              <th>Código</th>
                              <th>Descripción</th>
                              <th>Marca</th>
                              <th>Unidad</th>
                              <th>Cantidad</th>


                            </tr>
                          </thead>
                          <tbody>
                          @foreach($d_vc as $item)
                          <tr>
		                    <td scope="row">{{ $loop->iteration }}</td>
		                    <td>{{ $item->codigo }}</td>
		                    <td>{{ $item->descripcion }}</td>
		                    <td>{{ $item->marca }}</td>
		                    <td>{{ $item->unidad }}</td>
		                    <td>{{ $item->cantidad }}</td>


		                  </tr>
		                  @endforeach
                          </tbody>
                        </table>
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>

        </div>

      </div>
      {!! Form::close() !!}

    </div>
  </div>
</div>

@endsection
@section('script')
<script src="{{ asset('js/vale_consumo.js') }}"></script>
<script>

set_oc_id( '{{$valeConsumo->vale_consumo_id}}' );
set_obra( '{{$obra->obra_id}}',
    '{{$obra->num_meta}}',
    '{{$obra->descripcion}}');
@foreach($d_vc as $item)
add_detalle_vc(
    '{{$item->consumible_almac_id}}',
    '{{$item->cantidad}}');
@endforeach
</script>



@endsection
