@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="card">
    <div class="card-header">
        Editar Vale de Consumo
    </div>
    <div id="valeConsumoForm" class="card-block">
      {!! Form::open([
          'route' => ['valeconsumo.update' , $valeConsumo->vale_consumo_id],
          'method' => 'POST'
          ]) !!}
      <div class="row" >
        <div class="col-12 col-md-8 offset-md-2">
          <div class="form-group row">
            {!! Form::label('obra' , 'Obra :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              <select name="obra" class="js-select2-remote-obra" style="width: 100%; height: 100%;">
                <option value="0" selected="selected"> Obra </option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('fecha' , 'Fecha de Salida:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('fecha' , $valeConsumo->fecha_salida , ['class' => 'form-control']) !!}
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('firma' , 'Firma ;' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('firma' , $valeConsumo->firma , ['class' => 'form-control']) !!}
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-10 offset-md-1">
            <div class="card">
              <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">
                    Detalles del Vale de Consumo
                </h5>
              </div>

                <div class="card-block">
                  <div class="row js-detalle_vc">
                    <div class="col-12 col-md-4">
                      <select name="consumibleAlmacen" class="js-select2-consumibleAlmacen" style="width: 100%; height: 100%;">
                        <option value="0" selected="selected">Consumible</option>
                      </select>
                    </div>
                    <div class="col-12 col-md-4">
                      <input type="text" name="cantidad" class="form-control" placeholder="cantidad">
                    </div>
                    <div class="col-12 col-md-4">
                      <button id="js-btn-detalle_vc" class="btn btn-block btn-primary">Agregar</button>
                    </div>
                  </div>                  
                  <div class="container">
                    <div class="card-block">
                    <div class="row">
                        <table class="table" id="js-detalles_vc">
                          <thead class="thead-inverse">
                            <tr>
                              <th>#</th>
                              <th>Código</th>
                              <th>Descripción</th>
                              <th>Marca</th>
                              <th>Unidad</th>
                              <th>Cantidad</th>
                              <th>Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($d_vc as $item)
                          <tr>
		                    <td scope="row">{{ $loop->iteration }}</td>
		                    <td>{{ $item->codigo }}</td>
		                    <td>{{ $item->descripcion }}</td>
		                    <td>{{ $item->marca }}</td>
		                    <td>{{ $item->unidad }}</td>
		                    <td>{{ $item->cantidad }}</td>
                                    <td>
                                      <button type="button" class="js-delete btn btn-outline-danger" js-id="{{$loop->iteration}}">x</button>
                                    </td>
		                  </tr>
		                  @endforeach
                          </tbody>
                        </table>
                      </div>
                      </div>
                    </div>
                  </div>
            </div>

        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-8 offset-md-2">
          <div class="form-group float-right" style="margin-right: 15px;">
            {!! Form::submit('Guardar' , ['class' => 'btn btn-success js-btn-guardar-vc']) !!}
          </div>  
        </div>        
      </div>
      {!! Form::close() !!}
            
    </div>    
  </div>
</div>


<!-- MODAL DE NUEVA OBRA -->
<div class="modal" id="nuevaObraModal" tabindex="-1" role="dialog" aria-labelledby="nuevaObraModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva Obra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="form-group row">
                <label for="num_meta" class="col-form-label col-4">Nro de Meta :</label>
                <div class="col-8"><input name="num_meta" type="text" id="num_meta" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="descripcion" class="col-form-label col-4">Descripcion :</label>
                <div class="col-8"><input name="descripcion" type="text" id="descripcion" class="form-control"></div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary js-btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DE NUEVO CONSUMIBLE ALMACEN -->
<div class="modal" id="nuevoConsumibleModal" tabindex="-1" role="dialog" aria-labelledby="nuevaConsumibleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Consumible de Almacen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="form-group row">
                <label for="codigo" class="col-form-label col-4">Codigo :</label>
                <div class="col-8"><input name="codigo" type="text" id="codigo" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="descripcion" class="col-form-label col-4">Descripcion :</label>
                <div class="col-8"><input name="descripcion" type="text" id="descripcion" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="unidad" class="col-form-label col-4">Unidad :</label>
                <div class="col-8"><input name="unidad" type="text" id="unidad" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="marca" class="col-form-label col-4">Marca :</label>
                <div class="col-8"><input name="marca" type="text" id="marca" class="form-control"></div>
              </div>
              <div class="form-group row">
                <label for="precio_ref" class="col-form-label col-4">Precio Referencial :</label>
                <div class="col-8"><input name="precio_ref" type="text" id="precio_ref" class="form-control"></div>
              </div>
              <div class="form-group row">
               <label for="precio_ref" class="col-form-label col-4">Tipo :</label>
                <div class="col-8">
                <select name="tipo" id="tipo" class="form-control">
                  <option value="1">Filtros</option>
                  <option value="2">Lubricantes</option>
                  <option value="3">Elementos de desgaste</option>
                </select>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary js-btn-save">Guardar</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section('script')
<script src="{{ asset('js/vale_consumo.js') }}"></script>
<script>

set_vc_id( '{{$valeConsumo->vale_consumo_id}}' );
set_obra( '{{$obra->obra_id}}',
    '{{$obra->num_meta}}',
    '{{$obra->descripcion}}');

@foreach($d_vc as $item)
add_detalle_vc(
    '{{$item->consumible_almac_id}}',
    '{{$item->cantidad}}');
@endforeach
</script>



@endsection
