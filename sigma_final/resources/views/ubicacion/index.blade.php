@extends('layouts.app')
<style>
#map {
height: 500px;
width: 100%;
margin:
}
.hidden {
display: none;
}
.showed {
display: block;
}
</style>
@section('content')
<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          Ubicación
        </div>
        <div class="card-block">
          <div class="row">
            <div class="col-10">
              <div id="form-search-maquinaria">
                <div class="row">
                  <div class="col-12">
                    <!-- <input class="form-control" type="text" placeholder="Buscar por Num de registro , Serie o Placa"> -->
                    <select name="ubicacion" class="js-select2-remote-ubicacion" style="width: 100%; height: 100%;">
                      <option value="0" selected="selected">Buscador...</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-2">
              <button id="btn-clear" class="btn btn-outline-warning btn-block">Limpiar</button>
            </div>
          </div>
          <br/>
          <div class="row">
            <div class="col-md-8">
              <div id="map">
                MAPA
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div id="js-provincia-title" class="card-header">Haga click en una provincia</div>
                <div class="card-block">
                  <div id="js-provincia-content">
                  </div>
                  <div class="row">
                    <div class="col-12">
                    <br>
                      <button id="detallesProvincia" type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#modalMaquinarias">Ver Detalles</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Row content -->
<div class="container">
  <div id="js-maquinaria-row" class="row hidden js-maquinaria-row">
    <div class="col-12">
      <span></span>
    </div>
  </div>
</div>
<!-- END Row content -->
<!-- Row search content -->
<div class="container">
  <div id="js-maquinaria-data" class="row hidden js-maquinaria-row">
    <div class="col-6"><span></span></div>
    <div class="col-6"><span></span></div>
  </div>
</div>
<!-- END Row search content -->
<!-- Modal -->
<div class="modal fade" id="modalMaquinarias" tabindex="-1" role="dialog"
  aria-labelledby="myModalLabel5" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Maquinarias</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12">
            <table class="table table striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Serie</th>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Tipo</th>
                  <th>Nro Registro</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
@foreach($provincias as $provincia)
addMarker(
{{$provincia->provincia_id}},
"{{$provincia->nombre}}",
{{$provincia->latitud}},
{{$provincia->longitud}} );
@endforeach
</script>
@endsection