@extends('layouts.app')

<style type="text/css">
    html{
        height: 100%;
    }
    body{
        background-image: url('./images/maquinaria-pesada.jpg');
        background-position: center;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        height: 100%;
    }
    #app{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100%;
    }
</style>

@section('content')

<div class="container ">
    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="card" style="margin-top: 25%;">
              <h2 class="card-header">
                <div class="row d-flex align-items-center">
                    <div class="col-4">
                        <img src="{{ URL::to('/') }}/images/logo.png" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h3>Iniciar Sesión</h3>
                    </div>
                </div>
                
              </h2>
              <div class="card-block">
                <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="row form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label for="email" class="col-sm-12 form-control-label">E-Mail</label>

                            <div class="col-sm-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="form-control-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-12 form-control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Ingresar
                                </button>

                                <a class="btn btn-block" href="{{ route('password.request') }}">
                                    ¿Olvidaste tu contraseña?
                                </a>
                            </div>
                        </div>
                    </form>                
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
