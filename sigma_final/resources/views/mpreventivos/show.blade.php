<div class="mt-2">
	<div class="row ">
		<div class="col">
			<div class="d-flex mb-3 justify-content-end">
				<a href="{{ route('maquinarias.mpreventivos.create' , $maquinaria->maquinaria_id) }}" class="btn btn-primary {{ ($receta != null ? '' : 'disabled')}}"><i class="fa fa-plus"></i> Agregar</a>
			</div>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table">
			<thead class="thead-inverse">
				<tr>
					<th>MP</th>
					<th>Obra | Institución</th>
					<th>Mecanico</th>
					<th>Nro de Reporte</th>
					<th>Fecha de Ejecución</th>
					<th>Horometro Aproximado</th>
					<th>Fecha Aproximada</th>
					<th>Realizado</th>
					<th>Pase</th>
					<th colspan="2">Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($mps as $mp)
				<tr>
					<td>{{ $mp->mp }}</td>
					<td>{{ $mp->obra_institucion }}</td>
					<td>{{ $mp->mecanico }}</td>
					<td>{{ $mp->nro_reporte }}</td>
					<td>{{ $mp->fecha_ejecución }}</td>
					<td>{{ $mp->horometro_aprox }}</td>
					<td>{{ $mp->fecha_aprox }}</td>
					<td>
						<label class="form-check-label">
							@if( !isset($mp->realizado) )
							<input class="form-check-input"  type="checkbox" value="1" disabled>
							@else
							<input class="form-check-input"  checked="checked" type="checkbox" value="1" disabled>
							@endif
						</div>
					</td>
					<td>
						<label class="form-check-label">
							@if( !isset($mp->pase) )
							<input class="form-check-input"  type="checkbox" value="1" disabled>
							@else
							<input class="form-check-input"  checked="checked" type="checkbox" value="1" disabled>
							@endif
						</div>
					</td>
					<td>
						<a href="{{ route('maquinarias.mpreventivos.edit' , ['maquinaria' => $mp->maquinaria_id ,
						'mpreventivo' => $mp->mpreventivo_id]) }}" class="btn btn-success  {{ ($mp->status) ? 'disabled' : ''}}" role="button"><i class="fa fa-pencil"></i></a>
					</td>
					<td>
						@if(!$mp->status)
							{!! Form::open([
							'method' => 'DELETE',
							'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/mpreventivos/' . $mp->mpreventivo_id ]) !!}
							{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
							{!! Form::close() !!}
						@else
						<a href="" class="btn btn-danger disabled" role="button"><i class="fa fa-trash"></i></a>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
