@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row  justify-content-md-center" >
		<div class="col-md-7">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if (session('flash_message'))
			<div class="alert alert-danger">
				<ul>
					<li>{{ session('flash_message') }}</li>
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="row  justify-content-md-center" >
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<di class="d-flex justify-content-between align-items-center">
					
					<div>
						<h2>{{ $maquinaria->tipo . ' ' . $maquinaria->modelo . ' ' . $maquinaria->nro_registro }}</h2>
					</div>
					<div>
						<h4><strong>Horometro: </strong>{{ $maquinaria->horometro }}</h4>
					</div>
				</div>
				<div class="card-block">
					{!! Form::open([
					'route' => ['maquinarias.mpreventivos.store' , $maquinaria->maquinaria_id],
					'method' => 'POST'
					]) !!}
					<div class="form-group row">
						{!! Form::label('mp' , 'MP :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::number('mp' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('obra_institucion' , 'Obra | Institución ;' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('obra_institucion' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('nro_reporte' , 'Nro de reporte :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('nro_reporte' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('mecanico' , 'Mecánico :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('mecanico' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('fecha_ejecución' , 'Fecha de ejecución :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::date('fecha_ejecución' , \Carbon\Carbon::now() , ['class' => 'form-control']) !!}
						</div>
					</div>
					<!--<div class="form-group row">
								{!! Form::label('horometro_aprox' , 'Horometro aproximado :' , ['class' => 'col-form-label col-sm-3']) !!}
								<div class="col-sm-9">
											{!! Form::number('horometro_aprox' , null , ['class' => 'form-control']) !!}
								</div>
					</div>
					<div class="form-group row">
								{!! Form::label('fecha_aprox' , 'Fecha aproxiamada :' , ['class' => 'col-form-label col-sm-3']) !!}
								<div class="col-sm-9">
											{!! Form::date('fecha_aprox' , null , ['class' => 'form-control']) !!}
								</div>
					</div>-->
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Realizado</label>
						<div class="col-sm-9">
							<div class="form-check form-check-inline">
								<label class="form-check-label">
									{!! Form::checkbox('realizado' , 1 , false , [ 'class' => 'form-check-input' , 'id' =>'realizado' ]) !!}
								</label>
								<div id="show-realizado" class="alert alert-warning" style="display: none">
									Ya no podras editar o eliminar este registro
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Pase</label>
						<div class="col-sm-9">
							<div class="form-check form-check-inline">
								<label class="form-check-label">
									{!! Form::checkbox('pase' , 1 , false , [ 'class' => 'form-check-input']) !!}
								</label>
							</div>
						</div>
					</div>
					
					
					<div class="form-group">
						{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$('#realizado').change(function(){
		if($(this).is(':checked')){
			$('#show-realizado').show();
		}else{
			$('#show-realizado').hide();
		}
	});
</script>
@endsection