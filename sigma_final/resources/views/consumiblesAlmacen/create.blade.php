@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      {!! Form::open([
      'route' => ['consumiblesAlmacen.store'],
      'method' => 'POST'
      ]) !!}
      
      <div class="form-group row">
        {!! Form::label('codigo' , 'Código:' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('codigo' , null , ['class' => 'form-control']) !!}
        </div>
      </div>
      
      <div class="form-group row">
        {!! Form::label('descripcion' , 'Descripcion :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('descripcion' , null , ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('unidad' , 'Unidad :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('unidad' , null , ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('marca' , 'Marca :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('marca' , null , ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('precio_ref' , 'Precio Referencial :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('precio_ref' , null , ['class' => 'form-control']) !!}
        </div>
      </div>
      
      <div class="form-group row">
        {!! Form::label('tipo' , 'Tipo :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          <select name="tipo" id="tipo" class="form-control">
            <option value="1">Filtro</option>
            <option value="2">Lubricante</option>
            <option value="1">Elememnto de desgaste</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        {!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection