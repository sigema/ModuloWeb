@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Consumibles
				</div>
				<div class="card-block">
					<div class="row ">
						<div class="col">
							<div class="d-flex mb-3 justify-content-end">
								<a href="{{ route('consumibles.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<!--<div class="table-responsive">-->
							
							<table id="consumibles" class="display" cellspacing="0" width="100%">
								<thead class="thead-inverse">
									<tr>
										<th>#</th>
										<th>Código</th>
										<th>Descripcion</th>
										<th>Unidad</th>
										<th>Precio Referencial</th>
										<th>Marca</th>
										<th>Tipo</th>
										<th></th>
									</tr>
								</thead>
								
								<tbody>
									@foreach($consumiblesAlmacen as $c)
									<tr>
										<td scope="row">{{ $loop->iteration }}</td>
										<td>{{ $c->codigo }}</td>
										<td>{{ $c->descripcion }}</td>
										<td>{{ $c->unidad }}</td>
										<td>{{ $c->precio_ref }}</td>
										<td>{{ $c->marca }}</td>
										<td>
											@if($c->tipo == 1)
											Filtro
											@elseif($c->tipo == 2)
											Lubricante
											@elseif($c->tipo == 3)
											Elemento de desgaste
											@endif
										</td>

										<td><a href="{{ route('consumibles.edit' , $c->consumible_almac_id) }}" class="btn btn-success">Editar</a>
										</td>
						
									</tr>
									@endforeach
								</tbody>
							</table>
							
							<!--</div>-->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$(document).ready(function() {
		var table = $('#consumibles').DataTable({
			dom: 'Bfrtip',
	        buttons: [
	            'pdf'
	        ],
			"scrollX": true,
		});
	} );
</script>
@endsection