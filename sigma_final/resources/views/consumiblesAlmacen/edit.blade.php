@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      {!! Form::open([
      'route' => ['consumiblesAlmacen.update' , $consumiblesAlmacen->consumible_almac_id ],
      'method' => 'PUT'
      ]) !!}
      
      <div class="form-group row">
        {!! Form::label('codigo' , 'Código:' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('codigo' , $consumiblesAlmacen->codigo , ['class' => 'form-control']) !!}
        </div>
      </div>
      
      <div class="form-group row">
        {!! Form::label('descripcion' , 'Descripcion :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('descripcion' , $consumiblesAlmacen->descripcion , ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('unidad' , 'Unidad :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('unidad' , $consumiblesAlmacen->unidad , ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('marca' , 'Marca :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('marca' , $consumiblesAlmacen->marca , ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('precio_ref' , 'Precio Referencial :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          {!! Form::text('precio_ref' , $consumiblesAlmacen->precio_ref , ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="form-group row">
        {!! Form::label('tipo' , 'Tipo :' , ['class' => 'col-form-label col-sm-3']) !!}
        <div class="col-sm-9">
          <select name="tipo" id="tipo" class="form-control">

            <option value="1" {{ ($consumiblesAlmacen->tipo == 1 ) ? 'selected' : '' }}>Filtro</option>
            <option value="2" {{ ($consumiblesAlmacen->tipo == 2 ) ? 'selected' : '' }}>Lubricante</option>
            <option value="3" {{ ($consumiblesAlmacen->tipo == 3 ) ? 'selected' : '' }} >Elememnto de desgaste</option>
          </select>
        </div>
      </div>
      
      <div class="form-group">
        {!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection