@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row  justify-content-md-center" >
		<div class="col-md-7">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="row  justify-content-md-center" >
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="d-flex justify-content-between align-items-center">
						
						<div>
							<h2>{{ $maquinaria->tipo . ' ' . $maquinaria->modelo . ' ' . $maquinaria->nro_registro }}</h2>
						</div>
						<div>
							<h4><strong>Horometro: </strong>{{ $maquinaria->horometro }}</h4>
						</div>
					</div>
				</div>
				<div class="card-block">
					{!! Form::open([
					'route' => ['maquinarias.mcorrectivos.store' , $maquinaria->maquinaria_id],
					'method' => 'POST'
					]) !!}
					<div class="form-group row">
						{!! Form::label('obra_institucion' , 'Obra | Institución ;' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('obra_institucion' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('fecha' , 'Fecha :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::date('fecha' , \Carbon\Carbon::now() , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('mecanico' , 'Mecánico :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('mecanico' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('reporte_falla' , 'Reporte de falla :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('reporte_falla' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('nro_documento' , 'Nro de documento :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('nro_documento' , null , ['class' => 'form-control']) !!}
						</div>
					</div>
					
					<div class="form-group">
						{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection