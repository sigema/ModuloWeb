<div class="mt-2">
	<div class="row ">
		<div class="col">
			<div class="d-flex mb-3 justify-content-end">
				<a href="{{ route('maquinarias.mcorrectivos.create' , $maquinaria->maquinaria_id) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
			</div>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table">
			<thead class="thead-inverse">
				<tr>
					<th>Obra | Institución</th>
					<th>Fecha</th>
					<th>Mecanico</th>
					<th>Reporte de Falla</th>
					<th>Nro de documento</th>
					<th colspan="2">Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($mcs as $mc)
				<tr>
					<td>{{ $mc->obra_institucion }}</td>
					<td>{{ $mc->fecha }}</td>
					<td>{{ $mc->mecanico }}</td>
					<td>{{ $mc->reporte_falla }}</td>
					<td>{{ $mc->nro_documento }}</td>
					<td>
						<a href="{{ route('maquinarias.mcorrectivos.edit' , ['maquinaria' => $mc->maquinaria_id ,
						'mcorrectivo' => $mc->mcorrectivo_id]) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
					</td>
					<td>
						<a>
							{!! Form::open([
							'method' => 'DELETE',
							'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/mcorrectivos/' . $mc->mcorrectivo_id ]) !!}
							{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
							{!! Form::close() !!}
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>