@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row  justify-content-md-center" >
		<div class="col-md-7">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="row  justify-content-md-center" >
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="d-flex justify-content-between align-items-center">
						
						<div>
							<h2>{{ $maquinaria->tipo . ' ' . $maquinaria->modelo . ' ' . $maquinaria->nro_registro }}</h2>
						</div>
						<div>
							<h4><strong>Horometro: </strong>{{ $maquinaria->horometro }}</h4>
						</div>
					</div>
				</div>
				<div class="card-block">
					{!! Form::open([
					'url' => '/maquinarias/' . $mc->maquinaria_id . '/mcorrectivos/' . $mc->mcorrectivo_id,
					'method' => 'PUT'
					]) !!}
					<div class="form-group row">
						{!! Form::label('obra_institucion' , 'Obra | Institución ;' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('obra_institucion' , $mc->obra_institucion , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('fecha' , 'Fecha :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::date('fecha' , $mc->fecha , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('mecanico' , 'Mecánico :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('mecanico' , $mc->mecanico , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('reporte_falla' , 'Reporte de falla :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('reporte_falla' , $mc->reporte_falla , ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group row">
						{!! Form::label('nro_documento' , 'Nro de documento :' , ['class' => 'col-form-label col-sm-3']) !!}
						<div class="col-sm-9">
							{!! Form::text('nro_documento' , $mc->nro_documento , ['class' => 'form-control']) !!}
						</div>
					</div>
					
					<div class="form-group">
						{!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection