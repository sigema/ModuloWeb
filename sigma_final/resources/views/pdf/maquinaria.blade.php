@extends('layouts.app')
@section('style')
<style>
	.parpadea {
	animation-name: parpadeo;
	animation-duration: 1s;
	animation-timing-function: linear;
	animation-iteration-count: infinite;
	-webkit-animation-name:parpadeo;
	-webkit-animation-duration: 1s;
	-webkit-animation-timing-function: linear;
	-webkit-animation-iteration-count: infinite;
	}
	@-moz-keyframes parpadeo{
	0% { opacity: 1.0; }
	50% { opacity: 0.0; }
	100% { opacity: 1.0; }
	}
	@-webkit-keyframes parpadeo {
	0% { opacity: 1.0; }
	50% { opacity: 0.0; }
	100% { opacity: 1.0; }
	}
	@keyframes parpadeo {
	0% { opacity: 1.0; }
	50% { opacity: 0.0; }
	100% { opacity: 1.0; }
	}
</style>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<di class="d-flex justify-content-between align-items-center">
					<div>

					</div>
					<div>
						<h2>{{ $maquinaria->tipo . ' ' . $maquinaria->modelo . ' ' . $maquinaria->nro_registro }}</h2>
					</div>
					<div>
						<h4><strong>Horometro: </strong>{{ $maquinaria->horometro }}</h4>

				</div>
				<div class="card-block">
						<!-- Creando una vista de maquinaria-->


								<dt class="col-sm-3">Marca :</dt>
								<dd class="col-sm-9">{{ $maquinaria->marca }}</dd>
								<dt class="col-sm-3">Modelo :</dt>
								<dd class="col-sm-9">{{ $maquinaria->modelo }}</dd>
								<dt class="col-sm-3">Tipo :</dt>
								<dd class="col-sm-9">{{ $maquinaria->tipo }}</dd>
								<dt class="col-sm-3">Nro de registro :</dt>
								<dd class="col-sm-9">{{ $maquinaria->nro_registro }}</dd>
								<dt class="col-sm-3">Serie :</dt>
								<dd class="col-sm-9">{{ $maquinaria->serie }}</dd>
								<dt class="col-sm-3">Año :</dt>
								<dd class="col-sm-9">{{ $maquinaria->anio }}</dd>
								
								<dt class="col-sm-3">Estado :</dt>
								<dd class="col-sm-9">
								@if ( $maquinaria->estado == 1)
								Operativo
								@elseif ( $maquinaria->estado == 2)
								------
								@else
								Inoperativo
								@endif</dd>
								<dt class="col-sm-3">Horometro :</dt>
								<dd class="col-sm-9">{{ $maquinaria->horometro }}</dd>
								<dt class="col-sm-3">Condicion :</dt>
								<dd class="col-sm-9">{{ ($maquinaria->condicion == 1) ? 'Activo' : 'Inactivo' }}</dd>



				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane in active" id="mpreventivos">

						<div class="mt-2">
							<h3>Mantenimiento Correctivo</h3>
							<div class="table-responsive">
								<table class="table">
									<thead class="thead-inverse">
									<tr>
										<th>MP</th>
										<th>Obra | Institución</th>
										<th>Mecanico</th>
										<th>Nro de Reporte</th>
										<th>Fecha de Ejecución</th>
										<th>Horometro Aproximado</th>
										<th>Fecha Aproximada</th>
										<th>Realizado</th>
										<th>Pase</th>
										<th colspan="2">Acciones</th>
									</tr>
									</thead>
									<tbody>
									@foreach($mps as $mp)
										<tr>
											<td>{{ $mp->mp }}</td>
											<td>{{ $mp->obra_institucion }}</td>
											<td>{{ $mp->mecanico }}</td>
											<td>{{ $mp->nro_reporte }}</td>
											<td>{{ $mp->fecha_ejecución }}</td>
											<td>{{ $mp->horometro_aprox }}</td>
											<td>{{ $mp->fecha_aprox }}</td>
											<td>
												<label class="form-check-label">
													@if( !isset($mp->realizado) )
														<input class="form-check-input"  type="checkbox" value="1" disabled>
													@else
														<input class="form-check-input"  checked="checked" type="checkbox" value="1" disabled>
								@endif
							</div>
							</td>
							<td>
								<label class="form-check-label">
									@if( !isset($mp->pase) )
										<input class="form-check-input"  type="checkbox" value="1" disabled>
									@else
										<input class="form-check-input"  checked="checked" type="checkbox" value="1" disabled>
								</label>
							@endif
						</div>
						</td>
						<td>
							<a href="{{ route('maquinarias.mpreventivos.edit' , ['maquinaria' => $mp->maquinaria_id ,
						'mpreventivo' => $mp->mpreventivo_id]) }}" class="btn btn-success  {{ ($mp->status) ? 'disabled' : ''}}" role="button"><i class="fa fa-pencil"></i></a>
						</td>
						<td>
							@if(!$mp->status)
								{!! Form::open([
                                'method' => 'DELETE',
                                'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/mpreventivos/' . $mp->mpreventivo_id ]) !!}
								{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
								{!! Form::close() !!}
							@else
								<a href="" class="btn btn-danger disabled" role="button"><i class="fa fa-trash"></i></a>
							@endif
						</td>
						@endforeach
						</tbody>
						</table>
					</div>
				</div>


				</div>
					<div role="tabpanel" class="tab-pane fade" id="mcorrectivos">
						<h3>Mantenimiento Preventivo</h3>
							<div class="table-responsive">
								<table class="table">
									<thead class="thead-inverse">
									<tr>
										<th>Obra | Institución</th>
										<th>Fecha</th>
										<th>Mecanico</th>
										<th>Reporte de Falla</th>
										<th>Nro de documento</th>
										<th colspan="2">Acciones</th>
									</tr>
									</thead>
									<tbody>
									@foreach($mcs as $mc)
										<tr>
											<td>{{ $mc->obra_institucion }}</td>
											<td>{{ $mc->fecha }}</td>
											<td>{{ $mc->mecanico }}</td>
											<td>{{ $mc->reporte_falla }}</td>
											<td>{{ $mc->nro_documento }}</td>
											<td>
												<a href="{{ route('maquinarias.mcorrectivos.edit' , ['maquinaria' => $mc->maquinaria_id ,
						'mcorrectivo' => $mc->mcorrectivo_id]) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
											</td>
											<td>
												<a>
													{!! Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/mcorrectivos/' . $mc->mcorrectivo_id ]) !!}
													{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
													{!! Form::close() !!}
												</a>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="movimientos">
						<div class="mt-2">
<h3>Movimientos</h3>
							<div class="table-responsive">
								<table class="table">
									<thead class="thead-inverse">
									<tr>
										<th>Fecha de Entrada</th>
										<th>Fecha de Salida</th>
										<th>Obra / Institución</th>
										<th>Provincia</th>
										<th>Nro de Asignación</th>
										<th>Turno</th>
										<th>Descripción</th>
										<th colspan="2">Acciones</th>
									</tr>
									</thead>
									<tbody>
									@foreach($mov as $mo)
										<tr>
											<td>{{ $mo->fecha_entrada }}</td>
											<td>{{ $mo->fecha_salida }}</td>
											<td>{{ $mo->obra_institucion }}</td>
											<td>{{ $mo->nombre }}</td>
											<td>{{ $mo->doc_asignacion }}</td>
											<td>{{ $mo->turno }}</td>
											<td>{{ $mo->descripcion }}</td>

											<td>
												<a href="{{ route('maquinarias.movimientos.edit' , ['maquinaria' => $mo->maquinaria_id ,
						'movimiento' => $mo->movimiento_id]) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
											</td>
											<td>
												<a>
													{!! Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/movimientos/' . $mo->movimiento_id ]) !!}
													{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
													{!! Form::close() !!}
												</a>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>


			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
var table = $('#example').DataTable( {
lengthChange: false,
buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
} );
table.buttons().container()
.appendTo( '#example_wrapper .col-md-6:eq(0)' );
} );
</script>
@endsection