@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row  justify-content-md-center" >
    <div class="col-md-7">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
  </div>
  <div class="row  justify-content-md-center" >
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="d-flex justify-content-between align-items-center">
            
            <div>
              <h2>{{ $maquinaria->tipo . ' ' . $maquinaria->modelo . ' ' . $maquinaria->nro_registro }}</h2>
            </div>
            <div>
              <h4><strong>Horometro: </strong>{{ $maquinaria->horometro }}</h4>
            </div>
          </div>
        </div>
        <div class="card-block">
          {!! Form::open([
          'route' => ['maquinarias.movimientos.store' , $maquinaria->maquinaria_id],
          'method' => 'POST'
          ]) !!}
          
          <div class="form-group row">
            {!! Form::label('fecha_entrada' , 'Fecha de Entrada:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::date('fecha_entrada' , \Carbon\Carbon::now() , ['class' => 'form-control']) !!}
            </div>
          </div>
          
          <div class="form-group row">
            {!! Form::label('fecha_salida' , 'Fecha de Salida:' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::date('fecha_salida' ,null, ['class' => 'form-control']) !!}
            </div>
          </div>
          
          <div class="form-group row">
            {!! Form::label('obra_institucion' , 'Obra | Institución :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('obra_institucion' , null , ['class' => 'form-control']) !!}
            </div>
          </div>
          
          <div class="form-group row">
            {!! Form::label('provincia_id' , 'Provincia :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::select('provincia_id', $provincias->pluck('nombre'),$provincias->pluck('provincia_id'),['class' => 'form-control'])!!}
            </div>
          </div>
          
          <div class="form-group row">
            {!! Form::label('doc_asignacion' , 'Documento de Asignación :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('doc_asignacion' , null , ['class' => 'form-control']) !!}
            </div>
          </div>
          
          <div class="form-group row">
            {!! Form::label('descripcion' , 'Descripción :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              {!! Form::text('descripcion' , null , ['class' => 'form-control']) !!}
            </div>
          </div>
          
          <div class="form-group row">
            {!! Form::label('turno' , 'Turno :' , ['class' => 'col-form-label col-sm-3']) !!}
            <div class="col-sm-9">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  {!! Form::radio('turno' , 1 , true , [ 'class' => 'form-check-input'])!!}
                  1 turno
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  {!! Form::radio('turno' , 2 , false , [ 'class' => 'form-check-input']) !!}
                  2 turnos
                </label>
              </div>
            </div>
          </div>
          
          
          <div class="form-group">
            {!! Form::submit('Guardar' , ['class' => 'btn btn-success']) !!}
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection