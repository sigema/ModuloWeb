<div class="mt-2">
	<div class="row ">
		<div class="col">
			<div class="d-flex mb-3 justify-content-end">
				<a href="{{ route('maquinarias.movimientos.create' , $maquinaria->maquinaria_id) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
			</div>
		
		</div>
	</div>
	<div class="table-responsive">
		<table class="table">
			<thead class="thead-inverse">
				<tr>
					<th>Fecha de Entrada</th>
					<th>Fecha de Salida</th>
					<th>Obra / Institución</th>
					<th>Provincia</th>
					<th>Nro de Asignación</th>
					<th>Turno</th>
					<th>Descripción</th>
					<th colspan="2">Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($mov as $mo)
				<tr>
					<td>{{ $mo->fecha_entrada }}</td>
					<td>{{ $mo->fecha_salida }}</td>
					<td>{{ $mo->obra_institucion }}</td>
					<td>{{ $mo->nombre }}</td>
					<td>{{ $mo->doc_asignacion }}</td>
					<td>{{ $mo->turno }}</td>
					<td>{{ $mo->descripcion }}</td>
					
					<td>
						<a href="{{ route('maquinarias.movimientos.edit' , ['maquinaria' => $mo->maquinaria_id ,
						'movimiento' => $mo->movimiento_id]) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
					</td>
					<td>
						<a>
							{!! Form::open([
							'method' => 'DELETE',
							'url' => '/maquinarias/' . $maquinaria->maquinaria_id . '/movimientos/' . $mo->movimiento_id ]) !!}
							{!! Form::button('<i class="fa fa-trash"></i>',  ['class' => 'btn btn-danger' , 'type' => 'submit']) !!}
							{!! Form::close() !!}
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>